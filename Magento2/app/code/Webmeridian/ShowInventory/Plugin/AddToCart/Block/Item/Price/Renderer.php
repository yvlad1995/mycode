<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\AddToCart\Block\Item\Price;

use \Webmeridian\ShowInventory\Plugin\PluginAbstract;
use \Webmeridian\ShowInventory\Helper\Minicart\Helper;

/**
 * Class Renderer
 * @package Webmeridian\ShowInventory\Plugin\Tax\Block\Item\Price
 *
 * This class works when you add a product to the cart!
 */
class Renderer extends PluginAbstract
{

    /**
     * Renderer constructor.
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Tax\Block\Item\Price\Renderer $object
     * @param $price
     * @return string
     *
     * Works when adding in cart for
     * - Simple Product Type
     * - Configurable Product Type
     * - Virtual Product Type
     * - Downloadable Product Type
     */
    public function afterFormatPrice(\Magento\Tax\Block\Item\Price\Renderer $object, $price)
    {
        $qty = 0;
        if($this->_helper->isEnable()){
            $item = $object->getItem();
            $product = $object->getItem()->getProduct();

            if(
                $product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE
                && !$this->_helper->isActionName('checkout_cart_index')
            ) {
               $qty = $this->getQtyForProduct($object->getItem()->getProduct());
            } elseif(
                $product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE
                && $item->getQtyOptions()
                && !$this->_helper->isActionName('checkout_cart_index')
            ){
                $key = $this->_helper->getKeysForArray($item->getQtyOptions());
                $configurableItem = $item->getQtyOptions()[$key[0]];
                $product = $this->_helper->getProduct($configurableItem->getProductId());
                $qty = $this->getQtyForProduct($product);
            }
            return $this->_helper->getInventoryHtml($price, $qty);
        }

        return $price;

    }

}