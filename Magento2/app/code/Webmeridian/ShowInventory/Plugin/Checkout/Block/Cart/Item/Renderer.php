<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Checkout\Block\Cart\Item;

use \Webmeridian\ShowInventory\Plugin\PluginAbstract;

class Renderer extends PluginAbstract
{
    public function afterGetProductName($object, $name)
    {
        if($this->_helper->isEnable()){
            $product = $object->getProduct();
            $qty = $this->getQtyForProduct($product);

            return $this->returnPriceText($name, $qty);
        }
    }
}