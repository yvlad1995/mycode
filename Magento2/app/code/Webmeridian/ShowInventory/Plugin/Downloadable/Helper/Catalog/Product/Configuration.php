<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Downloadable\Helper\Catalog\Product;

use \Webmeridian\ShowInventory\Plugin\PluginAbstract;
use \Webmeridian\ShowInventory\Helper\Downloadable\Helper as DownloadableHelper;

/**
 * Class Configuration
 * @package Webmeridian\ShowInventory\Plugin\Downloadable\Helper\Catalog\Product
 * This class show inventory for Downloadable product on all pages except Checkout Cart
 */
class Configuration extends PluginAbstract
{

    /**
     * Configuration constructor.
     * @param DownloadableHelper $helper
     */
    public function __construct(DownloadableHelper $helper)
    {
        parent::__construct($helper);
    }

    /**
     * @param \Magento\Downloadable\Helper\Catalog\Product\Configuration $object
     * @param $item
     */
    public function beforeGetOptions(\Magento\Downloadable\Helper\Catalog\Product\Configuration $object, $item)
    {
        if($this->_helper->isEnable() && !$this->_helper->isActionName('checkout_cart_index')) {
            $this->_helper->setRegister('downloadable_item', $item);
        }

    }

    /**
     * @param \Magento\Downloadable\Helper\Catalog\Product\Configuration $object
     * @param $options
     * @return array
     */
    public function afterGetOptions(\Magento\Downloadable\Helper\Catalog\Product\Configuration $object, $options)
    {
        if($this->_helper->isEnable() && !$this->_helper->isActionName('checkout_cart_index')){
            $item = $this->_helper->getRegistry('downloadable_item');
            if (
                $this->_helper->isEnableInventoryStatus()
                && $this->_helper->isEnableCartPage()
                && $item
            ) {

                $this->_helper->unsetRegistry('downloadable_item');

                $options = $this->_helper->getOptions($item, $options);

                return array_merge(
                    $options,
                    $this->_helper->getProductConfiguration()->getCustomOptions($item)
                );
            }
        }

        return $options;
    }
}