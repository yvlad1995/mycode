<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin;

use \Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class PluginAbstract
 * @package Webmeridian\ShowInventory\Plugin
 */
abstract class PluginAbstract implements PluginInterface
{
    /**
     * @var ProductHelper
     */
    public $_helper;

    /**
     * PluginAbstract constructor.
     * @param ProductHelper $helper
     */
    public function __construct(ProductHelper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param $product
     * @return bool|float
     */
    public function getQtyForProduct($product)
    {
        if($product)
        {
            $productData = $this->_helper->getDataForProduct($product);
            return $this->_helper->getStockQty($productData['productId']);
        }
        return false;
    }

    public function returnPriceText($price)
    {
        $product = $this->_helper->getRegistry('product_for_inventory');
        $this->_helper->unsetRegistry('product_for_inventory');

        $qty = $this->getQtyForProduct($product);

        return $this->_helper->getInventoryHtml($price, $qty);
    }

}