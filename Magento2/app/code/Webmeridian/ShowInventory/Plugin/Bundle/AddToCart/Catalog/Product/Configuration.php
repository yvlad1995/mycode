<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Bundle\AddToCart\Catalog\Product;

use \Webmeridian\ShowInventory\Helper\Bundle\Helper as BundleHelper;
use \Webmeridian\ShowInventory\Plugin\PluginAbstract;

/**
 * Class Configuration
 * @package Webmeridian\ShowInventory\Plugin\Bundle\Helper\Catalog\Product
 *
 * This class works after added Bundle product type in cart
 */
class Configuration extends PluginAbstract
{
    /**
     * Configuration constructor.
     * @param BundleHelper $helper
     */
    public function __construct(BundleHelper $helper)
    {
        parent::__construct($helper);
    }

    /**
     * @param \Magento\Bundle\Helper\Catalog\Product\Configuration $object
     * @param $item
     */
    public function beforeGetOptions(\Magento\Bundle\Helper\Catalog\Product\Configuration $object, $item)
    {
        if($this->_helper->isEnable() && !$this->_helper->isActionName('checkout_cart_index')){
            $this->_helper->setRegister('bundle_item', $item);
        }
    }

    /**
     * @param \Magento\Bundle\Helper\Catalog\Product\Configuration $object
     * @param $options
     * @return array
     */
    public function afterGetOptions(\Magento\Bundle\Helper\Catalog\Product\Configuration $object, $options)
    {
        if($this->_helper->isEnable() && !$this->_helper->isActionName('checkout_cart_index')){
            $item = $this->_helper->getRegistry('bundle_item');
            $this->_helper->unsetRegistry('bundle_item');

            $options = $this->_helper->getOptions($object, $item);

            return array_merge(
                $options,
                $this->_helper->getProductConfiguration()->getCustomOptions($item)
            );
        }

        return $options;
    }
}