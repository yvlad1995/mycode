<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Bundle\Block\Catalog\Product\View\Type\Bundle;

use Webmeridian\ShowInventory\Plugin\PluginAbstract;

/**
 * Class Option
 * @package Webmeridian\ShowInventory\Plugin\Bundle\Block\Catalog\Product\View\Type\Bundle
 *
 * This class works on product view page for bundle product type
 */
class Option extends PluginAbstract
{
    /**
     * @param \Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option $object
     * @param $selection
     */
    public function beforeGetSelectionTitlePrice(\Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option $object, $selection)
    {
        if($this->_helper->isEnable()) {
            $product = $this->_helper->getProduct($selection->getId());
            $this->_helper->setRegister('product_for_inventory', $product);
        }
    }

    /**
     * @param \Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option $object
     * @param $price
     * @return string
     */
    public function afterGetSelectionTitlePrice(\Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option $object, $price)
    {
        if($this->_helper->isEnable()) {
            return $this->returnPriceText($price);
        }

        return $price;
    }
}