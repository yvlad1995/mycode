<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Catalog\Block\Product;

use \Webmeridian\ShowInventory\Plugin\PluginAbstract;
use \Webmeridian\ShowInventory\Helper\Grouped\Helper as BundleHelper;

/**
 * Class AbstractProduct
 * @package Webmeridian\ShowInventory\Plugin\Catalog\Block\Product
 *
 * This class works for grouped product type on product view page
 */
class AbstractProduct extends PluginAbstract
{

    /**
     * AbstractProduct constructor.
     * @param BundleHelper $helper
     */
    public function __construct(BundleHelper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Catalog\Block\Product\AbstractProduct $object
     * @param \Magento\Catalog\Model\Product $product
     */
    public function beforeGetProductPriceHtml(\Magento\Catalog\Block\Product\AbstractProduct $object,
                                              \Magento\Catalog\Model\Product $product)
    {
        if($this->_helper->isEnable()) {
            $this->_helper->setRegister('product_for_inventory', $product);
        }
    }

    /**
     * @param \Magento\Catalog\Block\Product\AbstractProduct $object
     * @param $price
     * @return string
     */
    public function afterGetProductPriceHtml(\Magento\Catalog\Block\Product\AbstractProduct $object, $price)
    {
        if($this->_helper->isEnable()) {
            return $this->returnPriceText($price);
        }
    }

    /**
     * @param $price
     * @return string
     */
    public function returnPriceText($price)
    {
        $qty = $this->_helper->getQtyForGrouped();
        return $this->_helper->getInventoryHtml($price, $qty);
    }
}