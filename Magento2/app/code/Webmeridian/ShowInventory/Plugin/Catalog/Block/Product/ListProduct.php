<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Catalog\Block\Product;

use \Webmeridian\ShowInventory\Plugin\PluginAbstract;

/**
 * Class ListProduct
 * @package Webmeridian\ShowInventory\Plugin\Block\Product
 *
 * This class works on list product page for:
 * - Simple Product
 */
class ListProduct extends PluginAbstract
{
    /**
     * @param \Magento\Catalog\Block\Product\ListProduct $object
     * @param \Magento\Catalog\Model\Product $product
     */
    public function beforeGetProductPrice(\Magento\Catalog\Block\Product\ListProduct $object,  \Magento\Catalog\Model\Product $product)
    {
        if($this->_helper->isEnable()){
            $this->_helper->setRegister('product_for_inventory', $product);
        }
    }

    /**
     * @param \Magento\Catalog\Block\Product\ListProduct $object
     * @param $price
     * @return string
     */
    public function afterGetProductPrice(\Magento\Catalog\Block\Product\ListProduct $object, $price)
    {
        if($this->_helper->isEnable()){
            return $this->returnPriceText($price);
        }
    }
}