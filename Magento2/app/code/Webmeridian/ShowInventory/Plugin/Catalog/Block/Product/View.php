<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Plugin\Catalog\Block\Product;

use \Webmeridian\ShowInventory\Helper\View\Helper as ViewHelper;
use \Webmeridian\ShowInventory\Plugin\PluginAbstract;

/**
 * Class View
 * @package Webmeridian\ShowInventory\Plugin\Catalog\Block\Product
 * Works on product view for:
 * - Simple product type
 * - Downloadable product type
 * - Virtual product type
 */
class View extends PluginAbstract
{
    /**
     * View constructor.
     * @param ViewHelper $helper
     */
    public function __construct(ViewHelper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Catalog\Block\Product\View $object
     * @param $config
     * @return mixed|string
     */
    public function afterGetJsonConfig(\Magento\Catalog\Block\Product\View $object, $config)
    {
        if($this->_helper->isEnable()){
            $product = $object->getProduct();
            if ($product &&
                ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE
                    || $product->getTypeId() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE
                    || $product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL
                )
            ) {
                $config = $this->_helper->jsonDecode($config);
                $qty = $this->getQtyForProduct($product);
                $config['quantity'] = $this->_helper->getTextTranslate($qty);
                return $this->_helper->jsonEncode($config);
            }
        }

        return $config;
    }
}