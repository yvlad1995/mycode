var config = {
    map: {
        '*': {
            priceBox: 'Webmeridian_ShowInventory/js/price-box',
            'Magento_Catalog/js/price-box': 'Webmeridian_ShowInventory/js/price-box',
            webmeridianshowinventory:      'Webmeridian_ShowInventory/js/showinventory',
        }
    },

    paths:{
        webmeridianshowinventory: 'Webmeridian_ShowInventory/js/showinventory'
    }
};
