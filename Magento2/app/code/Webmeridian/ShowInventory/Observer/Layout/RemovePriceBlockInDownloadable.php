<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;
use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class RemovePriceBlockInDownloadable
 * @package Webmeridian\ShowInventory\Observer\Layout
 *
 * This observer remove block price.copy on product view page for download product type
 */
class RemovePriceBlockInDownloadable implements ObserverInterface
{
    /**
     * @var ProductHelper
     */
    public $_helper;

    /**
     * RemovePriceBlockInDownloadable constructor.
     * @param ProductHelper $helper
     */
    public function __construct(ProductHelper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($this->_helper->isEnable()){
            $layout = $observer->getEvent()->getLayout();
            $block = $layout->getBlock('product.price.final.copy');//->getBlocks();
            if($block)
            {
                $layout->unsetElement('product.price.final.copy');
            }
        }
    }
}