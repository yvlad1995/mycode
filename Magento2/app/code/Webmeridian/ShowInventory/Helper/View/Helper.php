<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper\View;

use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\View
 */
class Helper extends ProductHelper
{
    /**
     * @param $price
     * @return string
     */
    public function getTextTranslate($price)
    {
        if($price > 0){
            return parent::getTextTranslate($price);
        }
    }
}