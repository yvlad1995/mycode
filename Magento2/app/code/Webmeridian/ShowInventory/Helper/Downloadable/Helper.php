<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper\Downloadable;

use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\Downloadable
 */
class Helper extends ProductHelper
{
    /**
     * @param $item
     * @param $options
     * @return bool
     */
    public function getOptions($item, $options)
    {
        if(!$item) return false;
        $qty = $this->getStockQty($item->getProduct()->getId());

        foreach ($options as $key => $option)
        {
            $options[$key]['value'][] = $this->getInventoryHtml('', $qty);
        }

        return $options;
    }
}