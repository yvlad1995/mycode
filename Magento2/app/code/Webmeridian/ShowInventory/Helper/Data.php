<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper;

use Magento\Framework\App\Helper\Context;
use \Magento\Framework\Registry;
use \Magento\CatalogInventory\Api\StockStateInterface;
use \Magento\Catalog\Model\Product;
use \Magento\Framework\Json\Helper\Data as jsonHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Catalog\Helper\Product\Configuration as ProductConfiguration;

/**
 * Class Data
 * @package Webmeridian\ShowInventory\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Registry
     */
    public $_registry;

    /**
     * @var StockStateInterface
     */
    public $_stockItem;

    /**
     * @var jsonHelper
     */
    public $_jsonHelper;

    /**
     * @var
     */
    public $_request;

    /**
     * @var ProductConfiguration
     */
    public $_productConfiguration;

    /**
     * @var Product
     */
    protected $_productModel;

    /**
     * Data constructor.
     * @param Context $context
     * @param Registry $registry
     * @param StockStateInterface $stockItem
     * @param Product $productModel
     * @param jsonHelper $jsonHelper
     * @param HttpRequest $httpRequest
     */
    public function __construct(Context $context,
                                Registry $registry,
                                StockStateInterface $stockItem,
                                Product $productModel,
                                jsonHelper $jsonHelper,
                                HttpRequest $httpRequest,
                                ProductConfiguration $productConfiguration
    )
    {
        $this->_registry     = $registry;
        $this->_stockItem    = $stockItem;
        $this->_productModel = $productModel;
        $this->_jsonHelper   = $jsonHelper;
        $this->_httpRequest  = $httpRequest;
        $this->_productConfiguration = $productConfiguration;
        parent::__construct($context);
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getDataForProduct(\Magento\Catalog\Model\Product $product)
    {
        return array(
            'productId' => $product->getId(),
        );
    }

    /**
     * @param $productType
     * @return bool
     */
    public function enableProductType($productType)
    {
        if($productType == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE ||
            $productType == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE ||
            $productType == \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL
        ){
            return true;
        }
        return false;
    }

    /**
     * @param $registerAlias
     * @param $data
     */
    public function setRegister($registerAlias, $data)
    {
        $this->_registry->register($registerAlias, $data);
    }

    /**
     * @param $registerAlias
     * @return mixed
     */
    public function getRegistry($registerAlias)
    {
        return $this->_registry->registry($registerAlias);
    }

    /**
     * @param $registerAlias
     */
    public function unsetRegistry($registerAlias)
    {
        $this->_registry->unregister($registerAlias);
    }

    /**
     * @param $text
     * @param $qty
     * @return string
     */
    public function getInventoryHtml($text, $qty)
    {
        $textInventory = $this->getTextTranslate($qty);
        return $text . " <div class=\"product_inventory_qty\">
                                <span class=\"qty inventory\">$textInventory</span>
                          </div>";
    }

    /**
     * @param $text
     * @return string
     */
    public function getTextTranslate($qty)
    {
        $text = __('left in stock');

       return sprintf('%s %s', $qty, $text);
    }

    /**
     * @param $productId
     * @return $this
     */
    public function getProduct($productId)
    {
        return $this->_productModel->load($productId);
    }

    /**
     * @param $data
     * @return string
     */
    public function jsonEncode($data)
    {
        return $this->_jsonHelper->jsonEncode($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function jsonDecode($data)
    {
        return $this->_jsonHelper->jsonDecode($data);
    }

    /**
     * @return HttpRequest
     */
    public function getHttpRequest()
    {
        return $this->_httpRequest;
    }

    /**
     * @return \Magento\Framework\App\ObjectManager
     */
    public function getObjectManager()
    {
        return \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * @param $alias
     * @return mixed
     */
    public function getModelByNamespace($alias)
    {
        return $this->getObjectManager()->create($alias);
    }

    /**
     * @return ProductConfiguration
     */
    public function getProductConfiguration()
    {
        return $this->_productConfiguration;
    }

    /**
     * @param array $array
     * @return array
     */
    public function getKeysForArray(array $array)
    {
        return array_keys($array);
    }


}