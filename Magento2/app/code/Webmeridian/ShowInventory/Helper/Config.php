<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper;

/**
 * Class Config
 * @package Webmeridian\ShowInventory\Helper
 */
class Config extends Data
{
    const ENABLE_INVENTORY = 'webmeridian_showinventory/general/enable';

    const ENABLE_CATEGORY  = 'webmeridian_showinventory/general/display_at_category';

    const ENABLE_PRODUCT   = 'webmeridian_showinventory/general/display_at_product_page';

    const ENABLE_CART      = 'webmeridian_showinventory/general/display_in_cart';

    const ENABLE_MINICART  = 'webmeridian_showinventory/general/display_in_minicart';

    /**
     * @param $pathConfig
     * @return mixed
     */
    public function isEnableConfig($pathConfig)
    {
        return $this->scopeConfig->getValue($pathConfig, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isEnableInventoryStatus()
    {
        return $this->isEnableConfig(\Webmeridian\ShowInventory\Helper\Config::ENABLE_INVENTORY);
    }

    /**
     * @return mixed
     */
    public function isEnableCategoryPage()
    {
        return $this->isEnableConfig(\Webmeridian\ShowInventory\Helper\Config::ENABLE_CATEGORY);
    }

    /**
     * @return mixed
     */
    public function isEnableProductPage()
    {
        return $this->isEnableConfig(\Webmeridian\ShowInventory\Helper\Config::ENABLE_PRODUCT);
    }

    /**
     * @return mixed
     */
    public function isEnableCartPage()
    {
        return $this->isEnableConfig(\Webmeridian\ShowInventory\Helper\Config::ENABLE_CART);
    }

    /**
     * @return mixed
     */
    public function isEnableMinicart()
    {
        return $this->isEnableConfig(\Webmeridian\ShowInventory\Helper\Config::ENABLE_MINICART);
    }

    /**
     * @param $actionName
     * @return bool
     */
    public function isActionName($actionName)
    {
        return ($this->getHttpRequest()->getFullActionName() == $actionName);
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        if($this->isEnableInventoryStatus())
        {
            if($this->configByActionName()){
                return true;
            }
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function configByActionName()
    {
        $actionName = $this->getHttpRequest()->getFullActionName();
        switch ($actionName)
        {
            case 'checkout_cart_index':
                return $this->isEnableCartPage();

            case 'catalog_product_view':
                return $this->isEnableProductPage();

            case 'catalog_category_view':
                return $this->isEnableCategoryPage();

            case 'customer_section_load':
                return $this->isEnableMinicart();
        }
    }
}