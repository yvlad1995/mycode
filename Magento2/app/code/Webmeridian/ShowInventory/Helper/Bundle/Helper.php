<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
*/

namespace Webmeridian\ShowInventory\Helper\Bundle;

use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\Bundle
 */
class Helper extends ProductHelper
{
    /**
     * @param $childrens
     * @param $item
     * @param array $data
     * @return array
     */
    protected function getLabelAndQty($childrens, $item, $data = [])
    {
        foreach ($childrens as $child){
            foreach ($child as $id){
                $product = $this->getModelByNamespace('Magento\Catalog\Model\Product')->load($id);

                $qty = $this->getQtyForBundle($id, $item->getQtyOptions());

                $data[$id] = [
                    'label' => $product->getName(),
                    'qty'   => $this->getInventoryHtml('', $qty)
                ];
            }
        }

        return $data;
    }

    /**
     * @param $productId
     * @param $itemQtyOptions
     * @return float
     */
    public function getQtyForBundle($productId, $itemQtyOptions)
    {
        if(array_key_exists($productId, $itemQtyOptions)){
            return $this->getStockQty($productId);
        }
    }

    /**
     * @param $options
     * @param $data
     * @return mixed
     */
    protected function getOptionsForBundle($options, $data)
    {
        foreach ($options as $key => $option) {
            foreach ($option['value'] as $value)
            {
                foreach ($data as $item)
                    if(strpos($value, $item['label'])){
                        $options[$key]['value'][] = $item['qty'];
                    }
            }
        }
        return $options;
    }

    /**
     * @param $object
     * @param $item
     * @return mixed
     */
    public function getOptions($object, $item)
    {
        $product = $item->getProduct();
        $options = $object->getBundleOptions($item);
        $bundleModel = $this->getModelByNamespace('\Magento\Bundle\Model\Product\Type');
        $childrens = $bundleModel->getChildrenIds($product->getId());
        $data = $this->getLabelAndQty($childrens, $item);

        return $this->getOptionsForBundle($options, $data);
    }
}