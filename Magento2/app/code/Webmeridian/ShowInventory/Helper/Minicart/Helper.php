<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper\Minicart;

use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\Minicart
 */
class Helper extends ProductHelper
{
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|bool
     */
    public function getDataForProduct(\Magento\Catalog\Model\Product $product)
    {
        return parent::getDataForProduct($product);
    }

}