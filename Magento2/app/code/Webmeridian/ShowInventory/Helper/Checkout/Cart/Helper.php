<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */


namespace Webmeridian\ShowInventory\Helper\Checkout\Cart;

use \Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\Checkout\Cart
 */
class Helper extends ProductHelper
{
    /**
     * @param $item
     * @return float
     */
    public function getStockQtyByItem($item)
    {
        if($this->isEnable()) {
            $product = $item->getProduct();

            if ($item->getProduct()->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $key = $this->getKeysForArray($item->getQtyOptions());
                $configurableItem = $item->getQtyOptions()[$key[0]];
                $product = $this->getProduct($configurableItem->getProductId());
            }
            return parent::getStockQty($product->getId());
        }
    }

    /**
     * @param $productId
     * @param $itemQtyOptions
     * @return float
     */
    public function getQtyForBundle($item, $i)
    {
        $product = $item->getProduct();
        $bundleModel = $this->getModelByNamespace('\Magento\Bundle\Model\Product\Type');
        $childrens = $bundleModel->getChildrenIds($product->getId());
        foreach ($childrens[$i] as $id){
            if(array_key_exists($id, $item->getQtyOptions())){
                return $this->getStockQty($id);
            }
        }
    }
}