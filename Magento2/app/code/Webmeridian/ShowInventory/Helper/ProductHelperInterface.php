<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper;

/**
 * Interface ProductHelperInterface
 * @package Webmeridian\ShowInventory\Helper
 */
interface ProductHelperInterface
{
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed
     */
    public function getDataForProduct(\Magento\Catalog\Model\Product $product);

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed
     */
    public function getTypeProduct(\Magento\Catalog\Model\Product $product);

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed
     */
    public function productEnable(\Magento\Catalog\Model\Product $product);

    /**
     * @param $productId
     * @return mixed
     */
    public function getStockQty($productId);

    /**
     * @param $text
     * @param $qty
     * @return mixed
     */
    public function getInventoryHtml($text, $qty);
}