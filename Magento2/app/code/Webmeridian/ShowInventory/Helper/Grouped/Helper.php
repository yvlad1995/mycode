<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper\Grouped;

use Webmeridian\ShowInventory\Helper\ProductHelper;

/**
 * Class Helper
 * @package Webmeridian\ShowInventory\Helper\Grouped
 */
class Helper extends ProductHelper implements HelperInterface
{
    /**
     * @return float|int
     */
    public function getQtyForGrouped()
    {
        $poolProductIds = $this->getPoolProductIds();

        $product = $this->getProductRegistry();

        if (!in_array($product->getId(), $poolProductIds)) {

            $poolProductIds[] = $product->getId();

            $this->setPoolProductIds($poolProductIds);

            return $this->getStockQty($product->getId());

        }

        return 0;
    }

    /**
     * @return array|mixed
     */
    public function getPoolProductIds()
    {
        $groupedProduct = $this->getRegistry('grouped_product_item');
        $this->unsetRegistry('grouped_product_item');
        if(!$groupedProduct)
        {
            return [];
        }

        return $groupedProduct;
    }

    /**
     * @param $poolProductIds
     */
    public function setPoolProductIds($poolProductIds)
    {
        $this->setRegister('grouped_product_item', $poolProductIds);
    }

    /**
     * @return mixed
     */
    protected function getProductRegistry()
    {
        $product = $this->getRegistry('product_for_inventory');

        $this->unsetRegistry('product_for_inventory');

        return $product;
    }

}