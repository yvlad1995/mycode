<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper\Grouped;

/**
 * Interface HelperInterface
 * @package Webmeridian\ShowInventory\Helper\Grouped
 */
interface HelperInterface
{
    /**
     * @return mixed
     */
    public function getQtyForGrouped();

    /**
     * @return mixed
     */
    public function getPoolProductIds();

    /**
     * @param $poolProductIds
     * @return mixed
     */
    public function setPoolProductIds($poolProductIds);
}