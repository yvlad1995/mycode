<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */

namespace Webmeridian\ShowInventory\Helper;

/**
 * Class ProductHelper
 * @package Webmeridian\ShowInventory\Helper
 */
class ProductHelper extends Config implements ProductHelperInterface
{
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|bool
     */
    public function getDataForProduct(\Magento\Catalog\Model\Product $product)
    {
        if($this->productEnable($product))
        {
            return parent::getDataForProduct($product);
        }
        return false;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|string
     */
    public function getTypeProduct(\Magento\Catalog\Model\Product $product)
    {
        return $product->getTypeId();
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function productEnable(\Magento\Catalog\Model\Product $product)
    {
        $productType = $this->getTypeProduct($product);

        if($this->enableProductType($productType))
        {
            return true;
        }

        return false;
    }

    /**
     * @param $productId
     * @return float
     */
    public function getStockQty($productId)
    {
        return $this->_stockItem->getStockQty($productId, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $text
     * @param $qty
     * @return string
     */
    public function getInventoryHtml($text, $qty = 0)
    {
        if($qty > 0){
            return parent::getInventoryHtml($text, $qty);
        }
        return $text;
    }

}