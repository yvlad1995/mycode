<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
*/
namespace Webmeridian\ShowInventory\Block\Product\View\Type;

use Webmeridian\ShowInventory\Block\Product\View\ConfigurableAbstract;

/**
 * Class Configurable
 * @package Webmeridian\ShowInventory\Block\Product\View\Type
 *
 * This class return jsoc config data for product Configurable Type
 * - works on product view page for swatch click!
 */
class Configurable extends ConfigurableAbstract
{
    /**
     * @return string|void
     */
    public function getJsonConfig()
    {
        $data = [];
        if($this->_configurableHelper->isEnable()) {

            $product = $this->getProduct();
            if ($product && $product->getTypeId() != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) return;

            $productAttributeOptions = $this->_configurableProduct
                ->getConfigurableAttributesAsArray($product);

            $data['attributes'] = $this->getAttributesData($productAttributeOptions);

            $childrens = $product->getTypeInstance()->getUsedProductIds($product);

            $data['optionsProduct'] = $this->getOptionsProduct($childrens, $productAttributeOptions);
        }
        return $this->jsonEncoder->encode($data);
    }

    /**
     * @param $productAttributeOptions
     * @param array $data
     * @return array
     */
    protected function getAttributesData($productAttributeOptions, $data = array())
    {
        foreach ($productAttributeOptions as $id => $productAttributeOption) {
            $data[] = $productAttributeOption['attribute_code'];
        }

        return $data;
    }

    /**
     * @param $productAttributeOptions
     * @param array $options
     * @return array
     */
    protected function getAttributeOptions($productAttributeOptions, $options = array())
    {
        foreach ($productAttributeOptions as $id => $productAttributeOption) {
            $options[]  = $productAttributeOption['attribute_code'];
        }
        return $options;
    }

    /**
     * @param $childrens
     * @param $productAttributeOptions
     * @return array
     */
    protected function getOptionsProduct($childrens, $productAttributeOptions)
    {
        $options = $this->getAttributeOptions($productAttributeOptions);
        foreach ($childrens as $id)
        {
            $productChildren = $this->_productModel->load($id);
            $qty = $this->_configurableHelper
                ->getStockQty($id, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            foreach ($options as $attributeId => $option) {
                $optionsProduct[$attributeId] = $productChildren->getData($option);
            }
            $quantity = $this->_configurableHelper->getTextTranslate($qty);
            $data[] = [
                'options' => $optionsProduct,
                'quantity' => $quantity,
            ];
        }

        return $data;
    }
}