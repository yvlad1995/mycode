<?php
/**
 * Webmeridian - Creating profitable online stores
 *
 * @author    vlad.y@webmeridian.org
 * @category  Webmeridian
 * @package   Webmeridian_ShowInventory
 * @copyright Copyright (c) 2017 Webmeridian
 */
namespace Webmeridian\ShowInventory\Block\Product\View;

use Webmeridian\ShowInventory\Helper\Configurable\Helper as ConfigurableHelper;
use Magento\ConfigurableProduct\Block\Product\View\Type\Configurable as TypeConfigurable;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProduct;

/**
 * Class ConfigurableAbstract
 * @package Webmeridian\ShowInventory\Block\Product\View
 */
abstract class ConfigurableAbstract extends TypeConfigurable
{
    /**
     * @var ConfigurableProduct
     */
    public $_configurableProduct;

    /**
     * @var ConfigurableHelper
     */
    public $_configurableHelper;

    /**
     * ConfigurableAbstract constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Stdlib\ArrayUtils $arrayUtils
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\ConfigurableProduct\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param array $data
     * @param ConfigurableProduct $configurableProduct
     * @param ConfigurableHelper $configurableHelper
     * @param ProductModel $productModel
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Stdlib\ArrayUtils $arrayUtils,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        \Magento\Catalog\Helper\Product $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        array $data = [],
        ConfigurableProduct $configurableProduct,
        ConfigurableHelper $configurableHelper,
        ProductModel $productModel
    )
    {
        $this->_configurableProduct = $configurableProduct;
        $this->_configurableHelper  = $configurableHelper;
        $this->_productModel        = $productModel;

        parent::__construct($context, $arrayUtils, $jsonEncoder, $helper, $catalogProduct, $currentCustomer, $priceCurrency, $configurableAttributeData, $data);
    }

    /**
     * //Implement this class in child classes
     */
    public function getJsonConfig()
    {

    }
}