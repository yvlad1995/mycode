<?php

namespace Web4pro\CartPopup\Observer;

use Magento\Framework\Event\ObserverInterface;

class OpenMiniCartObserver implements ObserverInterface
{
    public $_checkoutSession;
    public $_helper;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Web4pro\CartPopup\Helper\Data $helper
    ) {
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if($this->_helper->isEnablePopup() && $observer->getEvent()->getRequest()->isAjax())
        {
            $this->_checkoutSession->setWeb4proCartPopup(true);
        }
    }
}