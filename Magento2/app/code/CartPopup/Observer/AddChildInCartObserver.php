<?php

namespace Web4pro\CartPopup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\TestFramework\Event\Magento;

class AddChildInCartObserver implements ObserverInterface
{
    public $_checkoutSession;
    public $_scopeConfig;
    public $_helper;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Web4pro\CartPopup\Helper\Data $helper
    ) {
        $this->_helper          = $helper;
        $this->_scopeConfig     = $scopeConfig;
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $container = $observer->getEvent()->getContainer();
        if($container instanceof \Magento\Catalog\Block\ShortcutButtons && $container->getRequest()->isAjax() && $this->_helper->isEnablePopup() && $this->_checkoutSession->getWeb4proCartPopup())
        {
            $cartpopup = $container->getLayout()->createBlock('\Web4pro\CartPopup\Block\Cartpopup');
            $container->addShortcut($cartpopup);
        }
    }

}