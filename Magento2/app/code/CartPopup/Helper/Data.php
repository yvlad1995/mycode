<?php
/**
 * WEB4PRO - Creating profitable online stores
 *
 * @author    WEB4PRO
 * @category  WEB4PRO
 * @package   Web4pro_Stockstatus
 * @copyright Copyright (c) 2017 WEB4PRO (http://www.web4pro.net)
 * @license   http://www.web4pro.net/license.txt
 */
namespace Web4pro\CartPopup\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function isEnablePopup()
    {
        return $this->scopeConfig->getValue('web4pro_cartpopup/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}