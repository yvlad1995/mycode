<?php


namespace Web4pro\CartPopup\Block;

use Magento\Catalog\Block\ShortcutInterface;
use Magento\Checkout\Block\Cart\AbstractCart;

class Cartpopup extends AbstractCart implements ShortcutInterface
{

    public function _construct()
    {
        $this->setTemplate('Web4pro_CartPopup::cart/cartpopup.phtml');
        parent::_construct();
    }

    public function getAlias()
    {
        return 'web4procartpopup';
    }

    protected function _toHtml()
    {
         if($this->_checkoutSession->getWeb4proCartPopup()){
            $this->_checkoutSession->setWeb4proCartPopup(false);
            return parent::_toHtml();
        }
        return false;
    }

    public function getTimestop()
    {
        return $this->_scopeConfig->getValue('web4pro_cartpopup/general/time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) * 1000;
    }
}