<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 03.08.17
 * Time: 13:07
 */

class Webmeridian_ScriptNewAttribute_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $helper = $this->_getHelper();
        $attributeData = $helper->getAttributeData();
        $storeIds = $helper->getAllStoresIds();

        $typeProducts = $helper->getProductTypes();

        try{
            foreach($typeProducts as $typeProduct)
            {
                Mage::getModel('webmeridian_scriptnewattribute/attributeUpdate')
                    ->updateAttribute($attributeData, $typeProduct, $storeIds);
            }
            Mage::getSingleton('adminhtml/session')->addSuccess("Attributes have been updated!");
        }catch (Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->addError("Attributes have not been updated!");
        }

        $this->_redirect('adminhtml/dashboard/index');
    }

    protected function _getHelper()
    {
        return Mage::helper('webmeridian_scriptnewattribute');
    }

}