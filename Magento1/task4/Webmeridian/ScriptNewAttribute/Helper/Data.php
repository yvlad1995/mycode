<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 03.08.17
 * Time: 13:17
 */

class Webmeridian_ScriptNewAttribute_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getAttributeData()
    {
        return array('new' => 19799);
    }

    public function getAllStoresIds()
    {
        return Mage::app()->getStores();
    }

    public function getProductTypes()
    {
        return array(0 => 'simple', 1 => 'grouped');
    }
}