<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 03.08.17
 * Time: 12:56
 */

class Webmeridian_ScriptNewAttribute_Model_AttributeUpdate
{

    public function updateAttribute($attributeData, $productTypes, array $storeIds)
    {
        $productIds = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', $productTypes)
            ->getAllIds();

        foreach ($storeIds as $storeId){
            Mage::getSingleton('catalog/product_action')
                ->updateAttributes($productIds, $attributeData, $storeId);
        }
    }
}