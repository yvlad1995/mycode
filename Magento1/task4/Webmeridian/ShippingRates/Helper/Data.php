<?php

/**
 * Class Webmeridian_ShippingRates_Helper_Data
 */
class Webmeridian_ShippingRates_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    protected $_rates = array();

    /**
     * @param $rates
     * @return array
     */
    public function setRatesForShipping($rates)
    {
        if($rates['premiumrate'])
        {
            $this->setPremiumrateFirstCode($rates);
            $i = 0;
            foreach ($this->_rates['premiumrate'] as $key => $rate)
            {
                if($rate->getCode() == 'premiumrate_FedEx2day' && $i == 0)
                {
                    $i++;
                    array_unshift($this->_rates['premiumrate'], $rate);
                    unset($this->_rates['premiumrate'][$key]);
                }
            }
            return $this->_rates;
        }
        return $rates;
    }

    /**
     * @param $rates
     */
    protected function setPremiumrateFirstCode($rates)
    {
        $this->_rates['premiumrate'] = $rates['premiumrate'];

        unset($rates['premiumrate']);

        foreach ($rates as $key => $rate)
        {
            $this->_rates[$key] = $rate;
        }
    }

    /**
     * @param $rate
     * @return string
     */
    public function getClassForCollect($rate)
    {
        if(preg_match("/^premiumrate_/", $rate->getCode()) && preg_match("/_Collect$/", $rate->getCode())){
            return 'require_collect';
        }
        return '';
    }

}