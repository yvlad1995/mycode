<?php

/**
 * Class Brander_OurStores_Adminhtml_OurstoresController
 */
class Brander_OurStores_Adminhtml_OurstoresController extends Mage_Adminhtml_Controller_Action
{
    protected $_storeId;

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initOurStore();
        $this->_initMap();
        $this->_initDescription();
        $this->loadLayout()->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        $data  = $this->getRequest()->getPost();

        $ourstore = $this->_initOurStore();
        $storeCode  = $this->getRequest()->getParam('store');
        if (!$storeCode) {
            $this->_getSession()->addError($this->_getHelperDescription()->__('Warning! To save, you must select a store!'))
                ->setStoreData($data);
            return $this->_redirect('*/*/edit', array('id' => $ourstore->getId()));
        }

        if ($data) {
            try {
                $ourstore->setUrlKey();
                $ourstore->addData($data['ourstore']);
                $ourstore->save();

                $this->_storeId = $ourstore->getId();

                $description = $this->_initDescription();
                $dataDescription = $this->_getHelperDescription()->getDataDescription($data['ourstore']);
                $description->addData($dataDescription);
                $description->save();

                $map = $this->_initMap();
                $map->addData($data['map']);
                $map->save();

                $this->_getSession()->addSuccess($this->__('The Store has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setStoreData($data);
                return $this->_redirect('*/*/edit', array('id' => $ourstore->getId() ,'store' => $storeCode));

            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                return $this->_redirect('*/*/edit', array('id' => $ourstore->getId(), 'store' => $storeCode));
            }
        }else{
            $this->_getSession()->addError($this->__('Please fill all required field'));
        }

        if ($this->getRequest()->getParam('back')) {
            return $this->_redirect('*/*/edit', array('id' => $ourstore->getId(), 'store' => $storeCode));
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        try {
            $store = $this->_initOurStore();
            $store->delete();
            $this->_getSession()->addSuccess($this->__('The Store has been deleted.'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function massDeleteAction()
    {
        $massactionKey = $this->getRequest()->getParam('massaction_prepare_key');
        $storeIds = $this->getRequest()->getParam($massactionKey) ? $this->getRequest()->getParam($massactionKey) : [];
        if(count($storeIds) > 0) {
            try {

                $store = $this->_initOurStore();
                $store->deleteStoreByIds($storeIds);
                $this->_getSession()->addSuccess($this->__('The Store(s) has been deleted.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        return $this->_redirect('*/*/index');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _initOurStore()
    {
        $storeId = $this->_getStoreId();

        $store = Mage::getModel('brander_ourstores/store');

        if ($storeId) {
            $store->load($storeId);
        }

        Mage::register('current_store', $store);

        return $store;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     *
     * Init after save Store
     */
    protected function _initMap()
    {
        $storeId = $this->_getStoreId();

        $map = Mage::getModel('brander_ourstores/map');

        if ($storeId) {
            $map->load($storeId,'ourstore_id');
        } else {
            $storeId = $this->_storeId;
        }

        $map->setOurstoreId($storeId);

        $latitude  = $map->getLatitude() ? $map->getLatitude() : $this->_getHelperMap()->getLatitude();
        $longitude = $map->getLongitude() ? $map->getLongitude(): $this->_getHelperMap()->getLongitude();

        $map->setLatitude($latitude);
        $map->setLongitude($longitude);

        Mage::register('current_map', $map);

        return $map;

    }

    /**
     * @return false|Mage_Core_Model_Abstract
     *
     * Init after save Store
     */
    protected function _initDescription()
    {
        $storeId = $this->_getStoreId();
        $description = Mage::getModel('brander_ourstores/description');

        $storeCode  = $this->getRequest()->getParam('store');

        if ($storeId) {
            $collection = $description->getCollection();
            $description = $collection
                ->addFieldToFilter('ourstore_id', $storeId)
                ->addFieldToFilter('store_code', $storeCode)->getFirstItem();
        } else {
            $storeId = $this->_storeId;
        }

        $description->setOurstoreId($storeId);

        $description->setStoreCode($storeCode);

        Mage::register('current_description', $description);

        return $description;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * @return mixed
     */
    protected function _getStoreId()
    {
        return $this->getRequest()->getParam('id');
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelperMap()
    {
        return Mage::helper('brander_ourstores/map');
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelperDescription()
    {
        return Mage::helper('brander_ourstores/description');
    }
}