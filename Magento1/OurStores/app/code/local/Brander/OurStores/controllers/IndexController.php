<?php

/**
 * Class Brander_OurStores_IndexController
 */
class Brander_OurStores_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest() && $request->getParam('p')) {
            Mage::register('ourstore_id', $request->getParam('ourstore_id'));
            $pager = $this->getLayout()->createBlock('brander_ourstores/view_toolbar_pager','pager')
                ->setTemplate('brander/ourstores/view/pager.phtml');
            $block = $this->getLayout()->createBlock('brander_ourstores/view_toolbar')
                ->setTemplate('brander/ourstores/view/review-list.phtml')->setChild('pager', $pager);
            $this->getResponse()->setBody($block->toHtml());
            return true;
        }

        $ourstore = $this->_initStore();

        $this->_initDescription();

        if(!$ourstore->getId()) {
            return $this->_redirect('noroute');
        }

        $storeCode = Mage::app()->getStore()->getCode();

        $this->loadLayout();

        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbsBlock->addCrumb('general', array(
            'label'    => Mage::helper('brander_ourstores')->__('General'),
            'link'     => Mage::getUrl(),
        ));

        $breadcrumbsBlock->addCrumb('ourstores', array(
            'label'    => Mage::helper('brander_ourstores')->__('Our Stores'),
            'link'     => Mage::getUrl('ourstores'),
        ));

        $breadcrumbsBlock->addCrumb('city', array(
            'label'    => $ourstore->getCityName($ourstore->getCityId(), $storeCode),
            'link'     => Mage::getUrl('ourstores'),
        ));
        $breadcrumbsBlock->addCrumb('address', array(
            'label'    => $ourstore->getAddress(),
            'readonly' => true,
        ));

        $this->renderLayout();
    }



    /**
     * return city by filter
     */
    public function cityFilterAction()
    {
        $cityId = $this->getRequest()->getParam('city_id');

        if($cityId){
            Mage::register('city_id', $cityId);
        }

        $cities = $this->loadLayout()
            ->getLayout()
            ->createBlock('brander_ourstores/city', 'city')
            ->setTemplate('brander/ourstores/list/city.phtml')->toHtml();

        $this->getResponse()->setBody($cities);
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _initStore()
    {
        $storeId = $this->getRequest()->getParam('id');

        $store = Mage::getModel('brander_ourstores/store');

        if ($storeId) {
            $store->load($storeId);
        }

        Mage::register('current_store', $store);

        return $store;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _initDescription()
    {
        $store = Mage::registry('current_store');

        if(!isset($store)) {
            $store = $this->_initStore();
        }

        $storeId = $store->getId();

        $storeCode = Mage::app()->getStore()->getCode();

        $description = $this->_getDescriptionCollection()
            ->addFieldToFilter('ourstore_id', $storeId)
            ->addFieldToFilter('store_code', $storeCode)->getFirstItem();

        if(!count($description->getData())) {
            $description = $this->_getDescriptionCollection()
                ->addFieldToFilter('ourstore_id', $storeId)
                ->addFieldToFilter('store_code', 'admin')->getFirstItem();
        }

        Mage::register('current_description', $description);

        return $description;
    }

    protected function _getDescriptionCollection()
    {
        return Mage::getModel('brander_ourstores/description')
            ->getCollection();

    }

}