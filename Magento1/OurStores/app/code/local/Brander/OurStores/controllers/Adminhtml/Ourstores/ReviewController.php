<?php

/**
 * Class Brander_OurStores_Adminhtml_Ourstores_ReviewController
 */
class Brander_OurStores_Adminhtml_Ourstores_ReviewController extends Mage_Adminhtml_Controller_Action
{
    public function editAction()
    {
        $this->_initReview();
        $this->loadLayout()
            ->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        $data  = $this->getRequest()->getPost();

        if ($data) {
            try {
                $review = $this->_initReview();
                $review->addData($data);
                $review->save();

                $this->_getSession()->addSuccess($this->__('The Review has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setStoreData($data);

            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }

        return $this->getRedirectUrl();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        try {
            $review = $this->_initReview();
            $review->delete();
            $this->_getSession()->addSuccess($this->__('The Review has been deleted.'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->getRedirectUrl();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function massDeleteAction()
    {
        $massactionKey = $this->getRequest()->getParam('massaction_prepare_key');
        $reviewIds = $this->getRequest()->getParam($massactionKey);
        if(count($reviewIds) > 0) {
            try {
                $review = $this->_initReview();
                $review->deleteReviewByIds($reviewIds);
                $this->_getSession()->addSuccess($this->__('The Review(s) has been deleted.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        return $this->getRedirectUrl();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function massStatusAction()
    {
        $massactionKey = $this->getRequest()->getParam('massaction_prepare_key');
        $reviewIds     = $this->getRequest()->getParam($massactionKey);
        $status        = $this->getRequest()->getParam('status');
        if(count($reviewIds) > 0) {
            try {
                $review = $this->_initReview();
                $review->changeStatus($reviewIds, $status);
                $this->_getSession()->addSuccess($this->__('The Review(s) status has been changed.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        return $this->getRedirectUrl();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    protected function getRedirectUrl()
    {
        $storeId = $this->getRequest()->getParam('store_id');

        if($storeId)
        {
            return $this->_redirect('*/ourstores/edit', array('id' => $storeId));
        }
        return $this->_redirect('*/*/reviews');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _initReview()
    {
        $reviewId = $this->getRequest()->getParam('id');

        $review = Mage::getModel('brander_ourstores/review');

        if ($reviewId) {
            $review->load($reviewId);
        }

        Mage::register('current_review', $review);

        return $review;
    }

    /**
     * Grid all reviews
     */
    public function reviewsAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    /**
     * Grid all reviews
     */
    public function reviewsGridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Grid reviews for current store
     */
    public function reviewAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    /**
     * Grid reviews for current store
     */
    public function reviewGridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}