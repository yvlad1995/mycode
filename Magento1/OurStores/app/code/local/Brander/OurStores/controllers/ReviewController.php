<?php

/**
 * Class Brander_OurStores_ReviewController
 */
class Brander_OurStores_ReviewController extends Mage_Core_Controller_Front_Action
{
    const LENGTH_CUSTOMER_NAME = 100;

    const LENGTH_CUSTOMER_DESCRIPTION = 500;

    /**
     * @return bool
     */
    public function saveAction()
    {
        if(!$this->_validateData() || !$this->_validateFormKey()) {
            $this->getResponse()->setHeader('HTTP/1.1','404 Data Not Valid');
            $this->getResponse()->setHeader('Status','404 Data Not Valid');
            echo Mage::helper('brander_ourstores')->__('Please fill all fields');
            return false;
        }

        $data = $this->getRequest()->getParams();

        if ($data) {
            try {
                $review = Mage::getModel('brander_ourstores/review');
                $review->addData($data);
                $review->save();
            } catch (Mage_Core_Exception $e) {
                $this->getResponse()->setHeader('HTTP/1.1',$e->getMessage());
                $this->getResponse()->setHeader('Status',$e->getMessage());
                Mage::logException($e);

            } catch (Exception $e) {
                Mage::logException($e);
                $this->getResponse()->setHeader('HTTP/1.1',$e->getMessage());
                $this->getResponse()->setHeader('Status',$e->getMessage());
            }
        }
        echo Mage::helper('brander_ourstores')->__('Your comment will be displayed after moderator verification');

        return true;
    }

    /**
     * @return bool
     */
    protected function _validateData()
    {
        $data = $this->getRequest()->getParams();
        if (empty($data['ourstore_id']) ||
            empty($data['customer_name']) ||
            empty($data['review_description']) ||
            empty($data['rating'])) {
            return false;
        }

        if(
            strlen($data['customer_name']) > self::LENGTH_CUSTOMER_NAME ||
            strlen($data['review_description']) > self::LENGTH_CUSTOMER_DESCRIPTION
        ) {
            return false;
        }
        return true;
    }
}