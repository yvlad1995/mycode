<?php

/**
 * Class Brander_OurStores_Adminhtml_Ourstores_ImageController
 */
class Brander_OurStores_Adminhtml_Ourstores_ImageController extends Mage_Adminhtml_Controller_Action
{
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initImage();
        $this->loadLayout()->_setActiveMenu('brander_shop');
        $this->renderLayout();
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        $data  = $this->getRequest()->getPost();
        $storeId = $this->getRequest()->getParam('store_id');

        if ($data) {
            try {
                $image = $this->_initImage();
                $image->addData($data);
                $image->setOurstoreId($storeId);
                $image->save();

                $this->_getSession()->addSuccess($this->__('The Image has been saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                    ->setStoreData($data);

            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }

        return $this->_redirect('*/ourstores/edit',array('id' => $this->getRequest()->getParam('store_id')));
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        try {
            $image = $this->_initImage();
            $image->delete();
            $this->_getSession()->addSuccess($this->__('The Image has been deleted.'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('*/ourstores/edit',array('id' => $this->getRequest()->getParam('store_id')));
    }

    /**
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function massDeleteAction()
    {
        $massactionKey = $this->getRequest()->getParam('massaction_prepare_key');
        $imageIds = $this->getRequest()->getParam($massactionKey);
        if(count($imageIds) > 0) {
            try {
                $image = $this->_initImage();
                $image->deleteImageByIds($imageIds);
                $this->_getSession()->addSuccess($this->__('The Image(s) has been deleted.'));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        return $this->_redirect('*/ourstores/edit',array('id' => $this->getRequest()->getParam('store_id')));
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _initImage()
    {
        $imageId = $this->getRequest()->getParam('id');

        $image = Mage::getModel('brander_ourstores/image');

        if ($imageId) {
            $image->load($imageId);
        }

        Mage::register('current_image', $image);

        return $image;
    }

    public function imagesAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function imagesGridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}