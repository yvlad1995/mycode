<?php
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_ourstores/description'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'work_time_comment',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => false,
        ),
        'Work Time Comment'
    )
    ->addColumn(
        'address',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => false,
        ),
        'Store Address'
    )
    ->addColumn(
        'description',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable'  => false,
        ),
        'Description'
    )
    ->addColumn(
        'store_code',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'default'   => 'admin',
        ),
        'Store Code'
    )
    ->addColumn(
        'brands_description',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable'  => false,
        ),
        'Brands Description'
    )->addColumn(
        'ourstore_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => false,
        ),
        'Ourstore Id'
    )->addForeignKey(
        $installer->getFkName(
            'brander_ourstores/description',
            'ourstore_id',
            'brander_ourstores/ourstore',
            'entity_id'
        ),
        'ourstore_id', $installer->getTable('brander_ourstores/ourstore'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    );

$installer->getConnection()->createTable($table);

$installer->getConnection()->addForeignKey(
    $installer->getFkName('brander_ourstores/description', 'store_code', 'core_store', 'code'),
    $installer->getTable('brander_ourstores/description'),
    'store_code',
    $installer->getTable('core_store'),
    'code'
);

$installer->endSetup();