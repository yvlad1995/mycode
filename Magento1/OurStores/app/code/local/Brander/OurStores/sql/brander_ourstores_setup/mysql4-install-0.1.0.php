<?php
$installer = $this;

$installer->startSetup();
$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_ourstores/ourstore'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
        ),
        'Status'
    )
    ->addColumn(
        'image_preview',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => true,
        ),
        'Image Preview'
    )
    ->addColumn(
        'work_time_from',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => false,
        ),
        'Work Time From'
    )
    ->addColumn(
        'work_time_to',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => false,
        ),
        'Work Time To'
    )->addColumn(
        'rating',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        null,
        array(
            'nullable'  => true,
            'precision' => 4,
            'scale'     => 3,
        ),
        'Store Rating'
    )->addColumn(
        'store_review_qty',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => true,
        ),
        'Store Review Qty'
    )->addColumn(
        'city_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => false,
        ),
        'City Id'
    )->addColumn(
        'brands',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable'  => false,
        ),
        'Store Brands'
    )->addColumn(
        'url_key',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => false,

        ),
        'Store Url Key'
    )->addForeignKey(
        $installer->getFkName(
            'brander_ourstores/ourstore',
            'city_id',
            'brander_shipping/ukraine_cities',
            'entity_id'
        ),
        'city_id', $installer->getTable('brander_shipping/ukraine_cities'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_ourstores/review'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'customer_name',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => true,
        ),
        'Customer Name'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
            'default'   => 2,
        ),
        'Status'
    )
    ->addColumn(
        'review_description',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable'  => true,
        ),
        'Review Description'
    )
    ->addColumn(
        'rating',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
        ),
        'Rating'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT
        ),
        'Date Created'
    )
    ->addColumn(
        'ourstore_id',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
        ),
        'Our Store Id'
    )
    ->addForeignKey(
        $installer->getFkName(
            'brander_ourstores/review',
            'ourstore_id',
            'brander_ourstores/ourstore',
            'entity_id'
        ),
        'ourstore_id', $installer->getTable('brander_ourstores/ourstore'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_ourstores/map'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'longitude',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => true,
        ),
        'Longitude'
    )
    ->addColumn(
        'latitude',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => true,
        ),
        'Latitude'
    )
    ->addColumn(
        'ourstore_id',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
        ),
        'Our Store Id'
    )
    ->addForeignKey(
        $installer->getFkName(
            'brander_ourstores/map',
            'ourstore_id',
            'brander_ourstores/ourstore',
            'entity_id'
        ),
        'ourstore_id', $installer->getTable('brander_ourstores/ourstore'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_ourstores/image'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'image_path',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'nullable'  => true,
        ),
        'Image Path'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable'  => true,
        ),
        'Image Position'
    )
    ->addColumn(
        'ourstore_id',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'nullable'  => true,
        ),
        'Our Store Id'
    )
    ->addForeignKey(
        $installer->getFkName(
            'brander_ourstores/image',
            'ourstore_id',
            'brander_ourstores/ourstore',
            'entity_id'
        ),
        'ourstore_id', $installer->getTable('brander_ourstores/ourstore'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);

$installer->getConnection()->createTable($table);

$installer->endSetup();