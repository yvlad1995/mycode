<?php
$this->startSetup();
$this->getConnection()
    ->modifyColumn($this->getTable('brander_ourstores/ourstore'), 'work_time_to', array('type' => Varien_Db_Ddl_Table::TYPE_TEXT))
    ->modifyColumn($this->getTable('brander_ourstores/ourstore'), 'work_time_from', array('type' => Varien_Db_Ddl_Table::TYPE_TEXT));
$this->endSetup();