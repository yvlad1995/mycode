<?php

/**
 * Class Brander_OurStores_Helper_Data
 */
class Brander_OurStores_Helper_Data extends Mage_Core_Helper_Abstract
{

    const URL_REWRITE  = 'brander_ourstores/general/url_rewrite';

    /**
     * @param $path
     * @return null|string
     */
    public function getStoreConfig($path)
    {
        return Mage::app()->getStore()->getConfig($path);
    }

    /**
     * @return array
     * use in layout breadcrumbs on ourstores page
     */
    public function getHomeUrl()
    {
        return array(
            "label" => $this->__('Home'),
            "title" => $this->__('Home Page'),
            "link" => Mage::getUrl()
        );
    }

    /**
     * @param $month
     * @return string
     */
    public function getMonthLocale($month)
    {
        return $this->__($month);
    }

    /**
     * @return null|string
     */
    public function getUrlRewriteOurstores()
    {
        return '#^/' . $this->getStoreConfig(self::URL_REWRITE) . '/#';
    }
}