<?php

/**
 * Class Brander_OurStores_Helper_Map
 */
class Brander_OurStores_Helper_Review extends Brander_OurStores_Helper_Data
{
    /**
     * @return string
     */
    public function getMessageNotification()
    {
        return $this->__('%sOne or more users commented on your store... %s', '<strong style="font-size: 16px">', '</strong>');
    }

    /**
     * @param $qty
     * @return string
     */
    public function getTextQtyReviews($qty)
    {
        switch (true) {
            case ($qty == 1):
                return $this->__('Review');
            case ($qty <= 4):
                return $this->__('Reviews');
            case ($qty > 4):
                return $this->__('Reviewers');
            default:
                return $this->__('Review');
        }
    }
}