<?php

/**
 * Class Brander_OurStores_Helper_Map
 */
class Brander_OurStores_Helper_Map extends Brander_OurStores_Helper_Data
{
    const LATITUDE  = 'brander_ourstores/config/latitude';

    const LONGITUDE = 'brander_ourstores/config/longitude';

    const API_KEY = 'brander_ourstores/config/api_key';

    const API_ADDRESS = 'brander_ourstores/config/api_address';

    /**
     * @return null|string
     */
    public function getLatitude()
    {
        return $this->getStoreConfig(self::LATITUDE);
    }

    /**
     * @return null|string
     */
    public function getLongitude()
    {
        return $this->getStoreConfig(self::LONGITUDE);
    }

    /**
     * @return null|string
     */
    public function getApiKey()
    {
        return $this->getStoreConfig(self::API_KEY);
    }

    /**
     * @return null|string
     */
    public function getApiAddress()
    {
        return $this->getStoreConfig(self::API_ADDRESS);
    }

    /**
     * @param $param
     * @return bool|string
     */
    public function getMapsApiUrl($param)
    {
        if (is_string($param)) {
            return $this->getApiAddress() . '?key=' . $this->getApiKey() .(string) $param;
        }
        return false;
    }
}