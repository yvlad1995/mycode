<?php

/**
 * Class Brander_OurStores_Helper_Image
 */
class Brander_OurStores_Helper_Image extends Brander_OurStores_Helper_Data
{
    protected $_imagePath;

    /**
     * @param $image
     * @return bool
     */
    public function uploadImage($image)
    {
        if (!empty($_FILES['image']['name'])) {
            try {
                $this->removeImage($image);
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions($this->getAllowedExtension());
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $fileName = time() . 'store' . $this->getImageType($_FILES['image']);
                $uploader->save($this->getPathImageDirectory() . DS . 'brander' . DS . 'ourstores', $fileName);
                $this->_imagePath = 'brander' . DS . 'ourstores' . DS . $fileName;
            }catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Image don\'t uploaded');
            }
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getImagePath()
    {
        return $this->_imagePath;
    }

    /**
     * @return string
     */
    public function getPathImageDirectory()
    {
        return Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * @return string
     */
    public function getPathImageUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * @param $image
     * @return string
     */
    public function getImageType($image)
    {
        $types = $this->getAllowedExtension();
        foreach ($types as $type) {
            if (strstr($image['type'], $type)) {
                return '.' .$type;
            }
        }

        return '.png';
    }

    /**
     * @return array
     */
    public function getAllowedExtension()
    {
        return ['jpg', 'jpeg', 'gif', 'png'];
    }

    /**
     * @param $image
     */
    public function removeImage($image)
    {
        $imagePath = $this->getPathImageDirectory() . DS . $image;

        if (!is_dir($imagePath) && file_exists($imagePath)) {
            unlink($imagePath);
        }
    }
}