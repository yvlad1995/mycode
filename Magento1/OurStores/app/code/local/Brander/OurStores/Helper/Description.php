<?php

/**
 * Class Brander_OurStores_Helper_Description
 */
class Brander_OurStores_Helper_Description extends Brander_OurStores_Helper_Data
{
    /**
     * @param $params
     * @return array
     */
    public function getDataDescription($params)
    {
        $data = [];

        $data['work_time_comment']  = $params['work_time_comment'];
        $data['address']            = $params['address'];
        $data['description']        = $params['description'];
        $data['brands_description'] = $params['brands_description'];

        return $data;
    }
}