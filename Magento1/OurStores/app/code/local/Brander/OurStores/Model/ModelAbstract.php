<?php

/**
 * Class Brander_OurStores_Model_ModelAbstract
 */
abstract class Brander_OurStores_Model_ModelAbstract extends Mage_Core_Model_Abstract
{
    /**
     * @param $aliasImagePath
     * @return Mage_Core_Model_Abstract
     *
     * save store image preview and save images store
     */
    protected function _saveImage($aliasImagePath)
    {
        $_imageHelper = $this->_imageHelper();

        $data = Mage::app()->getRequest()->getPost();

        $image = !empty($_FILES['image']) && $_FILES['image']['size'] > 0 ? $_FILES['image']['name'] : null;
        if(!$image) {
            $image = isset($this->_data[$aliasImagePath]) ? $this->_data[$aliasImagePath] : null;
        }

        $imagePath = !empty($this->_data[$aliasImagePath]) ? $this->_data[$aliasImagePath] : '';

        if ($_imageHelper->uploadImage($image)) {
            $_imageHelper->removeImage($imagePath);
            $imagePreview = $_imageHelper->getImagePath();

            $this->_data[$aliasImagePath] = $imagePreview ? $imagePreview : false;
        } elseif (isset($data['image']['delete']) && $data['image']['delete']) {
            $_imageHelper->removeImage($imagePath);
            $this->_data[$aliasImagePath] = '';
        }
    }

    /**
     * @param $aliasImagePath
     * @return Mage_Core_Model_Abstract
     */
    protected function _deleteImage($aliasImagePath)
    {
        $image = $this->getData($aliasImagePath);
        if ($image) {
            $this->_imageHelper()->removeImage($image);
        }
    }
}