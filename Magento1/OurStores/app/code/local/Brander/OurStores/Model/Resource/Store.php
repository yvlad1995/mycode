<?php

/**
 * Class Brander_OurStores_Model_Resource_Store
 */
class Brander_OurStores_Model_Resource_Store extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/ourstore', 'entity_id');
    }
}