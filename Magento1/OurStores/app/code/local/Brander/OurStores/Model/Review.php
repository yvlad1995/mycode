<?php

/**
 * Class Brander_OurStores_Model_Review
 */
class Brander_OurStores_Model_Review extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/review');
    }

    /**
     * @param array $reviewIds
     * this function nedded for massaction delete reviews
     */
    public function deleteReviewByIds($reviewIds = array())
    {
        foreach ($reviewIds as $reviewId) {
            $review = $this->load($reviewId);
            $review->delete();
        }
    }

    /**
     * @param $reviewIds
     * @param $status
     * this function nedded for massaction delete reviews
     */
    public function changeStatus($reviewIds, $status)
    {
        foreach ($reviewIds as $reviewId) {
            $review = $this->load($reviewId);
            $review->setStatus($status);
            $review->save();
        }
    }

    /**
     * @return Mage_Core_Model_Abstract
     * edit review average rating for store
     */
    protected function _afterSave()
    {
        Mage::dispatchEvent('ourstore_review_changed', array('ourstore_id' => $this->getOurstoreId()));
        Mage::app()->getCacheInstance()->cleanType('block_html');
        return parent::_afterSave();
    }

    /**
     * @return Mage_Core_Model_Abstract
     * edit review average rating for store
     */
    protected function _afterDelete()
    {
        Mage::dispatchEvent('ourstore_review_changed' , array('ourstore_id' => $this->getOurstoreId()));
        return parent::_afterDelete();
    }
}