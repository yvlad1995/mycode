<?php

/**
 * Class Brander_OurStores_Model_Observer
 */
class Brander_OurStores_Model_Observer
{
    protected $_reviews;

    const STATUS_REVIEW = 1;

    /**
     * @param Varien_Event_Observer $observer
     *
     * change average rating for store by status approved review
     */
    public function changeRatingStore(Varien_Event_Observer $observer)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            $storeId = $observer->getEvent()->getOurstoreId();

            $this->_reviews = $this->_getReviewModel()
                ->getCollection()
                ->addFieldToFilter('ourstore_id', $storeId)
                ->addFieldToFilter('status', self::STATUS_REVIEW);

            $store = $this->_getStoreModel()->load($storeId);
            $store->setRating($this->_getSummRatingReview());
            $store->setStoreReviewQty($this->_getStoreReviewQty());
            $store->save();
        }
    }

    /**
     * @return float|int|null
     */
    protected function _getSummRatingReview()
    {
        $rating = 0;

        $count = $this->_getStoreReviewQty();

        if (!$count) {
           return null;
        }

        foreach ($this->_reviews as $review) {
            $rating += $review->getRating();
        }

        return $rating / $count;
    }

    /**
     * @return int
     */
    protected function _getStoreReviewQty()
    {
        return count($this->_reviews);
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getReviewModel()
    {
        return Mage::getModel('brander_ourstores/review');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getStoreModel()
    {
        return Mage::getModel('brander_ourstores/store');
    }

    /**
     * @param Varien_Event_Observer $observer
     * Add Url Rewrite for General Page Ourstores
     */
    public function initControllerRouters(Varien_Event_Observer $observer)
    {
        $urlRewrite = $this->_getHelper()->getUrlRewriteOurstores();
        $from = !empty(Mage::getConfig()->getNode('global/rewrite/ourstores')->from) ?
            Mage::getConfig()->getNode('global/rewrite/ourstores')->from : '';
        if($urlRewrite && $urlRewrite != $from) {
            Mage::getConfig()->getNode('global/rewrite/ourstores')->from = $urlRewrite;
        }
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_ourstores');
    }
}