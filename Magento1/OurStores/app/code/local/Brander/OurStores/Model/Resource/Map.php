<?php

/**
 * Class Brander_OurStores_Model_Resource_Map
 */
class Brander_OurStores_Model_Resource_Map extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/map', 'entity_id');
    }
}