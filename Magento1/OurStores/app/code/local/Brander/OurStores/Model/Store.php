<?php

/**
 * Class Brander_OurStores_Model_Store
 */
class Brander_OurStores_Model_Store extends Brander_OurStores_Model_ModelAbstract
{
    const IMAGE_PATH = 'image_preview';

    protected $_descriptionStore = null;

    protected function _construct()
    {
        $this->_init('brander_ourstores/store');
    }

    /**
     * @param string $code
     * @return mixed
     */
    public function toOptionArrayCity($code = 'ru')
    {
        if (is_array($this->_getCitiesModel()->getArrayCities($code))) {
            $cities = $this->_getCitiesModel()->getArrayCities($code);
            $cities[0] = array(
                'value' => '',
                'label' => $this->_imageHelper()->__('Select City')
            );
            return  $cities;
        }
    }

    /**
     * @return array
     * return brands array
     */
    public function toOptionArrayBrand()
    {
        $collection = $this->getManufacturerOptionValue();

        $options = [];
        foreach ($collection as $item) {
            $label = unserialize($item->getTitle());
            $options[] = [
                'value' => $item->getOptionId(),
                'label' => $label[0]
            ];
        }

        return $options;
    }

    /**
     * @return mixed
     * return manufacturer filter option value
     */
    public function getManufacturerOptionValue()
    {
        $filter = $this->getFilterManufacturer();
        return Mage::getResourceModel('brander_layerednavigation/value_collection')
            ->addFieldToFilter('filter_id', $filter->getFilterId());
    }

    /**
     * @return Mage_Core_Model_Abstract
     * return filter manufacturer
     */
    public function getFilterManufacturer()
    {
        $entityAttribute = Mage::getModel('eav/entity_attribute');
        $attributeId     = $entityAttribute->getIdByCode('catalog_product', 'manufacturer');
        return Mage::getModel('brander_layerednavigation/filter')->load($attributeId, 'attribute_id');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getCitiesModel()
    {
        return Mage::getModel('brander_shipping/cities');
    }

    /**
     * @param $cityId
     * @param string $code
     * @return bool
     */
    public function getCityName($cityId, $code = 'ru')
    {
        $cities = $this->toOptionArrayCity($code);
        return !empty($cities[$cityId]) ? $cities[$cityId] : false;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        $_imageCollection = $this->_getImageModel()
                            ->getCollection()
                            ->addFieldToFilter('ourstore_id', $this->getId());

        foreach ($_imageCollection as $image) {
            $this->_imageHelper()->removeImage($image->getImagePath());
        }

        $this->_deleteImage(self::IMAGE_PATH);

        $this->_deleteUrlRewrite();

        return parent::_beforeDelete();
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _imageHelper()
    {
        return Mage::helper('brander_ourstores/image');
    }

    /**
     * @param array $storeIds
     * for delete stores massaction
     */
    public function deleteStoreByIds($storeIds = array())
    {
        foreach ($storeIds as $storeId) {
            $store = $this->load($storeId);
            $store->delete();
        }
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getImageModel()
    {
        return Mage::getModel('brander_ourstores/image');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getDescriptionModel()
    {
        return Mage::getModel('brander_ourstores/description');
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterLoad()
    {
        $this->setBrands(unserialize($this->getBrands()));
        return parent::_afterLoad();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        $this->_saveUrlRewrite();
        Mage::app()->getCacheInstance()->cleanType('block_html');
        return parent::_afterSave();
    }

    /**
     * @return Mage_Core_Model_Abstract
     * serialize brands before save
     * save image
     * set url key for store
     */
    protected function _beforeSave()
    {
        $this->setBrands(serialize($this->getBrands()));
        $this->_saveImage(self::IMAGE_PATH);
        if ((!$this->getOrigData() || $this->getUrlKey() != $this->getOrigData('url_key')) &&
            $this->getAddress() || !$this->getUrlKey()) {

            if ($this->getUrlKey() != $this->getOrigData('url_key') &&
                strlen($this->getUrlKey()) > 0) {

                $this->setUrlKey($this->getUrlKey());
            }else{
                $this->setUrlKey($this->generateNewUrl($this->getStoreCode()));
            }
        }
        if(!$this->getUrlKey()) {
            throw new Exception($this->_imageHelper()->__('Field "Store Seo Url" cannot be empty!'));
        }
        return parent::_beforeSave();
    }

    /**
     * @return mixed
     * generate url for store
     */
    public function generateNewUrl($storeCode = false)
    {
        if ($this->getOrigData('url_key') && strlen($this->getUrlKey()) > 0) {
            return $this->getUrlKey();
        } else {
            return Mage::getModel('catalog/product_url')->formatUrlKey($this->getAddress($storeCode));
        }
    }


    /**
     * This function create new or edit
     * url rewrite for store link
     */
    protected function _saveUrlRewrite()
    {
        $stores = Mage::app()->getStores();

        $sql = "INSERT INTO " . $this->_getTableNameUrlRewrite() . "
                  (`store_id`, `request_path`, `target_path`, `is_system`) VALUES ";

        foreach ($stores as $store) {
            $sql .= " (" . $this->_getConnectionWrite()->quote($store->getStoreId()) . ", " .
                $this->_getConnectionWrite()->quote($this->generateNewUrl($store->getCode())) . ", 
            'ourstores/index/view/id/" . $this->getId() . "', '0') , ";
        }

        $sql = substr($sql, 0, -2);

        try {
            $this->_deleteUrlRewrite();
            $this->_getConnectionWrite()->query($sql);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('brander_ourstores')->__('This Address Already! Please write new address.'));
        }
    }

    /**
     * This function delete old url rewrite in
     * core_url_rewrite table
     */
    protected function _deleteUrlRewrite()
    {
        $stores = Mage::app()->getStores();

        $sql = "DELETE FROM `core_url_rewrite` WHERE ";
        foreach ($stores as $store) {
             $sql .= "(`target_path` = 'ourstores/index/view/id/" . $this->getId() . "' 
            AND `store_id` = " . $this->_getConnectionWrite()->quote($store->getStoreId()) . ") OR ";
        }
        $sql = substr($sql, 0, -3);

        try {
            $this->_getConnectionWrite()->query($sql);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getResourceModel()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @return mixed
     */
    protected function _getConnectionWrite()
    {
        return $this->_getResourceModel()->getConnection('core_write');
    }

    protected function _getConnectionRead()
    {
        return $this->_getResourceModel()->getConnection('core_read');
    }

    /**
     * @return mixed
     */
    protected function _getTableNameUrlRewrite()
    {
        return $this->_getResourceModel()->getTableName('core/url_rewrite');
    }

    /**
     * @param bool $storeCode
     * @return mixed
     */
    public function getAddress($storeCode = false)
    {
        $descriptionStore = $this->_getDescriptionStore($storeCode);
        return $descriptionStore->getAddress();
    }

    /**
     * @param bool $storeCode
     * @return false|Mage_Core_Model_Abstract|null
     */
    protected function _getDescriptionStore($storeCode = false)
    {
        if (!$storeCode && $storeCode = Mage::app()->getStore()->getCode()) {
                $storeCode == 'admin' ? 'ru' : $storeCode;
        }
        $this->_descriptionStore = $this->_getDescriptionModel();

        $query = 'SELECT * FROM ' . $this->_getResourceModel()->getTableName('brander_ourstores_description') .
            ' WHERE store_code=' . $this->_getConnectionRead()->quote($storeCode) .
            ' AND `ourstore_id`=' . $this->_getConnectionRead()->quote($this->getId());

        try{
            $data = $this->_getConnectionRead()->fetchAll($query);
        }catch (Exception $e) {
            Mage::log($e, null, 'ourstore.log');
        }

        $this->_descriptionStore->setData(array_shift($data));

        return $this->_descriptionStore;
    }
}