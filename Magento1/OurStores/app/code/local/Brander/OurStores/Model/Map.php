<?php

/**
 * Class Brander_OurStores_Model_Map
 */
class Brander_OurStores_Model_Map extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/map');
    }
}