<?php

/**
 * Class Brander_OurStores_Model_Resource_Description_Collection
 */
class Brander_OurStores_Model_Resource_Description_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/description');
    }
}