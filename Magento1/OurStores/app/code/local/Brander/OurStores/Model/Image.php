<?php

/**
 * Class Brander_OurStores_Model_Image
 */
class Brander_OurStores_Model_Image extends Brander_OurStores_Model_ModelAbstract
{
    const IMAGE_PATH = 'image_path';

    protected function _construct()
    {
        $this->_init('brander_ourstores/image');
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeDelete()
    {
        $this->_deleteImage(self::IMAGE_PATH);
        return parent::_beforeDelete();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        $this->_saveImage(self::IMAGE_PATH);
        return parent::_beforeSave();
    }

    /**
     * @param array $imageIds
     * for massaction
     */
    public function deleteImageByIds($imageIds = array())
    {
        foreach ($imageIds as $imageId) {
            $review = $this->load($imageId);
            $review->delete();
        }
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _imageHelper()
    {
        return Mage::helper('brander_ourstores/image');
    }

    protected function _afterSave()
    {
        Mage::app()->getCacheInstance()->cleanType('block_html');
        return parent::_afterSave();
    }
}