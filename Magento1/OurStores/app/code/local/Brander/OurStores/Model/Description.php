<?php

/**
 * Class Brander_OurStores_Model_Description
 */
class Brander_OurStores_Model_Description extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/description');
    }
}