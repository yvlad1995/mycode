<?php

/**
 * Class Brander_OurStores_Model_Resource_Store_Collection
 */
class Brander_OurStores_Model_Resource_Store_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/store');
    }

    /**
     * @return $this
     * Added Description for multistore
     */
    public function joinDescription()
    {
        $storeCode = Mage::app()->getStore()->getCode();
        $this->join(
            array('description' => 'brander_ourstores/description'),
            "main_table.entity_id=description.ourstore_id AND description.store_code='{$storeCode}'",
            array('description','work_time_comment', 'brands_description')
        );
        return $this;
    }
}