<?php

/**
 * Class Brander_OurStores_Model_Resource_Review
 */
class Brander_OurStores_Model_Resource_Review extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/review', 'entity_id');
    }

}