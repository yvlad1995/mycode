<?php

/**
 * Class Brander_OurStores_Model_Resource_Description
 */
class Brander_OurStores_Model_Resource_Description extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_ourstores/description', 'entity_id');
    }
}