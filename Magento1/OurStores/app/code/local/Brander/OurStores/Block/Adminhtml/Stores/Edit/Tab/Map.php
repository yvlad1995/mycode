<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Map
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Map extends Mage_Adminhtml_Block_Template
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Map constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        $this->setTemplate('brander_ourstores/map.phtml');
        parent::__construct($args);
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->_getMapCoordinates()->getLatitude();
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->_getMapCoordinates()->getLongitude();
    }

    /**
     * @return mixed
     */
    protected function _getMapCoordinates()
    {
        return Mage::registry('current_map');
    }
}