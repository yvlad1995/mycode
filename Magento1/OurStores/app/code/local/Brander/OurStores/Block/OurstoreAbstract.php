<?php

/**
 * Class Brander_OurStores_Block_OurstoreAbstract
 */
class Brander_OurStores_Block_OurstoreAbstract extends Mage_Core_Block_Template
{
    /**
     * @param $ourstoreId
     * @return mixed
     */
    public function getQtyReview($ourstoreId)
    {
        return $this->_getModel()->load($ourstoreId)->getStoreReviewQty();
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModel()
    {
        return Mage::getModel('brander_ourstores/store');
    }
}