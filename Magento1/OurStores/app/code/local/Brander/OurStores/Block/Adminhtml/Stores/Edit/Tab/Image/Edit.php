<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image_Edit
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'brander_ourstores';
        $this->_controller = 'adminhtml_stores_edit_tab_image';
        $this->removeButton('save');
        $this->removeButton('back');
        $this->removeButton('reset');

        $this->_addButton('back', array(
            'label'     => Mage::helper('brander_ourstores')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('brander_ourstores')->__('Reset'),
            'onclick'   => 'setLocation(window.location.href)',
        ), -1);

        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('brander_ourstores')->__('Delete Image')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('brander_ourstores')->__('Save Image'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete',
            array(
                $this->_objectId => $this->getRequest()->getParam($this->_objectId),
                'store_id'       => $this->getRequest()->getParam('store_id')
            )
        );
    }

    /**
     * get the edit form header
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_store') && Mage::registry('current_image')->getId()) {
            return Mage::helper('brander_ourstores')->__(
                "Edit Image"
            );
        } else {
            return Mage::helper('brander_ourstores')->__('New Image');
        }
    }


    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/ourstores/edit', array('id' => $this->getRequest()->getParam('store_id')));
    }
}
