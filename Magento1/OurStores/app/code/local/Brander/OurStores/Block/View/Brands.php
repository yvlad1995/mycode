<?php

/**
 * Class Brander_OurStores_Block_View_Brands
 */
class Brander_OurStores_Block_View_Brands extends Mage_Core_Block_Template
{
    /**
     * @return Mage_Core_Block_Abstract
     */
    public function getHelper()
    {
        return $this->helper('brander_ourstores');
    }

    /**
     * @return mixed
     */
    public function getBrandsDescription()
    {
        return Mage::registry('current_description')->getBrandsDescription();
    }

    /**
     * @return array
     */
    public function getBrands()
    {
        $store = Mage::registry('current_store');

        $brandsData = [];
        foreach ($this->_getBrands() as $brand){
            if(in_array($brand['value'], $store->getBrands())){
                $brandsData[] = $brand;
            }
        }

        return $brandsData;
    }

    /**
     * @return mixed
     */
    protected function _getBrands()
    {
        return Mage::getBlockSingleton('brander_productbanners/catalog_category_brands')->getItems();
    }
}