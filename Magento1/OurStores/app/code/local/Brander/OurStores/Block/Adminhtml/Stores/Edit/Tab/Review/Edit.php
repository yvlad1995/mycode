<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Edit
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'brander_ourstores';
        $this->_controller = 'adminhtml_stores_edit_tab_review';
        $this->removeButton('save');
        $this->removeButton('back');
        $this->removeButton('reset');

        $this->_addButton('back', array(
            'label'     => Mage::helper('brander_ourstores')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('brander_ourstores')->__('Reset'),
            'onclick'   => 'setLocation(window.location.href)',
        ), -1);

        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('brander_productbanners')->__('Delete Review')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('brander_productbanners')->__('Save Review'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('brander_ourstores')->__('Manage Review');
    }


    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        $storeId = $this->getRequest()->getParam('store_id');
        if ($storeId) {
            return $this->getUrl('*/ourstores/edit', array('id' => $storeId));
        }
        return $this->getUrl('*/ourstores_review/reviews');
    }

    /**
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save');
    }
}
