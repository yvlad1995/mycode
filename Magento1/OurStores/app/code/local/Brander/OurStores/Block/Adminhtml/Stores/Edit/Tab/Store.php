<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Store
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Store extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('store_data',
            array('legend'=>Mage::helper('brander_ourstores')->__('Store Data'))
        );

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('brander_ourstores')->__('Status'),
            'title' => Mage::helper('brander_ourstores')->__('Status'),
            'name'  =>'ourstore[status]',
            'required' => 1,
            'values' => array(
                array(
                    'value'     => 1,
                    'label'     => Mage::helper('brander_ourstores')->__('Enable'),
                ),

                array(
                    'value'     => 0,
                    'label'     => Mage::helper('brander_ourstores')->__('Disable'),
                ),

                array(
                    'value'     => '',
                    'label'     => Mage::helper('brander_ourstores')->__('------------'),
                ),
            ),
            'style' => 'width:100px'
        ));

        $fieldset->addField('image_preview', 'image', array(
            'label'=> Mage::helper('brander_ourstores')->__('Image Preview'),
            'title'=> Mage::helper('brander_ourstores')->__('Image Preview'),
            'name' => 'image',
        ));

        $fieldset->addField('work_time_from', 'text', array(
            'label'=> Mage::helper('brander_ourstores')->__('Work Time From'),
            'title'=> Mage::helper('brander_ourstores')->__('Work Time From'),
            'name' => 'ourstore[work_time_from]',
            'style' => 'width:50px',
            'required' => 1,
        ));

        $fieldset->addField('work_time_to', 'text', array(
            'label'=> Mage::helper('brander_ourstores')->__('Work Time To'),
            'title'=> Mage::helper('brander_ourstores')->__('Work Time To'),
            'name' => 'ourstore[work_time_to]',
            'style' => 'width:50px',
            'required' => 1,
        ));

        $fieldset->addField('work_time_comment', 'textarea', array(
            'label'=> Mage::helper('brander_ourstores')->__('Work Time Comment'),
            'title'=> Mage::helper('brander_ourstores')->__('Work Time Comment'),
            'name' => 'ourstore[work_time_comment]',
            'style' => 'width:150px;height:50px'
        ));


        $fieldset->addField('city_id', 'select', array(
            'label'=> Mage::helper('brander_ourstores')->__('City'),
            'title'=> Mage::helper('brander_ourstores')->__('City'),
            'name' => 'ourstore[city_id]',
            'value' => '4',
            'values' => Mage::getModel('brander_ourstores/store')->toOptionArrayCity(),
            'required' => 1,
            'class' => 'required-entry',
        ));

        $fieldset->addField('brands', 'multiselect', array(
            'label'=> Mage::helper('brander_ourstores')->__('Brands'),
            'title'=> Mage::helper('brander_ourstores')->__('Brands'),
            'name' => 'ourstore[brands]',
            'values' => $this->_getStore()->toOptionArrayBrand()
        ));

        $fieldset->addField('address', 'textarea', array(
            'label'=> Mage::helper('brander_ourstores')->__('Store Address'),
            'title'=> Mage::helper('brander_ourstores')->__('Store Address'),
            'name'=>'ourstore[address]',
            'required' => true,
            'style' => 'width:400px; height:80px'
        ));

        $fieldset->addField('description', 'textarea', array(
            'label' => Mage::helper('brander_ourstores')->__('Store Description'),
            'title' => Mage::helper('brander_ourstores')->__('Store Description'),
            'name'  => 'ourstore[description]',
            'style' => 'width:400px; height:80px'
        ));

        $fieldset->addField('brands_description', 'textarea', array(
            'label' => Mage::helper('brander_ourstores')->__('Brands Description'),
            'title' => Mage::helper('brander_ourstores')->__('Brands Description'),
            'name'  => 'ourstore[brands_description]',
            'style' => 'width:400px; height:80px'
        ));

        $fieldset->addField('url_key', 'text', array(
            'label' => Mage::helper('brander_ourstores')->__('Store Seo Url'),
            'title' => Mage::helper('brander_ourstores')->__('Store Seo Url'),
            'name'  => 'ourstore[url_key]',
            'after_element_html' => Mage::helper('brander_ourstores')
                ->__('%sIf Url is not entered, it will automatically be generated at the address store%s','<div>','</div>'),
        ));

        $fieldset->addField('store_code', 'hidden', array(
            'name'  => 'ourstore[store_code]',
        ));

        Mage::dispatchEvent('ourstores_after_field_add', array('form' => $form));

        if(!$this->getRequest()->getParam('store'))
        {
            Mage::getSingleton('adminhtml/session')->addError(
                $this->helper('brander_ourstores')->__('Please select store!')
            );
        } else {
            $form->setValues($this->_getStore()->getData());
            $form->addValues($this->_getDescription()->getData());

            $this->setForm($form);
        }
    }

    /**
     * @return mixed
     */
    protected function _getStore()
    {
        return Mage::registry('current_store');
    }

    protected function _getDescription()
    {
        return Mage::registry('current_description');
    }
}