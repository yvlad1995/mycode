<?php

/**
 * Class Brander_OurStores_Block_View_Toolbar
 */
class Brander_OurStores_Block_View_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
    /**
     * @param $rating
     * @return string
     */
    public function getRating($rating)
    {
        return Mage::getBlockSingleton('brander_ourstores/city')->getRatingCalc($rating) . '%';
    }

    /**
     * @param $review
     * @return false|string
     */
    public function getDateCreate($review)
    {
        $day = date('j',strtotime($review->getCreatedAt()));
        $month = date('F',strtotime($review->getCreatedAt()));
        $year = date('Y',strtotime($review->getCreatedAt()));
        $month = $this->helper('brander_ourstores')->getMonthLocale($month);
        return $this->__('%s %s %s', $day, $month, $year);
    }
}