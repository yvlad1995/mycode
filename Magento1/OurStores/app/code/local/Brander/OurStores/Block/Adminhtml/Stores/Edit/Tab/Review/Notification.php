<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Notification
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Notification extends Mage_Adminhtml_Block_Template
{
    /**
     * @var
     */
    protected $_messages;

    /**
     * @var
     */
    protected $_className;

    /**
     * @param string $className
     * @return string
     */
    public function _toHtml($className = "notification-global")
    {
        $html = '';

        if ($this->checkReviewMessage()) {
            $this->_messages = Mage::helper('brander_ourstores/review')->getMessageNotification();
            $this->_className = $className;

            Mage::dispatchEvent('ourstores_notification_reviews', array('notification' => $this));

            $html .= "<a href='" . $this->getUrl('*/ourstores_review/reviews') . "'>
                        <strong>
                            <div class='$this->_className'>" . $this->_messages . "</div>
                        </strong>
                      </a>";
        }

        return $html;
    }

    /**
     * @return bool
     */
    protected function checkReviewMessage()
    {
        $reviews = $this->_getModel()->getCollection();
        foreach ($reviews as $review) {
            if ($review->getStatus() == 2) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModel()
    {
        return Mage::getModel('brander_ourstores/review');
    }
}