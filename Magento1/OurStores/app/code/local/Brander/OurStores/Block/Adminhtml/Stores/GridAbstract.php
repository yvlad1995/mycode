<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_GridAbstract
 */
abstract class Brander_OurStores_Block_Adminhtml_Stores_GridAbstract extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * @param $imagePath
     * @return string
     */
    public function _imageHtml($imagePath)
    {
        $imageDirFile = Mage::helper('brander_ourstores/image')->getPathImageDirectory()  . DS . $imagePath;

        if(!is_dir($imageDirFile) && file_exists($imageDirFile)) {
            return '<img width="150px" height="100px" src="' . Mage::helper('brander_ourstores/image')->getPathImageUrl()  . $imagePath . '"/>';
        }
        return '';
    }
}