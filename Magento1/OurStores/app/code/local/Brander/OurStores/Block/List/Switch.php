<?php

/**
 * Class Brander_OurStores_Block_List_Switch
 */
class Brander_OurStores_Block_List_Switch extends Mage_Core_Block_Template
{
    protected $_switchNames;

    const LISTNAME = 'list-page';

    const MAPNAME = 'map-page';

    /**
     * Brander_OurStores_Block_List_Switch constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        parent::__construct($args);
        $this->setSwichName();
    }

    /**
     * @return mixed
     * for ourstores page
     */
    public function getSwitchNames()
    {
        return $this->_switchNames;
    }

    public function setSwichName()
    {
        $this->_switchNames = array(
            self::LISTNAME => 'List',
            self::MAPNAME  => 'On the map'
        );
    }
}