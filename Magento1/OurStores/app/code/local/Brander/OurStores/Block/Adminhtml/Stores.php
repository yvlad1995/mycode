<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores
 */
class Brander_OurStores_Block_Adminhtml_Stores extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores constructor.
     */
    public function __construct()
    {
        $this->_blockGroup = 'brander_ourstores';
        $this->_controller = 'adminhtml_stores';
        $this->_headerText = Mage::helper('brander_ourstores')->__('Manage Stores');
        $this->_addButtonLabel = Mage::helper('brander_ourstores')->__('Add New Store');
        parent::__construct();
    }
}