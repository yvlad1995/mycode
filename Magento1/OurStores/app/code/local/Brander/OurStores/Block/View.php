<?php

/**
 * Class Brander_OurStores_Block_View
 */
class Brander_OurStores_Block_View extends Brander_OurStores_Block_OurstoreAbstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->addData(array(
            'cache_lifetime' => false,
            'cache_tags' => array(
                'OURSTORES',
                'VIEW'
            )
        ));
    }

    /**
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $cacheId = array(
            'OURSTORES_VIEW-' . $this->getRequest()->getParam('id'),
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout()
        );
        return $cacheId;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return Mage::registry('current_description')->getAddress();
    }

    /**
     * @return mixed
     */
    public function getOurstore()
    {
        return Mage::registry('current_store');
    }

    /**
     * @return mixed
     */
    public function getCityName()
    {
        $storeCode = Mage::app()->getStore()->getCode();
        return $this->getOurstore()->getCityName($this->getOurstore()->getCityId(), $storeCode);
    }
}