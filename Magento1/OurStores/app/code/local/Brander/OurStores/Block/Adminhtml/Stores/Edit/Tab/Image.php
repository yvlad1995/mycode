<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image extends Brander_OurStores_Block_Adminhtml_Stores_GridAbstract
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('imageGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $storeId = Mage::app()->getRequest()->getParam('id');

        $collection = Mage::getModel('brander_ourstores/image')
            ->getCollection()
            ->addFieldToFilter('ourstore_id', $storeId);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('brander_ourstores')->__('Id'),
                'type'   => 'number',
                'align'  => 'left',
                'index'  => 'entity_id',
                'filter' => false
            )
        );

        $this->addColumn(
            'position',
            array(
                'header'         => Mage::helper('brander_ourstores')->__('Position'),
                'name'           => 'position',
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
                'filter' => false
            )
        );

        $this->addColumn(
            'image_path',
            array(
                'header' => Mage::helper('brander_ourstores')->__('Image'),
                'header_css_class'  => 'a-center',
                'type'   => 'image',
                'name'   => 'image_path',
                'align'  => 'center',
                'index'  => 'image_path',
                'filter' => false,
                'frame_callback' => array($this, '_imageHtml')
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * @param $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('*/ourstores_image/edit', array(
            'id'       => $item->getId(),
            'store_id' => $this->getRequest()->getParam('id'),
        ));
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/imagesGrid', array('_current'=>true));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->unsetChild('reset_filter_button');
        return $this->unsetChild('search_button');
    }

    /**
     * @return string
     */
    protected function getAdditionalJavascript() {
        return 'window.' . $this->getHtmlId() . '_massactionJsObject = ' . $this->getHtmlId() . '_massactionJsObject;';
    }

    /**
     * @return string
     */
    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();//get the parent class buttons
        $addButton = $this->getLayout()->createBlock('adminhtml/widget_button') //create the add button
        ->setData(array(
            'label'     => Mage::helper('brander_ourstores')->__('Add New Image'),
            'onclick'   => "setLocation('".$this->getUrl('*/ourstores_image/new', array(
                    'store_id' => $this->getRequest()->getParam('id'),
                ))."')",
        ))->toHtml();
        return $addButton.$html;
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('images');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('brander_ourstores')->__('Delete'),
            'url'  => $this->getUrl('*/ourstores_image/massDelete',
                array('store_id' => $this->getRequest()->getParam('id'))
            ),
            'confirm' => Mage::helper('brander_ourstores')->__('Are you sure?')
        ));

        Mage::dispatchEvent('ourstores_images_grid_massaction', array('block' => $this));

        return $this;
    }
}