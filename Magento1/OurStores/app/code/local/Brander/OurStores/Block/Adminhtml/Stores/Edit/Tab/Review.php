<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review extends Mage_Adminhtml_Block_Widget_Grid
{
    const STATUS = 2;

    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('reviewGrid');
        $this->setDefaultSort('status');
        $this->setDefaultDir('DESC');
        $this->setDefaultFilter(array('status' => self::STATUS));
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $ourstoreId = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('brander_ourstores/review')->getCollection();
        if ($ourstoreId) {
            $collection->addFieldToFilter('ourstore_id', $ourstoreId);
        }
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('brander_ourstores')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('brander_ourstores')->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::helper('review')->getReviewStatuses(),
        ));

        $this->addColumn('customer_name', array(
            'header'   => Mage::helper('brander_ourstores')->__('Customer Name'),
            'index'    => 'customer_name',
        ));

        $this->addColumn('review_description', array(
            'header'   => Mage::helper('brander_ourstores')->__('Review Description'),
            'index'    => 'review_description',
            'renderer' => 'Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Description'
        ));

        $this->addColumn('rating', array(
            'header'    => Mage::helper('brander_ourstores')->__('Rating'),
            'index'     => 'rating',
        ));

        $this->addColumn('ourstore_id', array(
            'header'   => Mage::helper('brander_ourstores')->__('Ourstore Id'),
            'index'    => 'ourstore_id',
        ));

        return parent::_prepareColumns();
    }

    /**
     * @return string
     * Custom grid url for page all reviews and store review
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/' . $this->getRequest()->getActionName(). 'Grid', array('_current'=>true));
    }

    /**
     * @param $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('*/ourstores_review/edit', array(
            'id'         => $item->getId(),
            'store_id' => $this->getRequest()->getParam('id'),
        ));
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('reviews');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('brander_ourstores')->__('Delete'),
            'url'  => $this->getUrl('*/ourstores_review/massDelete',
                array('store_id' => $this->getRequest()->getParam('id'))
            ),
            'confirm' => Mage::helper('brander_ourstores')->__('Are you sure?')
        ));

        $statuses = Mage::helper('review')->getReviewStatuses();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('brander_ourstores')->__('Change status'),
            'url'  => $this->getUrl('*/ourstores_review/massStatus',
                array(
                    '_current' => true,
                    'store_id' => $this->getRequest()->getParam('id')
                    )
            ),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('brander_ourstores')->__('Status'),
                    'values' => $statuses
                )
            )
        ));

        Mage::dispatchEvent('ourstores_review_grid_massaction', array('block' => $this));

        return $this;
    }

    /**
     * @return string
     */
    protected function getAdditionalJavascript() {
        return 'window.' . $this->getHtmlId() . '_massactionJsObject = ' . $this->getHtmlId() . '_massactionJsObject;';
    }
}
