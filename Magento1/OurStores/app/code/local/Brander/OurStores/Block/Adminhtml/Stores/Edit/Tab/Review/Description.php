<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Description
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Description
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        if (strlen($row->getReviewDescription()) > 100) {
            return substr($row->getReviewDescription(),0, 100) . ' ....';
        }
        return $row->getReviewDescription();
    }
}