<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Edit_Form
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'         => 'edit_form',
                'action'     => $this->getUrl(
                    '*/*/save',
                    array(
                        'id' => $this->getRequest()->getParam('id'),
                        'store_id' => $this->_getStoreId(),
                    )
                ),
                'method'     => 'post',
                'enctype'    => 'multipart/form-data'
            )
        );

        $fieldset = $form->addFieldset('store_review',
            array('legend'=>Mage::helper('brander_ourstores')->__('Store Review'))
        );

        $fieldset->addField('city', 'note', array(
            'label' => Mage::helper('brander_ourstores')->__('City'),
            'title' => Mage::helper('brander_ourstores')->__('City'),
            'text'  => $this->_getCity()->getDescription()
        ));

        $fieldset->addField('address', 'note', array(
            'label' => Mage::helper('brander_ourstores')->__('Address'),
            'title' => Mage::helper('brander_ourstores')->__('Address'),
            'name'  => 'address',
            'text'  => $this->_getAddress() //TODO
        ));

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('brander_ourstores')->__('Status'),
            'required'  => true,
            'name'      => 'status',
            'values'    => Mage::helper('review')->getReviewStatusesOptionArray(),
        ));

        $fieldset->addField('rating', 'note', array(
            'label'    => Mage::helper('brander_ourstores')->__('Rating'),
            'title'    => Mage::helper('brander_ourstores')->__('Rating'),
            'required' => true,
            'text'     => '<div id="rating_detail">'
                . $this->getLayout()->createBlock('brander_ourstores/adminhtml_stores_edit_tab_review_rating')->toHtml()
                . '</div>',
        ));

        $fieldset->addField('customer_name', 'text', array(
            'label'    => Mage::helper('brander_ourstores')->__('Customer Name'),
            'title'    => Mage::helper('brander_ourstores')->__('Customer Name'),
            'name'     => 'customer_name',
            'required' => true,
        ));

        $fieldset->addField('review_description', 'textarea', array(
            'label'    => Mage::helper('brander_ourstores')->__('Review Comment'),
            'title'    => Mage::helper('brander_ourstores')->__('Review Comment'),
            'name'     => 'review_description',
            'style'    => 'width:500px',
            'required' => true,
        ));

        Mage::dispatchEvent('ourstores_image_field_add', array('form' => $form));

        $form->setValues($this->_getReview()->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
    }

    /**
     * @return mixed
     */
    protected function _getReview()
    {
        return Mage::registry('current_review');
    }

    /**
     * @return mixed
     */
    protected function _getAddress()
    {
        return $this->_getStore()->getAddress();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getStore()
    {
        return Mage::getModel('brander_ourstores/store')->load($this->_getReview()->getOurstoreId());
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _getCity()
    {
        return Mage::getModel('brander_shipping/cities')->load($this->_getStore()->getCityId(), 'city_id');
    }

    /**
     * @return mixed
     */
    protected function _getStoreId()
    {
        return $this->getRequest()->getParam('store_id');
    }
}