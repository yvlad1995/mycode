<?php

/**
 * Class Brander_OurStores_Block_View_Image
 */
class Brander_OurStores_Block_View_Image extends Mage_Core_Block_Template
{
    /**
     * @return mixed
     */
    protected function _getAllImageStore()
    {
        $ourstoreId = Mage::registry('current_store')->getId();
        return $this->_getModelImage()
            ->getCollection()
            ->addFieldToFilter('ourstore_id', $ourstoreId)
            ->setOrder('position', 'ASC');
    }

    /**
     * @return bool|string
     */
    public function getImagesPreviewStore()
    {
        $image = $this->_getAllImageStore()->getFirstItem()->getImagePath();
        return $this->_getImage($image);
    }

    /**
     * @return array
     */
    public function getImagesStore()
    {
        $images = [];
        foreach ($this->_getAllImageStore() as $image) {
            if($this->_getImage($image->getImagePath())) {
                $images[] = $this->_getImage($image->getImagePath());
            }
        }
        return $images;
    }

    /**
     * @param $image
     * @return bool|string
     */
    protected function _getImage($image)
    {
        $imagePath = $this->getImageHelper()->getPathImageDirectory() . DS . $image;

        if(!is_dir($imagePath) && file_exists($imagePath)) {
            return $this->getImageHelper()->getPathImageUrl() . $image;
        }

        return false;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModelImage()
    {
        return Mage::getModel('brander_ourstores/image');
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getImageHelper()
    {
        return Mage::helper('brander_ourstores/image');
    }
}