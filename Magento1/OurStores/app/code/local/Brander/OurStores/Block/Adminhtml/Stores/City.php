<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_City
 */
class Brander_OurStores_Block_Adminhtml_Stores_City extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return mixed
     */
    public function render(Varien_Object $row)
    {
        return $this->_getModel()->getCityName($row->getCityId());
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModel()
    {
        return Mage::getModel('brander_ourstores/store');
    }
}