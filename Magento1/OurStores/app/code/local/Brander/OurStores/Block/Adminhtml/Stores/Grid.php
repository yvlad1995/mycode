<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Grid
 */
class Brander_OurStores_Block_Adminhtml_Stores_Grid extends Brander_OurStores_Block_Adminhtml_Stores_GridAbstract
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('storeGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {

        $collection = Mage::getModel('brander_ourstores/store')->getCollection();

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'entity_id',
            ));

        $this->addColumn('image_preview',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('Image Preview'),
                'index' => 'image_preview',
                'frame_callback' => array($this, '_imageHtml')
                ));

        $this->addColumn('city_id',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('City'),
                'index' => 'city_id',
                'renderer' => 'Brander_OurStores_Block_Adminhtml_Stores_City'
            ));

        $this->addColumn('work_time_from',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('Work Time From'),
                'index' => 'work_time_from',
            ));

        $this->addColumn('work_time_to',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('Work Time To'),
                'index' => 'work_time_to',
            ));

        $this->addColumn('address',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('Address'),
                'index' => 'address',
            ));

        $this->addColumn('rating',
            array(
                'header'=> Mage::helper('brander_ourstores')->__('Store Rating'),
                'index' => 'rating',
            ));

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('stores');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('brander_ourstores')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('brander_ourstores')->__('Are you sure?')
        ));

        Mage::dispatchEvent('ourstores_stores_grid_massaction', array('block' => $this));

        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
                'store'=>$this->getRequest()->getParam('store'),
                'id'=>$row->getId())
        );
    }
}