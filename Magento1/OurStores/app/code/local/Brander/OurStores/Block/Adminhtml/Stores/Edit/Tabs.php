<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tabs
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('ourstore_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('brander_ourstores')->__('Store Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {

        $this->addTab('store', array(
            'label'     => Mage::helper('brander_ourstores')->__('Store'),
            'content'   => $this->_translateHtml($this->getLayout()
                ->createBlock('brander_ourstores/adminhtml_stores_edit_tab_store')->toHtml()),
        ));

        $this->addTab('map', array(
            'label'     => Mage::helper('brander_ourstores')->__('Store Map'),
            'content'   => $this->_translateHtml($this->getLayout()
                ->createBlock('brander_ourstores/adminhtml_stores_edit_tab_map')->toHtml()),
        ));

        if($this->getRequest()->getParam('id')) {
            $this->addTab('reviews', array(
                'label' => Mage::helper('brander_ourstores')->__('Reviews'),
                'url' => $this->getUrl('*/ourstores_review/review', array('_current' => true)),
                'class' => 'ajax',
                'required' => false
            ));

            $this->addTab('images_store', array(
                'label' => Mage::helper('brander_ourstores')->__('Images Store'),
                'url' => $this->getUrl('*/ourstores_image/images', array('_current' => true)),
                'class' => 'ajax',
            ));
        }
        return parent::_prepareLayout();
    }

    /**
     * @param $html
     * @return mixed
     */
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }
}
