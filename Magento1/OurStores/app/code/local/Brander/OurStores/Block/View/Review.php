<?php

/**
 * Class Brander_OurStores_Block_View_Review
 */
class Brander_OurStores_Block_View_Review extends Mage_Catalog_Block_Product_List_Toolbar
{
    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
}