<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Rating
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Rating extends Mage_Adminhtml_Block_Template
{
    /**
     * Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Review_Rating constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('brander_ourstores/review/rating/stars.phtml');
    }

    /**
     * @return mixed
     */
    public function getRatingValue()
    {
        return $this->_getCurrentReview()->getRating();
    }

    /**
     * @return mixed
     */
    protected function _getCurrentReview()
    {
        return Mage::registry('current_review');
    }
}