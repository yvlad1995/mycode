<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image_Edit_Form
 */
class Brander_OurStores_Block_Adminhtml_Stores_Edit_Tab_Image_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return mixed
     */
    protected function _getStoreId()
    {
        return $this->getRequest()->getParam('store_id');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'         => 'edit_form',
                'action'     => $this->getUrl(
                    '*/*/save',
                    array(
                        'id'       => $this->getRequest()->getParam('id'),
                        'store_id' => $this->_getStoreId()
                    )
                ),
                'method'     => 'post',
                'enctype'    => 'multipart/form-data'
            )
        );

        $fieldset = $form->addFieldset('store_image',
            array('legend'=>Mage::helper('brander_ourstores')->__('Store Image'))
        );


        $fieldset->addField('image_path', 'image', array(
            'label'=> Mage::helper('brander_ourstores')->__('Image'),
            'title'=> Mage::helper('brander_ourstores')->__('Image'),
            'name' => 'image',
        ));

        $fieldset->addField('position', 'text', array(
            'label'=> Mage::helper('brander_ourstores')->__('Position'),
            'title'=> Mage::helper('brander_ourstores')->__('Position'),
            'name' => 'position',
            'style' => 'width:100px'
        ));

        Mage::dispatchEvent('ourstores_image_field_add', array('form' => $form));

        $form->setValues($this->_getImage()->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return mixed
     */
    protected function _getImage()
    {
        return Mage::registry('current_image');
    }
}