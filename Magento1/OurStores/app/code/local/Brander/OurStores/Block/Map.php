<?php

/**
 * Class Brander_OurStores_Block_Map
 */
class Brander_OurStores_Block_Map extends Mage_Core_Block_Template
{
    const CURRENT_STORE_ZOOM = 18;

    const STORES_ZOOM = 7;

    /**
     * @return mixed
     */
    public function getMapCoordinates()
    {
        $map = $this->_getModel();

        $collection = $this->_getModelStore()->getCollection()->addFieldToFilter('status', 1);

        $currentStore = Mage::registry('current_store');

        $coordinates = [];

        if($currentStore){
            $collection->addFieldToFilter('entity_id', $currentStore->getId());
        }else{
            $coordinates[] = [
                'latitude'  => (float)$this->getMapHelper()->getLatitude(),
                'longitude' => (float)$this->getMapHelper()->getLongitude(),
            ];
        }

        foreach ($collection as $item) {
            $map->load($item->getId(), 'ourstore_id');
            $coordinates[] = [
                'latitude'  => (float)$map->getLatitude(),
                'longitude' => (float)$map->getLongitude(),
            ];
        }

        return Mage::helper('core')->jsonEncode($coordinates);
    }

    /**
     * @return int
     */
    public function getZoom()
    {
        if(Mage::registry('current_store'))
        {
            return self::CURRENT_STORE_ZOOM;
        }

        return self::STORES_ZOOM;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModelStore()
    {
        return Mage::getModel('brander_ourstores/store');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModel()
    {
        return Mage::getModel('brander_ourstores/map');
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getMapHelper()
    {
        return Mage::helper('brander_ourstores/map');
    }
}