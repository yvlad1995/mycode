<?php

/**
 * Class Brander_OurStores_Block_View_Toolbar_Pager
 */
class Brander_OurStores_Block_View_Toolbar_Pager extends Mage_Page_Block_Html_Pager
{
    const AVAILIBLE_LIMIT = array(6=>6,12=>12,18=>18,'all'=>'all');

    const LIMIT = 6;

    const STATUS_REVIEW = 1;

    /**
     * Brander_OurStores_Block_View_Toolbar_Pager constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        parent::__construct($args);
        $ourstore = Mage::registry('current_store');
        $ourstoreId = Mage::registry('ourstore_id');
        $ourstoreId = $ourstoreId ? $ourstoreId : $ourstore->getId();
        $collection = Mage::getModel('brander_ourstores/review')->getCollection()
            ->addFieldToFilter('ourstore_id', $ourstoreId)
            ->addFieldToFilter('status', self::STATUS_REVIEW)
            ->setOrder('created_at', 'DESC');

        $this->setAvailableLimit(self::AVAILIBLE_LIMIT);
        $this->setUseContainer(false)
            ->setShowPerPage(false)
            ->setShowAmounts(false)
            ->setLimitVarName($this->getLimitVarName())
            ->setPageVarName($this->getPageVarName())
            ->setLimit(self::LIMIT);

        $this->setCollection($collection);
    }
}