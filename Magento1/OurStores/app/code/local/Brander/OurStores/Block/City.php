<?php

/**
 * Class Brander_OurStores_Block_City
 */
class Brander_OurStores_Block_City extends Brander_OurStores_Block_OurstoreAbstract
{
    const QTY_STARS = 5;

    protected $_ourStoreCity = [];

    /**
     * @return mixed
     */
    public function getCitiesArray()
    {
        $collection = $this->getOurStoreCity();

        $cities[0] = $this->getImageHelper()->__('Select City');

        foreach ($collection as $key => $item)
        {
            $cities[$key] = $item['name'];
        }

        return $cities;
    }

    /**
     * @return array
     */
    public function getOurStoreCity()
    {
        if(count($this->_ourStoreCity)) {
            return $this->_ourStoreCity;
        }
        $storeCode = Mage::app()->getStore()->getCode();

        if(!$this->_collection) {
            $this->_collection = $this->_getModel()->getCollection()
                ->addFieldToFilter('status', 1)
                ->setOrder('city_id', 'ASC');
        }

        $cityId = Mage::registry('city_id');

        if($cityId) {
            $this->_collection->addFieldToFilter('city_id', $cityId);
        }

        $this->_collection->joinDescription();

        foreach ($this->_collection as $item) {
            if (!array_key_exists($item->getCityId(), $this->_ourStoreCity)) {
                $this->_ourStoreCity[$item->getCityId()]['name'] = $this->_getModel()->getCityName($item->getCityId(), $storeCode);
            }
            $this->_ourStoreCity[$item->getCityId()][$item->getId()] = $item;
        }

        return $this->_ourStoreCity;
    }

    /**
     * @param $image
     * @return bool|string
     */
    public function getImageUrl($image)
    {
        $dirImagePath = $this->getImageHelper()->getPathImageDirectory() . DS . $image;

        if (!is_dir($dirImagePath) && file_exists($dirImagePath)) {
            return $this->getImageHelper()->getPathImageUrl() . $image;
        }

        return false;
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getImageHelper()
    {
        return Mage::helper('brander_ourstores/image');
    }

    /**
     * @param $ourstoreId
     * @return string
     *
     */
    public function getRatingPercent($ourstoreId = false)
    {
        if (!$ourstoreId) {
           return $this->getRatingCalc(Mage::registry('current_store')->getRating()) . '%';
        }

        return $this->getRatingCalc($this->_getModel()->load($ourstoreId)->getRating()) . '%';
    }

    /**
     * @param $rating
     * @return float|int
     */
    public function getRatingCalc($rating)
    {
        return ($rating * 100) / self::QTY_STARS;
    }

}