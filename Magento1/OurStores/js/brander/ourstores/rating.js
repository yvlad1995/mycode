jQuery(function($) {
    var ratingValue = $('#product-review-table .value');

    $('#product-review-table .value.current').prevAll(".value").addClass("current");

    ratingValue.mouseenter(function () {
        $(this).addClass('active');
        $(this).prevAll(".value").addClass("active");
    });

    ratingValue.mouseleave(function () {
        $('#product-review-table .active').each(function () {
            $(this).removeClass("active");
        });
    });

    ratingValue.click(function () {
        $('#product-review-table .value.current').removeClass("current");
        $(this).addClass("current");
        $(this).prevAll(".value").addClass("current");
    });
});
