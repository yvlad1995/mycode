function pagination(url,page) {
    jQuery(function ($, url) {
        $.ajax({
            url:url,
            data: {
                p: page,
                ourstore_id: ourstore_id
            },
            dataType: "html",
            success: function (html) {
                if (html.length > 0) {
                    $('#reviews').empty();
                    $('#reviews').prepend(html);
                }
            }
        });
    })
};
