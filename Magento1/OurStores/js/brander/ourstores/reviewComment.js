
function reviewCommentSave() {
    if (!inProgress) {
        inProgress = true;
        var customerName = jQuery('input[name="customer_name"]').val();
        var reviewDescription = jQuery('textarea[name="review_description"]').val();
        var formKey = jQuery('input[name="form_key"]').val();
        var rating = jQuery('.value.current').last().children('input').val();
    }
    jQuery.ajax({
        url: url,
        dataType: "text",
        type: "POST",
        data: {
            customer_name: customerName,
            review_description: reviewDescription,
            ourstore_id: currentStoreId,
            form_key: formKey,
            rating: rating,
        },

        success: function (msg) {
            this.setMessage(msg, 'success-msg');
            this.resetField();
        },

        error: function (msg) {
            this.setMessage(msg.responseText, 'error-msg');
        },

        resetField: function () {
            jQuery('#review-form')[0].reset()
        },

        setMessage: function (msg, msgType) {
            jQuery('#input-message-box').empty();
            jQuery('#input-message-box').append('<div class="' + msgType + '">'
                + msg +
                '</div>');
            setTimeout(function () {
                jQuery('#input-message-box').empty();
            }, 15000);

            inProgress = false;
        }
    })
};
