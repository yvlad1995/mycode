<?php

$this->startSetup();
    $table = $this->getTable('branderstickers/stickers');
$table = $this->getConnection()
    ->newTable($this->getTable('branderstickers/stickers'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ),
        'Position'
    )
    ->addColumn(
        'status',
        Varien_Db_Ddl_Table::TYPE_TINYINT,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ),
        'Status'
    )
    ->addColumn(
        'image',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(),
        'Image'
    )
    ->addColumn(
        'product_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER ,
        null,
        array(),
        'Product Id'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER ,
        null,
        array(),
        'Store Id'
    )
    ->setComment('Product Stickers');
$this->getConnection()->createTable($table);
$this->endSetup();
