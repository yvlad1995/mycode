<?php

class Brander_Stickers_Helper_Data extends Brander_Stickers_Helper_Config
{
    public function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    public function getPathImage()
    {
        return Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    public function getUrlImage()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }
}