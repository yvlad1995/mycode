<?php

/**
 * Class Brander_Stickers_Helper_Stickers
 */
class Brander_Stickers_Helper_Stickers extends Brander_Stickers_Helper_Data
{
    const IMAGE = 'image';

    const NAME  = 'name';

    const IMAGE_WIDTH_ADMIN = 70;

    const IMAGE_HEIGHT_ADMIN = 70;

    /**
     * @return string
     */
    public function uploadImage()
    {

        if (!empty($_FILES[self::IMAGE][self::NAME])) {
            try {
                $uploader = new Varien_File_Uploader(self::IMAGE);
                $uploader->setAllowedExtensions($this->getAllowedExtension());
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $fileName = time() . self::IMAGE . $this->getImageType($_FILES[self::IMAGE]);
                $uploader->save($this->getPathImage() . DS . 'brander' . DS  . 'stickers' , $fileName);
                return 'brander' . DS  . 'stickers' . DS . $fileName;
            }catch (Exception $e) {
                return Mage::getSingleton('core/session')->addError('Image don\'t uploaded');
            }
        }
    }

    /**
     * @return array
     */
    public function getAllowedExtension()
    {
        return ['jpg', 'jpeg', 'svg', 'png'];
    }

    /**
     * @param $image
     * @return string
     */
    public function getImageType($image)
    {
        $types = $this->getAllowedExtension();
        foreach ($types as $type)
        {
            if(strstr($image['type'], $type))
            {
                return '.' .$type;
            }
        }

        return '.png';
    }

    /**
     * @param $image
     */
    public function removeImage($image)
    {
        if($image)
        {
            if(file_exists($this->getPathImage() . DS . $image))
            {
                unlink($this->getPathImage() . DS . $image);
            }
        }
    }

    /**
     * @return array
     */
    public function getMessagesSave()
    {
        return [
            $this->__('Sticker was saved'),
            $this->__('Error saving sticker')
        ];
    }

    /**
     * @return array
     */
    public function getMessagesDelete()
    {
        return [
            $this->__('Sticker was deleted'),
            $this->__('Error deleted sticker')
        ];
    }
}