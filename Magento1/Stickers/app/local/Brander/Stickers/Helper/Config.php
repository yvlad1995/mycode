<?php

class Brander_Stickers_Helper_Config extends Mage_Core_Helper_Abstract
{
    const ENABLE_STICKERS = 'branderstickers/config/enable';

    const STICKERS_QTY = 'branderstickers/config/default_qty';

    const STATUS_ENABLED = 1;

    const STATUS_DISABLED = 0;

    static public function isEnableStickers()
    {
        return Mage::getStoreConfig(self::ENABLE_STICKERS, Mage::app()->getStore()->getId());
    }

    static public function isStickersQty()
    {
        return Mage::getStoreConfig(self::STICKERS_QTY, Mage::app()->getStore()->getId());
    }

    /**
     * @return array
     */
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('catalog')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('catalog')->__('Disabled')
        );
    }
}