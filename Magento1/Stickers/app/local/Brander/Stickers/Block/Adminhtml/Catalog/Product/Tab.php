<?php

class Brander_Stickers_Block_Adminhtml_Catalog_Product_Tab
    extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected $_collection;

    public function __construct()
    {
        parent::__construct();
        $this->setId('branderstickers_grid');
        $this->setDefaultSort('ID');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $this->_collection = Mage::getModel('branderstickers/stickers')
            ->getCollection()
            ->addFieldToFilter('product_id', $this->_getProduct()->getId())
            ->addFieldToFilter('store_id', $this->_getHelper()->getStoreId());

        $this->setCollection($this->_collection);
        return parent::_prepareCollection();
    }

    protected function _prepareLayout()
    {
        $this->unsetChild('reset_filter_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Reset Filter'),
                    'onclick'   => $this->getJsObjectName().'.resetFilter()',
                ))
        );
        $this->unsetChild('search_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Search'),
                    'onclick'   => $this->getJsObjectName().'.doFilter()',
                    'class'   => 'task'
                ))
        );
    }

    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();
        $addButton = '';
        if($this->getStickersQty() < Brander_Stickers_Helper_Config::isStickersQty())
        {
            $addButton = $this->getLayout()->createBlock('adminhtml/widget_button') //create the add button
            ->setData(array(
                'label'     => Mage::helper('adminhtml')->__('Add New Sticker'),
                'onclick'   => "setLocation('".$this->getUrl('*/catalog_stickers/new', array(
                        'product_id'          => $this->_getProduct()->getId(),
                    ))."')",
                'class'   => 'sticker'
            ))->toHtml();
        }

        return $addButton.$html;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => $this->_getHelper()->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));


        $this->addColumn('image', array(
            'header'    => $this->_getHelper()->__('Image'),
            'width'     => 100,
            'index'     => 'image',
            'frame_callback' => array($this, 'getImage')
        ));

        $this->addColumn('position', array(
            'header'            => $this->_getHelper()->__('Position'),
            'name'              => 'position',
            'validate_class'    => 'validate-number',
            'index'             => 'position',
            'width'             => 60,
        ));

        $this->addColumn('status', array(
            'header'    => $this->_getHelper()->__('Status'),
            'width'     => 90,
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Brander_Stickers_Helper_Config::getOptionArray(),
        ));

        return parent::_prepareColumns();
    }


    public function getImage($value)
    {
        $width = Brander_Stickers_Helper_Stickers::IMAGE_WIDTH_ADMIN;
        $height =Brander_Stickers_Helper_Stickers::IMAGE_HEIGHT_ADMIN;
        $imagePath = $this->_getHelper()->getUrlImage() .  $value;
        return "<img src='" . $imagePath . "' width=".$width." height=".$height."/>";
    }


    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($item)
    {
        return $this->getUrl('*/catalog_stickers/edit', array(
            'id'         => $item->getId(),
            'product_id' => $this->_getProduct()->getId(),
        ));
    }

    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Stickers');
    }

    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Click here for added stickers for product');
    }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    protected function _getProduct()
    {
        return Mage::registry('current_product');
    }

    public function getLayout()
    {
        return Mage::app()->getLayout();
    }

    protected function getStickersQty()
    {
        return count($this->_collection->getItems());
    }

    protected function _getHelper()
    {
        return Mage::helper('branderstickers/stickers');
    }

}