<?php


class Brander_Stickers_Block_Adminhtml_Catalog_Product_Tab_Edit_Tab_Sticker extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $helper = Mage::helper('branderstickers');
        $model  = $this->getEntity();

        $form     = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('status', 'select', array(
            'label'   => $helper->__('Status'),
            'default' => 0,
            'name'    => 'status',
            'type'    => 'options',
            'options' => array(
                '0' => 'Disable',
                '1' => 'Enable'
            )
        ));

        $fieldset->addField('position', 'text', array(
            'label'    => $helper->__('Position'),
            'name'     => 'position',
        ));

        $fieldset->addField('image', 'image', array(
            'name'     => 'image',
            'label'    => $helper->__('Image Sticker'),
        ));

        $fieldset->addField('product_id', 'hidden', array(
            'name'     => 'product_id',
        ));

        $fieldset->addField('form_key', 'hidden', array(
            'name'     => 'form_key',
        ));

        $fieldset->addField('id', 'hidden', array(
            'name'     => 'id',
        ));

        $model->addData([
            'product_id' => $this->getRequest()->getParam('product_id'),
            'id'  => $this->getRequest()->getParam('id'),
            'form_key'    => Mage::getSingleton('core/session')->getFormKey()
            ]);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
