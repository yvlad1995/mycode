<?php


class Brander_Stickers_Block_Adminhtml_Catalog_Product_Tab_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_banner_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('branderstickers')->__('Post Information'));
    }

    protected function _prepareLayout()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        $entity = Mage::getModel('branderstickers/stickers')->load($id);

        $this->addTab(
            'info',
            array(
                'label'   => Mage::helper('branderstickers')->__('Banner Information'),
                'content' => $this->getLayout()->createBlock(
                    'branderstickers/adminhtml_catalog_product_tab_edit_tab_sticker'
                )
                    ->setEntity($entity)
                    ->toHtml(),
            )
        );

        return parent::_beforeToHtml();
    }

}
