<?php


class Brander_Stickers_Block_Adminhtml_Catalog_Product_Tab_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'branderstickers';
        $this->_controller = 'adminhtml_catalog_product_tab_edit';
        $this->removeButton('back');
        $this->removeButton('reset');

        $this->_addButton('back', array(
            'label'     => Mage::helper('adminhtml')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('adminhtml')->__('Reset'),
            'onclick'   => 'setLocation(window.location.href)',
        ), -1);

        $this->_updateButton(
            'save',
            'label',
            Mage::helper('branderstickers')->__('Save Stickers')
        );
    }

    /**
     * get the edit form header
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_sticker') && Mage::registry('current_sticker')->getId()) {
            return Mage::helper('branderstickers')->__(
                "Edit Sticker"
            );
        } else {
            return Mage::helper('branderstickers')->__('Add Sticker');
        }
    }

    /**
     * @return string
     */
    public function getFormHtml()
    {
        $block = $this->getLayout()->createBlock('branderstickers/adminhtml_catalog_product_tab_edit_form', 'form');
        $this->setChild('form', $block);
        $this->getChild('form')->setData('action', $this->getSaveUrl());
        return $this->getChildHtml('form');
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/catalog_product/edit',array(
            'id' => $this->getRequest()->getParam('product_id')
        ));
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete',
            array(
                $this->_objectId => $this->getRequest()->getParam($this->_objectId),
                'product_id' => $this->getRequest()->getParam('product_id')
            ));
    }
}
