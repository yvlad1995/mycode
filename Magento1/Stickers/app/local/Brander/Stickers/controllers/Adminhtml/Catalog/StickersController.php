<?php

class Brander_Stickers_Adminhtml_Catalog_StickersController extends Mage_Adminhtml_Controller_Action
{
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initStickers();
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _initStickers()
    {
        $id = $this->getRequest()->getParam('id');
        $sticker = Mage::getModel('branderstickers/stickers')
            ->setStoreId($this->_getHelper()->getStoreId());
        if($id)
        {
            $sticker->load($id);
            Mage::register('current_sticker', $sticker);
        }

        return $sticker;
    }

    public function saveAction()
    {
        $this->tryCatchBlock('save', $this->_getHelper()->getMessagesSave());

        $this->_redirect('*/catalog_product/edit',array(
            'id' => $this->getRequest()->getParam('product_id')
        ));
    }

    public function deleteAction()
    {
        $this->tryCatchBlock('delete', $this->_getHelper()->getMessagesDelete());
        return $this->_redirect('*/catalog_product/edit',array(
            'id' => $this->getRequest()->getParam('product_id')
        ));
    }

    protected function _getHelper()
    {
        return Mage::helper('branderstickers/stickers');
    }


    protected function tryCatchBlock($action, $messages = array())
    {
        $data = $this->getRequest()->getParams();
        try{
            $sticker = $this->_initStickers();
            $sticker->$action($data);
            $this->_getSession()->addSuccess(
                $messages[0]
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage())
                ->setData($data);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError(
                $messages[1]
            )->setData($data);
        }
    }
}