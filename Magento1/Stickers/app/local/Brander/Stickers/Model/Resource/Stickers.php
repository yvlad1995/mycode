<?php

/**
 * Class Brander_Stickers_Model_Resource_Stickers
 */
class Brander_Stickers_Model_Resource_Stickers extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('branderstickers/stickers', 'entity_id');
    }
}