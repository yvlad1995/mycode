<?php

/**
 * Class Brander_Stickers_Model_Resource_Stickers_Collection
 */
class Brander_Stickers_Model_Resource_Stickers_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('branderstickers/stickers');
    }
}