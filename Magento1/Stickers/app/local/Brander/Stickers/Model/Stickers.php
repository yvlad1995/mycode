<?php

/**
 * Class Brander_Stickers_Model_Stickers
 */
class Brander_Stickers_Model_Stickers extends Mage_Core_Model_Abstract
{
    public function __construct()
    {
        $this->_init('branderstickers/stickers');
    }

    /**
     * @param array $data
     * @return Mage_Core_Model_Abstract
     */
    public function save($data = array())
    {
        if(!count($data))
        {
            return parent::save();
        }

        $this->addData($data);
        $imagePath = $this->_getHelper()->uploadImage();

        if($this->getImage() && isset($data['image']['delete']) || $imagePath)
        {
            $this->_removeImage();
        }

        $this->addData(['image' => $imagePath]);


        return parent::save();
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function delete()
    {
        $this->_removeImage();
        return parent::delete();
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('branderstickers/stickers');
    }

    /**
     *
     */
    protected function _removeImage()
    {
        $this->_getHelper()->removeImage($this->_getImage());
    }

    /**
     * @return mixed
     */
    protected function _getImage()
    {
        $image = $this->getImage();
        if(is_array($image) && isset($image['value']))
        {
            return $image['value'];
        }
        return $image;
    }
}