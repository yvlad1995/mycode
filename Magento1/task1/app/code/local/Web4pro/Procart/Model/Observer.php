<?php

class Web4pro_Procart_Model_Observer
{
    public function addLayout(Varien_Event_Observer $observer)
    {
        if($block = $observer->getEvent()->getBlock())
        {
            if($block instanceof Mage_Catalog_Block_Product_List)
            {
                if($block->getParentBlock() instanceof Mage_Catalog_Block_Category_View || $block->getParentBlock() instanceof Mage_CatalogSearch_Block_Result)
                {
                    $block->setChild('web4proprocart.config',$block->getLayout()->addBlock('web4proprocart/config', 'web4proprocart.config'));
                }
            }
        }

    }
}

