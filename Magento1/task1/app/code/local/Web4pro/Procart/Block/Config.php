<?php

class Web4pro_Procart_Block_Config extends Mage_Core_Block_Template
{
    public function __construct()
    {
        $this->setTemplate("web4pro/procart/config.phtml");
        parent::__construct();
    }

    public function _prepareLayout()
    {
        $this->insert($this->getLayout()->addBlock('gomage_procart/product_list', 'procart.product.list')
            ->setTemplate('gomage/procart/product/list.phtml'));
        return parent::_prepareLayout(); // TODO: Change the autogenerated stub
    }

    protected function _toHtml(){
        if($this->getRequest()->getParam('is_ajax')==1){
            return parent::_toHtml();
        }
        return '';
     }

}
