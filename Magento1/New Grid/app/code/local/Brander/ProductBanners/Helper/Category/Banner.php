<?php

/**
 * Class Brander_ProductBanners_Helper_Category_Banner
 */
class Brander_ProductBanners_Helper_Category_Banner extends Mage_Core_Helper_Abstract
{

    protected $_imagePath = [];

    protected $_banners;

    const DESCTOP_WIDTH = 1366;

    const TABLET_WIDTH = 1024;

    /**
     * @param $data
     * @return mixed
     */
    public function uploadImage($data)
    {
        foreach ($this->getImageNames() as $imageName) {
            if (!empty($_FILES[$imageName]['name'])) {
                try {
                    $this->removeImage($data, $imageName);
                    $uploader = new Varien_File_Uploader($imageName);
                    $uploader->setAllowedExtensions($this->getAllowedExtension());
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $fileName = time() . $imageName . $this->getImageType($_FILES[$imageName]);
                    $uploader->save($this->getPathImageDirectory() . DS . 'catalog' . DS . 'category', $fileName);
                    $this->_imagePath[$imageName] = 'catalog' . DS . 'category' . DS . $fileName;
                }catch (Exception $e) {
                    return Mage::getSingleton('core/session')->addError('Image don\'t uploaded');
                }
            }
        }
    }

    /**
     * @param $image
     * @return string
     */
    public function getImageType($image)
    {
        $types = $this->getAllowedExtension();
        foreach ($types as $type)
        {
            if(strstr($image['type'], $type))
            {
                return '.' .$type;
            }
        }

        return '.png';
    }

    /**
     * @param $data
     * @param $imageName
     */
    public function removeImage($data, $imageName)
    {
        if(isset($data[$imageName]))
        {
            $this->_imagePath[$imageName] = '';
            if(file_exists($this->getPathImageDirectory() . DS . $data[$imageName]))
            {
                unlink($this->getPathImageDirectory() . DS . $data[$imageName]);
            }
        }
    }

    /**
     * @return string
     * IMPORTANT: Do not change:
     * 1) <a href='{{redirect_url}}'>
     * 2) <img src='{{image_url}}'/>
     * This work in js/productbanners/loadBannersCategoryProductList.js
     */
    public function getBannerHtml()
    {
        $html = "<div class='inner-container category-list-banner'>";
        $html .= "<div class='banner hotcategory'>";
        $html .= "<a href='{{redirect_url}}'>";
        $html .= "<img src='{{image_url}}'/>";
        $html .= "</a>";
        $html .= "</div>";
        $html .= "</div>";

        return $html;
    }

    /**
     * @param $banner
     * @param $width
     * @return string
     */
    public function getBannerImageUrl($banner, $width)
    {
        switch (true)
        {
            case $width >= self::DESCTOP_WIDTH:
                return Mage::getBaseUrl('media') . $banner->getImageDesctop();
            case $width >= self::TABLET_WIDTH:
                return Mage::getBaseUrl('media') . $banner->getImageTablet();
            case $width < self::TABLET_WIDTH:
                return Mage::getBaseUrl('media') . $banner->getImageMobile();
            default:
                return $banner->getImageDesctop();
        }
    }

    /**
     * @return array
     */
    public function getImageNames()
    {
        return [
            'image_desctop',
            'image_tablet',
            'image_mobile'
        ];
    }

    /**
     * @return array
     */
    public function getAllowedExtension()
    {
        return ['jpg', 'jpeg', 'gif', 'png'];
    }

    /**
     * @return array
     */
    public function getImagePath()
    {
        return $this->_imagePath;
    }

    /**
     * @return string
     */
    public function getPathImageDirectory()
    {
        return Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function jsonEncode($data)
    {
        return Mage::helper('core')->jsonEncode($data);
    }

}