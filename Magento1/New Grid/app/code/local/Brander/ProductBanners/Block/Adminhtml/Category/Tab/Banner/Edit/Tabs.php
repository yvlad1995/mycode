<?php

/**
 * Class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit_Tabs
 */
class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('category_banner_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('brander_unitopblog')->__('Post Information'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     * prepare the layout
     */
    protected function _prepareLayout()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        $entity = Mage::getModel('brander_productbanners/category_banner')->load($id);

        $this->addTab(
            'info',
            array(
                'label'   => Mage::helper('brander_productbanners')->__('Banner Information'),
                'content' => $this->getLayout()->createBlock(
                    'brander_productbanners/adminhtml_category_tab_banner_edit_tab_banner'
                )
                    ->setEntity($entity)
                    ->toHtml(),
            )
        );

        return parent::_beforeToHtml();
    }

    /**
     * @return mixed
     * Retrieve post entity
     */
    public function getPost()
    {
        return Mage::registry('current_post');
    }
}
