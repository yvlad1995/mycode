<?php

/**
 * Class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit_Tab_Banner
 */
class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit_Tab_Banner extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $helper = Mage::helper('brander_productbanners');
        $model  = $this->getEntity();

        $form     = new Varien_Data_Form();
        $fieldset = $form->addFieldset('general_form', array(
            'legend' => $helper->__('General Information')
        ));

        $fieldset->addField('position', 'text', array(
            'label'    => $helper->__('Position'),
            'required' => true,
            'name'     => 'position',
        ));

        $fieldset->addField('image_desctop', 'image', array(
            'name'     => 'image_desctop',
            'label'    => $helper->__('Image Desctop'),
        ));

        $fieldset->addField('image_tablet', 'image', array(
            'name'     => 'image_tablet',
            'label'    => $helper->__('Image Tablet'),
        ));

        $fieldset->addField('image_mobile', 'image', array(
            'name'     => 'image_mobile',
            'label'    => $helper->__('Image Mobile'),
        ));

        $fieldset->addField('url_redirect', 'text', array(
            'label'    => $helper->__('Url Redirect'),
            'name'     => 'url_redirect',
        ));
        $fieldset->addField('category_id', 'hidden', array(
            'name'     => 'category_id',
        ));

        $fieldset->addField('form_key', 'hidden', array(
            'name'     => 'form_key',
        ));

        $model->addData([
            'category_id' => $this->getRequest()->getParam('categoryId'),
            'form_key'    => Mage::getSingleton('core/session')->getFormKey()
            ]);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
