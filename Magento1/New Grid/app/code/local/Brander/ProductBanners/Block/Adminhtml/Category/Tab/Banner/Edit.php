<?php

/**
 * Class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit
 */
class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'brander_productbanners';
        $this->_controller = 'adminhtml_category_banner';
        $this->removeButton('save');
        $this->removeButton('back');
        $this->removeButton('reset');

        $this->_addButton('back', array(
            'label'     => Mage::helper('adminhtml')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('adminhtml')->__('Reset'),
            'onclick'   => 'setLocation(window.location.href)',
        ), -1);

        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('brander_productbanners')->__('Delete Banner')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('brander_productbanners')->__('Save Banner'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

    }

    /**
     * get the edit form header
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_banner') && Mage::registry('current_banner')->getId()) {
            return Mage::helper('brander_productbanners')->__(
                "Edit Banner"
            );
        } else {
            return Mage::helper('brander_productbanners')->__('Add Banner');
        }
    }

    /**
     * @return string
     */
    public function getFormHtml()
    {
        $block = $this->getLayout()->createBlock('brander_productbanners/adminhtml_category_tab_banner_edit_form', 'form');
        $this->setChild('form', $block);
        $this->getChild('form')->setData('action', $this->getSaveUrl());
        return $this->getChildHtml('form');
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/catalog_category/');
    }
}
