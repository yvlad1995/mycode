<?php

/**
 * Class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner
 */
class Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Brander_ProductBanners_Block_Adminhtml_Category_Tab_Banner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_banner');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * get current category
     *
     * @access public
     * @return Mage_Catalog_Model_Category|null

     */
    public function getCategory()
    {
        return Mage::registry('current_category');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('brander_productbanners/category_banner')->getCollection()->addFieldToFilter('category_id', $this->getCategory()->getId());
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * Prepare the columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('brander_productbanners')->__('Id'),
                'type'   => 'number',
                'align'  => 'left',
                'index'  => 'entity_id',
            )
        );

        $this->addColumn(
            'position',
            array(
                'header'         => Mage::helper('brander_productbanners')->__('Position'),
                'name'           => 'position',
                'width'          => 60,
                'type'           => 'number',
                'validate_class' => 'validate-number',
                'index'          => 'position',
            )
        );

        $this->addColumn(
            'url_redirect',
            array(
                'header' => Mage::helper('brander_productbanners')->__('Url Redirect'),
                'header_css_class'  => 'a-center',
                'type'   => 'text',
                'name'   => 'url_redirect',
                'align'  => 'center',
                'index'  => 'url_redirect'
            )
        );

        return parent::_prepareColumns();
    }

    /**
     * @param $item
     * @return string
     * get row url
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('*/category_banner/edit', array(
            'id'         => $item->getId(),
            'categoryId' => $this->getCategory()->getId(),
        ));
    }

    /**
     * @return string
     *  get grid url
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            'adminhtml/unitopblog_post_catalog_category/postsgrid',
            array(
                'categoryId' => $this->getCategory()->getId(),
            )
        );
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        return $this->unsetChild('reset_filter_button');
    }

    /**
     * @return string
     */
    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();//get the parent class buttons
        $addButton = $this->getLayout()->createBlock('adminhtml/widget_button') //create the add button
        ->setData(array(
            'label'     => Mage::helper('adminhtml')->__('Add New Banner'),
            'onclick'   => "setLocation('".$this->getUrl('*/category_banner/new', array(
                    'categoryId'          => $this->getCategory()->getId(),
                ))."')",
            'class'   => 'banner'
        ))->toHtml();
        return $addButton.$html;
    }
}