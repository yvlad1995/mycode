<?php

$this->startSetup();
$table = $this->getConnection()
    ->newTable($this->getTable('brander_productbanners/category_banner'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ),
        'Entity ID'
    )
    ->addColumn(
        'position',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => false,
            'default'   => '0',
        ),
        'Position'
    )
    ->addColumn(
        'url_redirect',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(
            'unsigned'  => true,
            'nullable'  => true,
        ),
        'Url Redirect'
    )
    ->addColumn(
        'image_desctop',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(),
        'Image Desctop'
    )
    ->addColumn(
        'image_desctop',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(),
        'Image Desctop'
    )
    ->addColumn(
        'image_tablet',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(),
        'Image Tablet'
    )
    ->addColumn(
        'image_mobile',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        null,
        array(),
        'Image Mobile'
    )
    ->addColumn(
        'category_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER ,
        null,
        array(),
        'Category Id'
    )
    ->addColumn(
        'store_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER ,
        null,
        array(),
        'Store Id'
    )
    ->setComment('Category Banner Table');
$this->getConnection()->createTable($table);
$this->endSetup();
