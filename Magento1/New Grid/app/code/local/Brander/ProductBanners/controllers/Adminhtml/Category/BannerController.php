<?php

/**
 * Class Brander_ProductBanners_Adminhtml_Category_BannerController
 */
class Brander_ProductBanners_Adminhtml_Category_BannerController extends Mage_Adminhtml_Controller_Action
{
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initBanner();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * @return $this|bool|Mage_Core_Controller_Varien_Action
     */
    public function saveAction()
    {
        if(!$this->_validateFormKey()){
            $this->_getSession()->addError('Please reload this page and try again!');
            return $this->_redirect('*/category_banner/edit', array('id' => Mage::app()->getRequest()->getParam('id')));
        }

        $_helper = $this->_getHelper();
        $post = $this->getRequest()->getPost();
        $data = $this->validateData($post);
        $_helper->uploadImage($data);
        $this->deleteImage();
        $images = $_helper->getImagePath();

        if ($data) {
            $banner  = $this->_initBanner();
            $data = array_merge($data, $images);
            $banner->addData($data);

            try {
                $banner->save();
                $this->_getSession()->addSuccess(
                    $_helper->__('Banner was saved')
                );
            } catch (Mage_Core_Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage())
                    ->setData($data);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError(
                    $_helper->__('Error saving banner')
                )->setData($data);
            }
        }

        $this->_redirect('*/catalog_category/', array('store' => Mage::app()->getStore()->getId()));
        return true;
    }

    /**
     * @return mixed
     */
    protected function validateData($data = null)
    {
        foreach ($this->_getHelper()->getImageNames() as $imageName)
        {
            if(isset($data[$imageName]) && is_array($data[$imageName]))
            {
                $data[$imageName] = $data[$imageName]['value'];
            }
        }
        return $data;
    }

    /**
     * @return mixed
     */
    protected function _initBanner()
    {
        $id  = (int) $this->getRequest()->getParam('id');
        $banner    = Mage::getModel('brander_productbanners/category_banner')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if ($id) {
            $banner->load($id);
            Mage::register('current_banner', $banner);
        }

        return $banner;
    }


    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $banner = $this->_initBanner();
        $this->deleteImage($banner->getData(), true);
        try {
            $banner->delete($id);
            $this->_getSession()->addSuccess(
                $this->_getHelper()->__('Banner was deleted')
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError(
                $this->_getHelper()->__('Error deleted banner')
            );
        }

        $this->_redirect('*/catalog_category/', array('store'=>Mage::app()->getStore()->getId()));
    }

    /**
     * @param null $data
     * @param bool $delete
     *
     * THIS METHOD NEED REFACTOR!!!
     */
    protected function deleteImage($data = null ,$delete = false)
    {
        if(!$data)
        {
            $data = $this->getRequest()->getPost();
        }
        foreach ($this->_getHelper()->getImageNames() as $imageName) {
            if(!$delete)
            {
                $delete = isset($data[$imageName]['delete']) ? true : false;
            }
            if(isset($data[$imageName]) && $delete)
            {
                $this->_getHelper()->removeImage($this->validateData($data), $imageName);
            }
        }
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_productbanners/category_banner');
    }

    /**
     * @return bool
     */
    protected function _validateFormKey()
    {
        if (!($formKey = $this->getRequest()->getPost('form_key', null))
            || $formKey != Mage::getSingleton('core/session')->getFormKey()) {
            return false;
        }
        return true;
    }
}