<?php

/**
 * Class Brander_ProductBanners_Category_BannerController
 */
class Brander_ProductBanners_Category_BannerController extends Mage_Core_Controller_Front_Action
{
    /**
     * @return bool|void
     */
    public function loadBannerAction()
    {
        if($this->isAjax())
        {
            $this->_redirect('/');
            return;
        }

        $categoryId = $this->getRequest()->getParam('categoryId', null);
        $width      = $this->getRequest()->getParam('width', null);

        try {
            $model = Mage::getModel('brander_productbanners/category_banner',
                array('categoryId' => $categoryId, 'width' => $width)
            );
            echo $model->getJsonDataBanners();
        }catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function isAjax()
    {
        if(!$this->getRequest()->isXmlHttpRequest())
            return true;
        return false;
    }
}
