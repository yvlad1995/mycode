<?php

/**
 * Class Brander_ProductBanners_Model_Category_Banner
 */
class Brander_ProductBanners_Model_Category_Banner extends Mage_Core_Model_Abstract
{
    protected $_category   = null;

    protected $_banners    = null;

    protected $_productQty = null;

    protected $_width      = 1366;

    protected $_iterators = [];

    const FIRST_ITEM_ITERATOR = 6;

    const ANY_ITEM_ITERATOR = 18;

    /**
     * Brander_ProductBanners_Model_Category_Banner constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        $this->_construct();
        if(!$this->_banners && count($data))
        {
            $this->_category = Mage::getModel('catalog/category')->load($data['categoryId']);
            $this->_width = $data['width'];
            $this->_productQty = count($this->_category->getProductCollection()->getItems());
            $this->setBanners()
                ->setIterators()
                ->sortBanners()
                ->setIteratorInBanner();
        }
    }

    /**
     * Init model
     */
    public function _construct()
    {
        $this->_init('brander_productbanners/category_banner');
    }

    /**
     * @return $this
     */
    protected function sortBanners()
    {
        function mySort($first,$last)
        {
          if($first->getPosition() < $last->getPosition())
          {
              return -1;
          }
          elseif($first->getPosition() > $last->getPosition())
          {
              return 1;
          }
          else return 0;
        }

        usort($this->_banners,"mySort");

        return $this;
    }

    /**
     * @return $this
     */
    protected function setBanners()
    {
        if(!$this->_banners)
        {
            $_banners = $this->getCollection()
                ->addFieldToFilter('category_id', $this->_category->getId());
            $this->_banners = $_banners->getItems();
        }
        return $this;
    }

    /**
     * @return $this
     * We get the number of working banners, for the category
     */
    protected function setIterators()
    {
        $qty = $this->_productQty;

        if($qty >= self::FIRST_ITEM_ITERATOR) {
            $item = self::FIRST_ITEM_ITERATOR;
            $qty -= self::FIRST_ITEM_ITERATOR;
            $iterators = floor($qty / self::ANY_ITEM_ITERATOR);
            $this->_iterators[] = $item;

            for ($i = $iterators; $i > 0; $i--) {
                $item += self::ANY_ITEM_ITERATOR;
                $this->_iterators[] = $item;
            }
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function setIteratorInBanner()
    {
        $_bannerTemplate = $this->getBannerHelper()->getBannerHtml();
        foreach ($this->_iterators as $key => $iterator)
        {
            if(!isset($this->_banners[$key])) return $this;
            $this->_banners[$key]->setIterator($iterator)
                ->setTemplate($_bannerTemplate)
                ->setImageUrl(
                    $this->getBannerHelper()
                    ->getBannerImageUrl($this->_banners[$key], $this->_width)
                );
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJsonDataBanners()
    {
        $_banners = [];
        foreach ($this->_banners as $banner) {
            $_banners[] = $banner->getData();
        }

        return $this->getBannerHelper()->jsonEncode($_banners);
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function getBannerHelper()
    {
        return Mage::helper('brander_productbanners/category_banner');
    }

}