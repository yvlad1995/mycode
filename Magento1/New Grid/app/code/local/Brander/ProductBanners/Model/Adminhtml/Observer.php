<?php

class Brander_ProductBanners_Model_Adminhtml_Observer
{
    public function addCategoryTab(Varien_Event_Observer $observer)
    {
        $tabs = $observer->getEvent()->getTabs();
        $content = $tabs->getLayout()->createBlock(
            'brander_productbanners/adminhtml_category_tab_banner',
            'category.banner.grid'
        )->toHtml();
        $serializer = $tabs->getLayout()->createBlock(
            'adminhtml/widget_grid_serializer',
            'category.banner.grid.serializer'
        );
        $serializer->initSerializerBlock(
            'category.banner.grid',
            'getSelectedPosts',
            'banner',
            'category_banner'
        );
        $serializer->addColumnInputName('position');
        $content .= $serializer->toHtml();
        $tabs->addTab(
            'banner',
            array(
                'label'   => Mage::helper('brander_unitopblog')->__('Banner'),
                'content' => $content,
            )
        );
        return $this;
    }
}
