<?php

/**
 * Class Brander_ProductBanners_Model_Resource_Category_Banner_Collection
 */
class Brander_ProductBanners_Model_Resource_Category_Banner_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('brander_productbanners/category_banner');
    }
}
