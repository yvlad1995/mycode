<?php

/**
 * Class Brander_ProductBanners_Model_Resource_Category_Banner
 */
class Brander_ProductBanners_Model_Resource_Category_Banner extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('brander_productbanners/category_banner', 'entity_id');
    }
}