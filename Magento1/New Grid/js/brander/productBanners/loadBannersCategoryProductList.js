var loadBannersCategoryProductList = Class.create({
    initialize: function(url, categoryId) {
        this.url = url;
        this.categoryId = categoryId;
        this.template = '';
    },

    load: function () {
        this.ajaxRequest();
    },

    ajaxRequest: function () {
        jQuery.ajax({
            url: this.url,
            data: {
                categoryId: this.categoryId,
                width: window.innerWidth
            },
            dataType: 'json',
            success: function (dataBanners) {
                var count = dataBanners.length;
                for(var i = 0; i < count; i++)
                {
                    if(dataBanners[i].iterator){
                        this.setTemplate(dataBanners[i].template, dataBanners[i].url_redirect, dataBanners[i].image_url);
                        jQuery('#product-banner-' + dataBanners[i].iterator).after(this.template);
                    }
                }
            },

            setTemplate: function(template, url_redirect, image_url) {
                var tmp_template = template.replace("<a href='{{redirect_url}}'>", "<a href='" + url_redirect + "'>");
                this.template = tmp_template.replace("<img src='{{image_url}}'/>", "<img src='" + image_url + "'/>");
            }
        });
    },
});