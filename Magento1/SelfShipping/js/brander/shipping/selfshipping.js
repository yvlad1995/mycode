// function selfShipping(text, departments) {
//     var code = jQuery('select[name="shipping_method"]').find('option:selected').val();
//     if(code === 'selfshipping_' && jQuery('#selfshipping_selfshipping').length < 1)
//     {
//         count = departments.length;
//         var cityName = jQuery("[name='billing[city]']").val();
//         var html = '<select id="shipping-departments" name="shipping_department">';
//         for(var i = 0; i < count; i++)
//         {
//             if(departments[i].cityName == cityName)
//             {
//                 html += '<option value="' + departments[i].id + '">' + departments[i].description + '</option>';
//             }
//         }
//         text = text.replace('<select id="shipping-departments" name="shipping_department">', html);
//         jQuery('#shipping_method_field').after(text);
//     } else if(code !== 'selfshipping_') {
//         jQuery('#selfshipping_selfshipping').remove();
//     }
// }

var selfShipping = Class.create({
    // selfshipping_
    initialize: function(text, departments, code) {
        this.text = text;
        this.departments = departments;
        this.code = code;
        this.shippingMethodName = 'select[name="shipping_method"]';
        this.shippingMethodField = '#shipping_method_field';
        this.shippingOptionSelected = 'option:selected';
        this.blockSelector = '#selfshipping_selfshipping';
        this.billingFieldSelector = "[name='billing[city]']";
        this.html = '<select id="shipping-departments" name="shipping_department">';
    },

    reloadShipping: function () {
        var code = jQuery(this.shippingMethodName).find(this.shippingOptionSelected).val();
        if(code === this.code && jQuery(this.blockSelector).length < 1)
        {
            this.addOption();
        } else if(code !== this.code) {
            jQuery(this.blockSelector).remove();
        }
    },

    addOption: function () {
        var cityName = jQuery(this.billingFieldSelector).val();
        var html = this.html;
        count = this.departments.length;
        for(var i = 0; i < count; i++)
        {
            if(this.departments[i].cityName == cityName)
            {
                html += '<option value="' + this.departments[i].id + '">' + this.departments[i].description + '</option>';
            }
        }
        text = this.text.replace(this.html, html);

        jQuery(this.shippingMethodField).after(text);
    }

});
