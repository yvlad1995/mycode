<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'shipping_department', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Shipping Department'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_address'), 'shipping_department', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Shipping Department'
    ));

$installer->endSetup();