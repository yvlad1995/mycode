<?php

/**
 * Class Brander_Shipping_Helper_Data
 */
class Brander_Shipping_Helper_Map extends Brander_Shipping_Helper_Data
{
    const ACTIVE  = 'brander_map/config/active';

    const API_KEY = 'brander_map/config/api_key';

    const API_ADDRESS = 'brander_map/config/api_address';

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->getStoreConfig(self::API_KEY);
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->getStoreConfig(self::ACTIVE);
    }

    /**
     * @return mixed
     */
    public function getApiAddress()
    {
        return $this->getStoreConfig(self::API_ADDRESS);
    }
}
