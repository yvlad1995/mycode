<?php

/**
 * Class Brander_Shipping_Helper_Data
 */
class Brander_Shipping_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $code
     * @return bool
     */
    public function getSroreIdByCode($code)
    {
        $stores = Mage::app()->getStores();
        foreach ($stores as $store) {
            if($store->getCode() == $code) {
                return $store->getStoreId();
            }
        }

        return false;
    }

    /**
     * @param bool $storeId
     * @return mixed
     */
    public function getStoreCodeByStoreId($storeId = false)
    {
        if (!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        $stores = Mage::app()->getStores();
        foreach ($stores as $store) {
            if($store->getId() == $storeId) {
                return $store->getCode();
            }
        }
    }

    /**
     * @param $text
     * @return mixed
     */
    public function jsonEncode($text)
    {
        return Mage::helper('core')->jsonEncode($text);
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getStoreConfig($path)
    {
        return Mage::getStoreConfig($path);
    }

    /**
     * @param $path
     * @return false|Mage_Core_Model_Abstract
     */
    public function getModel($path)
    {
        return Mage::getModel($path);
    }
}