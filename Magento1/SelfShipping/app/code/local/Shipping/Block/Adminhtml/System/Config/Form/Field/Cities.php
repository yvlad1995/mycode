<?php

/**
 * Class Brander_Shipping_Block_Adminhtml_System_Config_Form_Field_Cities
 */
class Brander_Shipping_Block_Adminhtml_System_Config_Form_Field_Cities extends Mage_Core_Block_Html_Select
{
    protected $_storeCode = 'ru';

    /**
     * Brander_Shipping_Block_Adminhtml_System_Config_Form_Field_Cities constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        if (isset($args['store_departments'])) {
            $this->_storeCode = $args['store_departments'];
        }
        parent::__construct($args);
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        $options = Mage::getSingleton('brander_shipping/cities')
            ->toOptionArray($this->_storeCode);
        array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));

        foreach ($options as $option) {
            $this->addOption($option['value'], $option['label']);
        }

        return parent::_toHtml();
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

}