<?php

/**
 * Class Brander_Shipping_Block_Adminhtml_System_Config_Departments
 */
class Brander_Shipping_Block_Adminhtml_System_Config_Departments
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_itemRenderer = [];

    public function _prepareToRender()
    {
        $this->addColumn('id', array(
            'label' => Mage::helper('brander_shipping')->__('Id'),
            'style' => 'width:50px',
        ));

        $this->addColumn('sort', array(
            'label' => Mage::helper('brander_shipping')->__('Sort Order'),
            'style' => 'width:50px',
        ));

        $this->addColumn('latitude', array(
            'label' => Mage::helper('brander_shipping')->__('Latitude'),
            'style' => 'width:100px',
        ));

        $this->addColumn('longitude', array(
            'label' => Mage::helper('brander_shipping')->__('Longitude'),
            'style' => 'width:100px',
        ));

        $this->addColumn('city_ru', array(
            'label' => Mage::helper('brander_shipping')->__('City Ru'),
            'style' => 'width:80px',
            'renderer' => $this->_getRenderer('ru'),
        ));

        $this->addColumn('description_ru', array(
            'label' => Mage::helper('brander_shipping')->__('Description Ru'),
            'style' => 'width:100px',
        ));

        $this->addColumn('city_ua', array(
            'label' => Mage::helper('brander_shipping')->__('City Ua'),
            'style' => 'width:80px',
            'renderer' => $this->_getRenderer('ua'),
        ));

        $this->addColumn('description_ua', array(
            'label' => Mage::helper('brander_shipping')->__('Description Ua'),
            'style' => 'width:100px',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('brander_shipping')->__('Add');
    }

    /**
     * @param string $code
     * @return mixed
     */
    protected function  _getRenderer($code = 'ru')
    {
        if (!isset($this->_itemRenderer[$code])) {
            $this->_itemRenderer[$code] = $this->getLayout()->createBlock(
                'brander_shipping/adminhtml_system_config_form_field_cities', '',
                [
                    'is_render_to_js_template' => true,
                    'store_departments'       => $code
                ]

            );
        }

        return $this->_itemRenderer[$code];
    }

    /**
     * @param Varien_Object $row
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getRenderer('ru')
                ->calcOptionHash($row->getData('city_ru')),
            'selected="selected"'
        )
            ->setData(
                'option_extra_attr_' . $this->_getRenderer('ua')
                    ->calcOptionHash($row->getData('city_ua')),
                'selected="selected"'
            );
    }
}