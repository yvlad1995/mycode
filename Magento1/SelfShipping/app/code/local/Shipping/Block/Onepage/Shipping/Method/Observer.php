<?php

/**
 * Class Brander_Shipping_Block_Onepage_Shipping_Method_Observer
 */
class Brander_Shipping_Block_Onepage_Shipping_Method_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     * @return bool
     * Use on checkout page : Shipping method by city
     */
    public function getShippingRates(Varien_Event_Observer $observer)
    {
        $groups = $observer->getEvent()->getGroups();

        $params = $this->_getPost();

        $city = isset($params['billing']['city']) ? $params['billing']['city'] : null;

        $options = array_flip($this->_getModelCities()->getOptionArray());

        if (isset($options[$city])) {
            $configs = $this->_getModelCarriers()->getConfigData('departments') ?
                unserialize($this->_getModelCarriers()->getConfigData('departments')) : [];

            foreach ($configs as $config) {
                if (isset($config['city_ru']) && $config['city_ru'] == $options[$city] ||
                    isset($config['city_ua']) && $config['city_ua'] == $options[$city] ) {
                    return true;
                }
            }
        }

        return $groups->unsetData('selfshipping');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModelCities()
    {
        return Mage::getModel('brander_shipping/cities');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModelCarriers()
    {
        return Mage::getModel('brander_shipping/carrier');
    }

    /**
     * @return mixed
     */
    protected function _getPost()
    {
        return Mage::app()->getRequest()->getPost();
    }
}