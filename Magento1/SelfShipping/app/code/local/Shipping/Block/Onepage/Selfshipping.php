<?php

/**
 * Class Brander_Shipping_Block_Onepage_Selfshipping
 */
class Brander_Shipping_Block_Onepage_Selfshipping extends Mage_Core_Block_Template
{

    /**
     * @return mixed
     */
    public function getDepartments()
    {
        $arr = [];
        $cities = array_flip($this->getArrayCities());
        foreach ($this->_getCarrierModel()->getDepartments() as $department) {
            if (in_array($department['cityId'], $cities)) {
                $department['cityName'] = array_search($department['cityId'], $cities);
                $arr[] = $department;
            }
        }
        return $this->_getHelper()->jsonEncode($arr);
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getCitiesModel()
    {
        return Mage::getModel('brander_shipping/cities');
    }

    /**
     * @return mixed
     */
    public function getStoreCodeByStoreId()
    {
        return Mage::helper('brander_shipping')->getStoreCodeByStoreId($this->getStoreId());
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    /**
     * @return mixed
     */
    public function getArrayCities()
    {
        return $this->_getCitiesModel()->getArrayCities($this->getStoreCodeByStoreId());
    }

    /**
     * @param string $path
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_shipping');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getCarrierModel()
    {
        return Mage::getModel('brander_shipping/carrier');
    }

    public function getHtml()
    {
       return $this->_getHelper()->jsonEncode('<div id="selfshipping_selfshipping" class="field">
                    <label for="shipping_method_select">' . $this->__('Methods:') . '</label>
                    <div class="input-box">
                        <select id="shipping-departments" name="shipping_department"></select> 
                    </div>
                </div>');
    }
}