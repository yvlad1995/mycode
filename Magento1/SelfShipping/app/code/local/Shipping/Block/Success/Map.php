<?php

/**
 * Class Brander_Shipping_Block_Success_Map
 */
class Brander_Shipping_Block_Success_Map extends Mage_Core_Block_Template
{

    const KEY      = '?key=';

    const CALLBACK = '&callback=initMap';

    protected $_coordinates;

    /**
     * @return bool|string
     */
    public function initMap()
    {
        if (!$this->_getActive()) {
            return false;
        }

        return $this->_getApiAddress() . self::KEY . $this->_getApiKey() . self::CALLBACK;
    }

    /**
     * @return bool
     */
    public function getLatitude()
    {
        $coordinates = $this->_getMapsCoordinates();

        if(!empty($coordinates['latitude'])) {
            return $coordinates['latitude'];
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getLongitude()
    {
        $coordinates = $this->_getMapsCoordinates();

        if(!empty($coordinates['longitude'])) {
            return $coordinates['longitude'];
        }

        return false;
    }

    /**
     * @return mixed
     */
    protected function _getMapsCoordinates()
    {
        return Mage::registry('maps_coordinates');
    }

    /**
     * @return mixed
     */
    protected function _getActive()
    {
        return $this->_getHelper()->getActive();
    }

    /**
     * @return mixed
     */
    protected function _getApiKey()
    {
        return $this->_getHelper()->getApiKey();
    }

    /**
     * @return mixed
     */
    protected function _getApiAddress()
    {
        return $this->_getHelper()->getApiAddress();
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_shipping/map');
    }
}
