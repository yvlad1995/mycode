<?php

/**
 * Class Brander_Shipping_Model_Carrier
 */
class Brander_Shipping_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_departments;

    protected $_code = 'selfshipping';

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return array('self_shipping' => 'Self Shipping');
    }

    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return bool|false|Mage_Core_Model_Abstract
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigData('active')) {
            return false;
        }
        $result = Mage::getModel('shipping/rate_result');
        $result->append($this->_getSelfShippingRate());
        return $result;
    }

    /**
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getSelfShippingRate()
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod($this->getConfigData('type'));
        $rate->setMethodTitle($this->getConfigData('name'));
        $rate->setPrice($this->getConfigData('price'));
        $rate->setDepartments($this->getConfigData('departments'));

        return $rate;
    }

    public function getDepartments()
    {
        if ($this->_departments) {
            return $this->_departments;
        }

        $departments = [];
        foreach (unserialize($this->getConfigData('departments')) as $department) {
            $department['description'] = $this->_getDepartmentDescription($department);
            $department['cityId'] = $this->_getDepartmentCityId($department);
            $departments[] = $department;
        }
        usort($departments, array($this,'_sortDepartments'));
        $this->_departments = $departments;
        return $this->_departments;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public function getDepartment($id)
    {
        $departments = $this->getDepartments();
        foreach ($departments as $department)
        {
            if($department['id'] == $id)
            {
                return $department;
            }
        }

        return false;
    }

    /**
     * @param $department
     * @return mixed
     */
    protected function _getDepartmentDescription($department)
    {
        $storeCode = $this->_getHelper()->getStoreCodeByStoreId();
        return $department['description_' . $storeCode];
    }

    /**
     * @param $department
     * @return mixed
     */
    protected function _getDepartmentCityId($department)
    {
        $storeCode = $this->_getHelper()->getStoreCodeByStoreId();
        return $department['city_' . $storeCode];
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_shipping/carrier');
    }

    /**
     * @param $first
     * @param $last
     * @return int
     */
    protected function _sortDepartments($first,$last)
    {
        if($first['sort'] < $last['sort']){
            return -1;
        }elseif($first['sort'] > $last['sort']){
            return 1;
        }
        else return 0;
    }
}