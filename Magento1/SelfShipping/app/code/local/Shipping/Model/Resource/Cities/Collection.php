<?php

/**
 * Class Brander_Shipping_Model_Resource_Cities_Collection
 */
class Brander_Shipping_Model_Resource_Cities_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_shipping/cities');
    }
}