<?php

/**
 * Class Brander_Shipping_Model_Resource_Cities
 */
class Brander_Shipping_Model_Resource_Cities extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_shipping/ukraine_cities', 'entity_id');
    }
}