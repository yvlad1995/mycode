<?php

/**
 * Class Brander_Shipping_Model_Api
 */
class Brander_Shipping_Model_Api
{
	protected $_apiKey;

    /**
     * Brander_Shipping_Model_Api constructor.
     */
	public function __construct()
	{
		$this->_apiKey = Mage::helper('brander_newpost')->getApiKey();
	}

    /**
     * @param $xml
     * @return SimpleXMLElement
     */
	protected function _sendRequest($xml)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/xml/');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);

		return simplexml_load_string($response);
	}

    /**
     * @return array|bool
     */
	protected function _getCities()
	{
		$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><file/>');
		$xml->addChild('apiKey', $this->_apiKey);
		$xml->addChild('modelName', 'Address');
		$xml->addChild('calledMethod', 'getCities');
		$response = $this->_sendRequest($xml->asXML());

		if (!$response) {
			return false;
		}

		try {
            return Mage::getModel('brander_shipping/cities')->saveAddress($response);
		} catch (Exception $e) {
			Mage::logException($e);
			return array();
		}
	}

    /**
     * @return array|bool
     */
	public function processImport()
	{
		return $this->_getCities();
	}
}
