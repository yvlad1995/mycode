<?php

/**
 * Class Brander_Shipping_Model_Observer
 */
class Brander_Shipping_Model_Observer
{
    /**
     * @param $observer
     */
    public function saveShippingMethod($observer)
    {
        $request = $observer->getEvent()->getRequest();

        $quote = $observer->getEvent()->getQuote();

        if (stristr($request->getParam('shipping_method'),'selfshipping')) {
            $shippingDepartment = $this->_getDepartment($request->getParam('shipping_department'));
            $quote->getShippingAddress()->setStreet($shippingDepartment['description']);
            $quote->getShippingAddress()->setShippingDepartment($shippingDepartment['id']);
            $quote->collectTotals()->save();
        }
    }

    /**
     * @param $departmentId
     * @return mixed
     */
    protected function _getDepartment($departmentId)
    {
        return Mage::getModel('brander_shipping/carrier')->getDepartment($departmentId);
    }

    /**
     * @param Varien_Event_Observer $observer
     * add coordinates in register
     */
    public function successAction(Varien_Event_Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        foreach ($orderIds as $id) {
            $order = $this->__getHelper()->getModel('sales/order')->load($id);

            $quote = $this->__getHelper()->getModel('sales/quote')->load($order->getQuoteId());

            $departmentId = $quote->getShippingAddress()->getShippingDepartment();

            if (!$departmentId) {
                return false;
            }

            $department = $this->__getHelper()->getModel('brander_shipping/carrier')->getDepartment($departmentId);

            Mage::register('maps_coordinates',
                [
                    'latitude' => $department['latitude'],
                    'longitude' => $department['longitude']
                ]
            );
        }
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function __getHelper()
    {
        return Mage::helper('brander_shipping');
    }
}
