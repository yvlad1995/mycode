<?php

/**
 * Class Brander_Shipping_Model_Cities
 */
class Brander_Shipping_Model_Cities extends Mage_Core_Model_Abstract
{
    protected $_cityIds = [];

    protected function _construct()
    {
        $this->_init('brander_shipping/cities');
    }

    /**
     * @param $response
     * save address sync by api New Post
     */
    public function saveAddress($response)
    {
        $sql = 'INSERT INTO '.$this->getTableName().' 
        (description, city_id, entity_description, store_id) VALUES ';

        foreach ($response->data->item as $key => $item) {
            if(!in_array($item->CityID->__toString(), $this->getAlreadyCityIds())) {
                $sql .= '(' .
                    $this->_getConnection()->quote($item->Description->__toString()) . ', ' .
                    $item->CityID->__toString() . ', ' .
                    $this->_getConnection()->quote($item->SettlementTypeDescription->__toString()) . ', ' .
                    $this->_getHelper()->getSroreIdByCode('ua')
                    . '), ';

                $sql .= '(' .
                    $this->_getConnection()->quote($item->DescriptionRu->__toString()) . ', ' .
                    $item->CityID->__toString() . ', ' .
                    $this->_getConnection()->quote($item->SettlementTypeDescriptionRu->__toString()) . ', ' .
                    $this->_getHelper()->getSroreIdByCode('ru')
                    . '), ';
            }
        }

        $sql = substr($sql, 0, -2);

        $this->_getConnection()->query($sql);
    }

    /**
     * @return array
     */
    protected function getAlreadyCityIds()
    {
        if ($this->_cityIds) {
            return $this->_cityIds;
        }

        $alreadyCities = $this->getCollection()->getItems();

        $this->_cityIds = [];

        foreach ($alreadyCities as $city) {
            $this->_cityIds[] = $city->getCityId();
        }

        return $this->_cityIds;
    }

    /**
     * @return mixed
     */
    protected function _getConnection()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return Mage::getSingleton('core/resource')->getTableName('brander_shipping/ukraine_cities');
    }

    /**
     * @param $code
     * @return mixed
     */
    protected function _getCitiesArray($code)
    {
        return $this->getCollection()->addFieldToFilter('store_id', $this->_getHelper()->getSroreIdByCode($code))->getItems();
    }

    /**
     * @param bool $code
     * @return array
     */
    public function toOptionArray($code = true)
    {
        $options = [];
        foreach ($this->_getCitiesArray($code) as $item) {
            $options[] = [
                'value' => $item->getCityId() ,
                'label' => $item->getDescription()
            ];
        }

        return $options;
    }

    /**
     * @param bool $code
     * @return array
     */
    public function getArrayCities($code = true)
    {
        $options = [];
        foreach ($this->_getCitiesArray($code) as $item) {
            $options[$item->getCityId()] = $item->getDescription();
        }

        return $options;
    }

    /**
     * @return array
     */
    public function getOptionArray()
    {
        $options = array();
        $collection = $this->getCollection()
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId());
        foreach ($collection as $city) {
            $options[$city->getCityId()] = $city->getDescription();
        }
        asort($options);
        return $options;
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_shipping/cities');
    }
}