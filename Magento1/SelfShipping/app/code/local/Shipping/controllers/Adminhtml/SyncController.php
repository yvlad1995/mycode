<?php

/**
 * Class Brander_Shipping_Adminhtml_SyncController
 */
class Brander_Shipping_Adminhtml_SyncController extends Mage_Adminhtml_Controller_Action
{
    public function processAction()
    {
        Mage::getModel('brander_shipping/api')->processImport();
    }
}