var loadBannerCategory = Class.create({

    initialize: function(url) {
        this.url = url;
    },

    load: function (category) {
        var id = jQuery(category).attr('id').replace('category-id-', '');
        var requestId = 'id=' + id;
        var isBanner = jQuery('#image-banner-' + id).length;

        data = {};
        data.selector = category;
        data.attr = 'class';
        data.find = [
            'level0',
            'level1',
        ];

        if(id !== 'undefined'  && !isBanner && this.isImageTrueLevel(data))
        {
            ScreenWidth = 'width=' + window.innerWidth;

            var url = this.url + requestId + '&' + ScreenWidth;

            this.ajaxRequest(category, url);
        }
    },

    ajaxRequest: function (category, url) {
        jQuery.ajax({
            url: url,
            dataType: 'json',
            success: function (banner) {
                jQuery(category).append(banner);
            }
        });
    },

    isImageTrueLevel: function (data) {
        for(var i = data.find.length - 1; i >= 0; i--)
        {
            var result = jQuery(data.selector).attr(data.attr).indexOf(data.find[i]);
            if(result > 0)
            {
                return true;
            }
        }
        return false;
    }
});