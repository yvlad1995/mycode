var loadBrandsCategory = Class.create({

    initialize: function(url) {
        this.url = url;
        InProgressAjaxBrandsLoad = false;
    },

    load: function (category) {
        if(InProgressAjaxBrandsLoad == false)
        {
            InProgressAjaxBrandsLoad = true;

            this.ajaxRequest(category);
        }
    },

    ajaxRequest: function (category) {
        jQuery.ajax({
            url: this.url,
            dataType: 'json',
            success: function (banner) {
                jQuery('#brands-category #brands-subcategory').append(banner);
            }
        });
    }
});