jQuery(document).ready(function(){

    var loadBrandsForCategory = new loadBrandsCategory(urlControllerLoadBrands);

    var megaMenus = jQuery('li.nav-item.banner-megamenu');

    var el = jQuery('#brands-category');

    loadBrandsForCategory.load(el);

    for(var i = 0; i < megaMenus.length; i++)
    {
        loadBannerForCategory.load(megaMenus[i]);
    }
});