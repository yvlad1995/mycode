<?php

/**
 * Class Brander_MegaMenu_Model_Brands
 */
class Brander_MegaMenu_Model_Brands
{
    /**
     * @return array
     */
    public function sortBrands()
    {
        $brands = $this->getBrands()->getItems();
        $sortBrands = array();
        $newBrands = array();
        foreach($brands as $key => $items)
        {
            foreach($items as $letter => $item)
            {
                foreach($item as $optionKey => $option)
                {
                    if($this->validateOption($option))
                    {
                        $attrModel = $this->getAttributeOptionModel()
                            ->setIdFilter($option['value']);
                        $attrData = $attrModel->getData();
                        $sortOrder = $attrData[$optionKey]['sort_order'];
                        $item[$optionKey]['letter'] = $letter;
                    }
                }
                $newBrands[$sortOrder] = $item;
            }
            ksort($newBrands);
            $sortBrands[$key] = $newBrands;
        }
        return $sortBrands;
    }

    /**
     * @return Object
     */
    protected function getAttributeOptionModel()
    {
        return Mage::getResourceModel('eav/entity_attribute_option_collection');
    }

    /**
     * @param $option
     * @return bool
     */
    protected function validateOption($option)
    {
        $optionRequire = Mage::helper('brander_megamenu/brands')->getListRequireOptionField();
        foreach ($optionRequire as $item)
        {
            if(!isset($option[$item])) return false;
        }
        return true;
    }

    /**
     * @return object
     */
    public function getBrands()
    {
        return Mage::getBlockSingleton('brander_layerednavigation/list');
    }
}