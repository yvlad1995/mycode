<?php

/**
 * Class Brander_MegaMenu_Model_Banner
 */
class Brander_MegaMenu_Model_Banner
{
    /**
     * @var Mage_Core_Helper_Abstract
     */
    protected $_helper;

    protected $_id;

    protected $_width;

    protected $_image;

    protected $_urlRedirect;

    const DESCTOP = 'desctop';

    const TABLET = 'tablet';

    const MOBILE = 'mobile';

    /**
     * Brander_MegaMenu_Model_Banner constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->_helper = Mage::helper('brander_megamenu/banner');
        $this->_id = $request['id'];
        $this->_width = $request['width'];
        $this->getImageAndRedirectUrl();
    }

    /**
     * @return bool
     */
    public function getJsonBanner()
    {
        if(!$this->_validateRequest()){
            return false;
        }
        return $this->_helper->getBannerHtml($this->_image, $this->_urlRedirect, $this->_id);
    }

    /**
     * @param $imageValue
     * @return mixed
     */
    protected function getImage($imageValue)
    {
        $imageName = $this->_helper->getModel('catalog/category')->load($this->_id)->getData('category_banner_' . $imageValue);
        if(!$imageName)
        {
            return $this->_helper->getMediaUrl($imageValue, true);
        }
        return  $this->_helper->getMediaUrl($imageName);
    }

    /**
     * @return mixed
     */
    protected function getRedirectUrl()
    {
        $urlRedirect = $this->_helper->getModel('catalog/category')->load($this->_id)->getData('url_banner_megamenu');
        if(!$urlRedirect)
        {
            return $this->_helper->getRedirectUrl();
        }
        return $urlRedirect;
    }


    /**
     * Check device and set image and url redirect
     */
    protected function getImageAndRedirectUrl()
    {
        switch (true){
            case $this->_width >= 1366:
                $this->_image = $this->getImage(self::DESCTOP);
                $this->_urlRedirect = $this->getRedirectUrl();
                break;

            case $this->_width >= 1024:
                $this->_image = $this->getImage(self::TABLET);
                $this->_urlRedirect = $this->getRedirectUrl();
                break;

            case $this->_width < 1024:
                $this->_image = $this->getImage(self::MOBILE);
                $this->_urlRedirect = $this->getRedirectUrl();
                break;
        }
    }

    /**
     * @return bool
     */
    protected function _validateRequest()
    {
        if(!$this->_id || !$this->_width)
        {
            return false;
        }
        return true;
    }
}