<?php

/**
 * Class Brander_MegaMenu_ImageController
 */
class Brander_MegaMenu_BannerController extends Mage_Core_Controller_Front_Action
{
    public function loadBannerAction()
    {
        $json = $this->getModelBanner()->getJsonBanner();

        echo $json;
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function getModelBanner()
    {
        return Mage::getModel('brander_megamenu/banner', $this->getRequest()->getParams());
    }
}