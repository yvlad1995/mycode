<?php

/**
 * Class Brander_MegaMenu_BrandsController
 */
class Brander_MegaMenu_BrandsController extends Mage_Core_Controller_Front_Action
{
    public function loadBrandsAction()
    {
        $json = $this->getHelper()->getJsonBrands();

        echo $json;
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function getHelper()
    {
        return Mage::helper('brander_megamenu/brands');
    }
}