<?php
/**
 * Created by PhpStorm.
 * User: roma
 * Date: 31.08.17
 * Time: 13:11
 */

/**
 * Class Brander_CmsHome_Helper_Config
 */
class Brander_MegaMenu_Helper_Config extends Brander_MegaMenu_Helper_Data
{
    const TITLE_BANNER = 'brander_megamenu/config/title_banner';

    const ALT_BANNER = 'brander_megamenu/config/alt_banner';

    const REDIRECT_URL_BANNER = 'brander_megamenu/config/url_banner';

    const IMAGE_DESCTOP_BANNER = 'brander_megamenu/config/image_desctop_banner';

    const IMAGE_TABLET_BANNER = 'brander_megamenu/config/image_tablet_banner';

    const IMAGE_MOBILE_BANNER = 'brander_megamenu/config/image_mobile_banner';

    const TEXT_BANNER = 'brander_megamenu/config/text_banner';

    const BRANDS_ENABLED = 'brander_megamenu/brands/brands_enable';

    const BRANDS_REDIRECT_URL = 'brander_megamenu/brands/brands_redirect_url';

    /**
     * @param $path
     * @return mixed
     */
    public function getConfigValue($path)
    {
        return Mage::getStoreConfig($path, Mage::app()->getStore()->getStoreId());
    }

    /**
     * @return mixed
     */
    public function getTitleBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::TITLE_BANNER);
    }

    /**
     * @return mixed
     */
    public function getAltBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::ALT_BANNER);
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::REDIRECT_URL_BANNER);
    }

    /**
     * @return mixed
     */
    public function getImageDesctopBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::IMAGE_DESCTOP_BANNER);
    }

    /**
     * @return mixed
     */
    public function getImageTabletBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::IMAGE_TABLET_BANNER);
    }

    /**
     * @return mixed
     */
    public function getImageMobileBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::IMAGE_MOBILE_BANNER);
    }

    /**
     * @return mixed
     */
    public function getTextBanner()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::TEXT_BANNER);
    }

    /**
     * @return mixed
     */
    public function getBrandsEnabled()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::BRANDS_ENABLED);
    }

    /**
     * @return mixed
     */
    public function getBrandsRedirectUrl()
    {
        return $this->getConfigValue(Brander_MegaMenu_Helper_Config::BRANDS_REDIRECT_URL);
    }
}