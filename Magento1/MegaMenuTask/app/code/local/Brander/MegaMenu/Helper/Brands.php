<?php

/**
 * Class Brander_MegaMenu_Helper_Brands
 */
class Brander_MegaMenu_Helper_Brands extends Brander_MegaMenu_Helper_Config
{
    /**
     * @param $html
     * @return string
     */
    public function getNavigationBrands($html)
    {
        if($this->getBrandsEnabled()) {
            $html .= "<li id='brands-category' class='nav-item level0 nav-1 level-top extended-width nav-item--parent classic'>";
            $html .= "<a href='" . $this->getBrandsRedirectUrl() . "' class='level-top'>";
            $html .= "<span>" . $this->__('Brands') . "</span>";
            $html .= "<span class='caret'>&nbsp;</span>";
            $html .= "</a>";
            $html .= "<span class='opener'></span>";
            $html .= "<ul class='level0 nav-submenu nav-panel--dropdown nav-panel'>";
            $html .= "<li id=\"brands-subcategory\"  class='nav-item level1 nav-1-1 extended-width last classic'></li></ul>";
            $html .= "</li>";
        }
        return $html;
    }

    /**
     * @return mixed
     */
    public function getJsonBrands()
    {
        $html = "";
        $brands = $this->getBrandsModel()->sortBrands();

        foreach ($brands as $item) {
            foreach ($item as $optionsKey => $options)
            {
                $letter = !empty($options[0]['letter']) ? $options[0]['letter'] : $optionsKey;
                $html .= "<div id=\"brand-" . $letter . "\">";
                $html .= "<dl>";
                $html .= "<dt>" . $letter . "</dt>";
                $html .= "<dd>";
                $html .= "<ul class=\"bare-list\">";
                foreach ($options as $option){
                    $html .= "<li>";
                    $html .= "<a href=\"" . $option['url'] . "\">";
                    if ($option['img']) {
                        $html .= "<img src=\"" . $option['img'] . "\" />";
                    } else {
                        $html .= $this->htmlEscape($option['label']);
                    }

                    if ($this->getBrandsModel()->getBrands()->getShowCounts()) {
                        $html .= "<span class=\"count\">('" . $option['cnt'] . "')</span>'";
                    }

                    $html .= "</a>";
                    $html .= "</li>";
                }

                $html .= "</ul>";
                $html .="</dd>";
                $html .= "</dl>";
                $html .= "</div>";
            }
        }
        return $this->getJsonEncode($html);
    }

    /**
     * @return array
     */
    public function getListRequireOptionField()
    {
        return ['value', 'label', 'cnt'];
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    public function getBrandsModel()
    {
        return Mage::getModel('brander_megamenu/brands');
    }
}