<?php

/**
 * Class Brander_MegaMenu_Helper_Banner
 */
class Brander_MegaMenu_Helper_Banner extends Brander_MegaMenu_Helper_Config
{
    /**
     * @param $image
     * @param $urlRedirect
     * @param $id
     * @return string
     */
    public function getBannerHtml($image,$urlRedirect, $id)
    {
        $html = '<a id="image-banner-' . $id .'" class="image-banner" href="' . $urlRedirect . '">' .
            '<img class="desctop-image" style="display:none" src="' . $image . '" />' .
            '</a>';

        $json = $this->getJsonEncode($html);

        return $json;
    }

    /**
     * @param $imageValue
     * @param bool $defaultImage
     * @return string
     */
    public function getMediaUrl($imageValue, $defaultImage = false)
    {
        if(!$defaultImage)
        {
            return $this->getBaseUrl('media') . 'catalog/category/' . $imageValue;
        }
        $methodImage = "getImage" . ucfirst($imageValue) . "Banner";
        return $this->getBaseUrl('media') . "brander/megamenu/" . $imageValue . "/" . $this->$methodImage();
    }
}