<?php

class Brander_MegaMenu_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $path
     * @return false|Mage_Core_Model_Abstract
     */
    public function getModel($path)
    {
        return Mage::getModel($path);
    }

    /**
     * @param $path
     * @return string
     */
    public function getBaseUrl($path)
    {
        return Mage::getBaseUrl($path);
    }

    /**
     * @param $html
     * @return mixed
     */
    public function getJsonEncode($html)
    {
        return Mage::helper('core')->jsonEncode($html);
    }
}