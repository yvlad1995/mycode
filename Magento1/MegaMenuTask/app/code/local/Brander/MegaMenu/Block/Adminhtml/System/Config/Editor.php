<?php

/**
 * Class Brander_MegaMenu_Block_Adminhtml_System_Config_Editor
 */
class Brander_MegaMenu_Block_Adminhtml_System_Config_Editor
    extends Mage_Adminhtml_Block_System_Config_Form_Field
    implements Varien_Data_Form_Element_Renderer_Interface
{

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element){
        $element->setWysiwyg(true);
        $element->setConfig(Mage::getSingleton('cms/wysiwyg_config')->getConfig());
        return parent::_getElementHtml($element);
    }

    /**
     * @return mixed
     */
    protected function getAfterElementHtml()
    {
        $html = Mage::getSingleton('core/layout')
            ->createBlock(
                'adminhtml/widget_button',
                '',
                array(
                    'label'   => Mage::helper('catalog')->__('WYSIWYG Editor'),
                    'type'=> 'button',
                    'disabled' => false,
                    'class' => (false) ? 'disabled btn-wysiwyg' : 'btn-wysiwyg',
                    'onclick' => 'catalogWysiwygEditor.open(\''.
                        Mage::helper('adminhtml')->getUrl('*/*/wysiwyg').'\', \''.
                        $this->getHtmlId().'\')'
                )
            )
            ->toHtml();
        return $html;
    }
}