<?php

/**
 * @var $this Mage_Core_Model_Resource_Setup
 */

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$entityTypeId     = $setup->getEntityTypeId('catalog_category');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = 4;

$setup->addAttribute('catalog_category', 'category_banner_desctop', array(
    'type'          => 'varchar',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'group'         => 'General Information',
    'label'         => 'Category Banner Desctop Megamenu',
    'visible'       => 1,
    'required'      => 0,
    'user_defined'  => 1,
    'frontend_input' =>'',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible_on_front'  => 1,
));

$setup->addAttribute('catalog_category', 'category_banner_tablet', array(
    'type'          => 'varchar',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'group'         => 'General Information',
    'label'         => 'Category Banner Tablet Megamenu',
    'visible'       => 1,
    'required'      => 0,
    'user_defined'  => 1,
    'frontend_input' =>'',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible_on_front'  => 1,
));

$setup->addAttribute('catalog_category', 'category_banner_mobile', array(
    'type'          => 'varchar',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'group'         => 'General Information',
    'label'         => 'Category Banner Mobile Megamenu',
    'visible'       => 1,
    'required'      => 0,
    'user_defined'  => 1,
    'frontend_input' =>'',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible_on_front'  => 1,
));

$setup->addAttribute('catalog_category', 'url_banner_megamenu', array(
    'type'          => 'varchar',
    'input'         => 'text',
    'backend'       => 'catalog/category_attribute_backend_image',
    'group'         => 'General Information',
    'label'         => 'Category Banner Redirect URL For Megamenu',
    'visible'       => 1,
    'required'      => 0,
    'user_defined'  => 1,
    'frontend_input' =>'',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible_on_front'  => 1,
));


$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'category_banner_desctop',
    '15'  //sort_order
);

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'category_banner_tablet',
    '16'  //sort_order
);

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'category_banner_mobile',
    '17'  //sort_order
);

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'url_banner_megamenu',
    '18'  //sort_order
);

$installer->endSetup();


