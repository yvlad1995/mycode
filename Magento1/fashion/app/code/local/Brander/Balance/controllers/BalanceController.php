<?php


require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';
/**
 * Class Brander_Balance_IndexController
 */
class Brander_Balance_BalanceController extends Mage_Customer_AccountController
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}