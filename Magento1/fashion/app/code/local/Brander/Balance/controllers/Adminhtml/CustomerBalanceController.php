<?php

/**
 * Class Brander_Balance_Adminhtml_IndexController
 */
class Brander_Balance_Adminhtml_CustomerBalanceController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_forward('grid');
    }

    public function gridAction()
    {
        $this->loadLayout()->_setActiveMenu('brander_base');
        $this->renderLayout();
    }


    public function updateTitleAction()
    {
        $customerId = (int) $this->getRequest()->getParam('id');
        $balance = $this->getRequest()->getParam('title');
        if ($customerId) {
            $model = Mage::getModel('brander_balance/balance')->load($customerId, 'customer_id');
            $model->setCustomerId($customerId);
            $model->setBalance($balance);
            $model->save();
        }
    }

    public function editAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}