<?php

class Brander_Balance_Model_Balance extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_balance/balance');
    }
}