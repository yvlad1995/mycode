<?php

class Brander_Balance_Model_Resource_Balance_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_balance/balance');
    }
}