<?php

class Brander_Balance_Model_Resource_Balance extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_balance/balance', 'entity_id');
    }
}