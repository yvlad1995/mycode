<?php

class Brander_Balance_Model_Resource_Balance_Data extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_balance/balance_data', 'entity_id');
    }
}