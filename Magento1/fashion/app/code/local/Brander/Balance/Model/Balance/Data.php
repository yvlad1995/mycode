<?php

class  Brander_Balance_Model_Balance_Data extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brander_balance/balance_data');
    }

    public function getBalanceValue()
    {
        $customerId = $this->_getHelper()->getCustomerId();
        if($customerId) {
            return $this->load($customerId, 'customer_id');
        }

        return new Varien_Object();
    }

    protected function _getHelper()
    {
        return Mage::helper('brander_balance/data');
    }
}