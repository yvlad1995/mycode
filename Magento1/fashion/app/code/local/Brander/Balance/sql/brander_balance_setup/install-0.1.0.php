<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_balance/balance'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Entity Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ), 'Customer Id')
    ->addColumn('balance', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Balance')
    ->addForeignKey($installer->getFkName('brander_balance/balance', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id', $installer->getTable('customer/entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Customer Balance')
    ->addIndex(
        $installer->getIdxName('brander_balance/balance', array('customer_id'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        'customer_id',
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_balance/balance_data'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Entity Id')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATE, null, array(
        'nullable'  => false,
    ), 'Date created at')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'unique'    => true,
        'primary'   => true,
    ), 'Customer Id')
    ->addColumn('incoming', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Incoming')
    ->addColumn('balance', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Data Balance')
    ->addColumn('purpose_payment', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Purpose of payment')
    ->addColumn('ttn', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'TTN')
    ->addForeignKey($installer->getFkName('brander_balance/balance_data', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id', $installer->getTable('customer/entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Customer Balance Data');
$installer->getConnection()->createTable($table);