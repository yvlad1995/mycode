<?php

/**
 * Class Brander_OurStores_Block_Adminhtml_Stores_Edit
 */
class Brander_Balance_Block_Adminhtml_Balance_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'brander_balance';
        $this->_controller = 'adminhtml_balance';
        $this->removeButton('back');
        $this->removeButton('reset');
        $this->removeButton('save');

        $this->_addButton('back', array(
            'label'     => Mage::helper('brander_balance')->__('Back'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('brander_balance')->__('Reset'),
            'onclick'   => 'setLocation(window.location.href)',
        ), -1);

        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('brander_balance')->__('Delete Store')
        );

        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('brander_balance')->__('Save Store And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                jQuery('#reviewGrid_massaction-select').removeClass('required-entry');
                jQuery('select#visibility').removeClass('required-entry');
                editForm.submit($('edit_form').action);
            }
        ";

    }

    /**
     * get the edit form header
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
//        if (Mage::registry('current_store') && Mage::registry('current_store')->getId()) {
//            return Mage::helper('brander_ourstores')->__(
//                "Edit Store"
//            );
//        } else {
//            return Mage::helper('brander_ourstores')->__('New Store');
//        }
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/index');
    }
}
