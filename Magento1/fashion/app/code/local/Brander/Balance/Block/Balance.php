<?php

class Brander_Balance_Block_Balance extends Mage_Core_Block_Template
{
    public function __construct(array $args = array())
    {
        parent::__construct($args);
        $this->setTemplate('brander/balance/balance.phtml');
    }

    public function getBalance()
    {
        return $this->getBalanceHelper()->getBalance();
    }

    public function getBalanceHelper()
    {
        return Mage::helper('brander_balance/balanceData');
    }

    public function getModel()
    {
        return Mage::getModel('brander_balance/balance');
    }
}