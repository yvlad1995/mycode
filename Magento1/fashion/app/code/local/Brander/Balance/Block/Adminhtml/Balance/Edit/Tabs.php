<?php

class Brander_Balance_Block_Adminhtml_Balance_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('brander_balance_grid');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('brander_balance')->__('Customer Balance Information'));
    }

    protected function _prepareLayout()
    {
        $this->addTab('balance', array(
            'label'     => Mage::helper('brander_balance')->__('Balance'),
            'content'   => $this->_translateHtml($this->getLayout()
                ->createBlock('brander_balance/adminhtml_balance_edit_tab_balance')->toHtml()),
        ));

        $this->addTab('import', array(
            'label'     => Mage::helper('brander_balance')->__('Import'),
            'content'   => $this->_translateHtml($this->getLayout()
                ->createBlock('brander_balance/adminhtml_balance_edit_tab_import')->toHtml()),
        ));

        $this->addTab('export', array(
            'label'     => Mage::helper('brander_balance')->__('Export'),
            'content'   => $this->_translateHtml($this->getLayout()
                ->createBlock('brander_balance/adminhtml_balance_edit_tab_export')->toHtml()),
        ));

        return parent::_prepareLayout();
    }

    /**
     * @param $html
     * @return mixed
     */
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }
}