<?php

class Brander_Balance_Block_Balance_Data extends Mage_Core_Block_Template
{
    protected $_dataCollection = [];

    /**
     * Brander_Balance_Block_Balance_Data constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        parent::__construct($args);
        $this->setTemplate('brander/balance/balance/data.phtml');
    }

    /**
     * @return string
     */
    public function getBalanceValuesJson()
    {
        $values = $this->_getDataBalanceCollection();

        $data = [];
        if (count($values)) {
            foreach ($values as $value) {
                $value = $this->_getHelper()->getValidateData($value);
                $data[] = $value->getData();
            }
        }

        if(count($data)) {
            return $this->_getDataJson($data);
        }

        return $this->_getHelper()->getNameRawTable()->toJson();
    }

    /**
     * @return array
     */
    protected function _getDataBalanceCollection()
    {
        if (!count($this->_dataCollection)) {
            $customerId = $this->_getHelper()->getCustomerId();
            return Mage::getResourceModel('brander_balance/balance_data_collection')
                ->addFieldToFilter('customer_id', $customerId)->getItems();
        }

        return $this->_dataCollection;
    }

    /**
     * @return int
     */
    public function getBalanceValuesSize()
    {
        return count($this->_getDataBalanceCollection());
    }

    /**
     * @return string
     */
    public function getBalanceLabelsJson()
    {
        $labels = $this->_getHelper()->getLabelBalanceTable();
        if ($labels instanceof Varien_Object) {
            return $labels->toJson();
        }

        return $this->_getHelper()->getNameRawTable()->toJson();
    }

    /**
     *
     */
    public function getBalanceData()
    {
        $customerId = $this->getHelper()->getCustomer()->getId();
        $balanceData = $this->_getModel()->load($customerId, 'customer_id');
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('brander_balance/balanceData');
    }

    /**
     * @return false|Mage_Core_Model_Abstract
     */
    protected function _getModel()
    {
        return Mage::getModel('brander_balance/balance_data');
    }

    /**
     * @param bool $data
     * @return string
     */
    protected function _getDataJson($data = false)
    {
        $dataJson = new Varien_Object();
        $dataJson->setData($data);
        return $dataJson->toJson();
    }
}