<?php

/**
 * Class Brander_Balance_Helper_Data
 */
class Brander_Balance_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PAYMENT_LINK = 'brander_balance/general/payment_link';

    public function getCustomerName()
    {
        $customer = $this->getCustomer();

        return $customer->getFirstname() . ' ' . $customer->getLastname();
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getButtonPaymentLink()
    {
        return $this->getConfigValue(self::PAYMENT_LINK);
    }

    public function getConfigValue($path, $storeId = null)
    {
        return Mage::getStoreConfig($path, $storeId);
    }

    public function getBalance($value = false)
    {
        //TODO Вынести значения по методам
        if (!$value) {
            $customerId = $this->getCustomerId();
            $value = Mage::getModel('brander_balance/balance')
                ->load($customerId, 'customer_id')->getBalance();
        }
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $currencySymbol = Mage::app()->getLocale()->currency($currencyCode)->getSymbol();

        return Mage::helper('core')->currency($value, false, false) . ' ' . $currencySymbol;
    }

    public function getCustomerId()
    {
        $customer = $this->getCustomer();
        return !empty($customer->getId()) ? $customer->getId() : null;
    }

}