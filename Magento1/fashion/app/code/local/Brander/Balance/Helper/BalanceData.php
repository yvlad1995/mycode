<?php

/**
 * Class Brander_Balance_Helper_BalanceData
 */
class Brander_Balance_Helper_BalanceData extends Brander_Balance_Helper_Data
{
    /**
     * @return Varien_Object
     */
    public function getLabelBalanceTable()
    {
        $labelsData = new Varien_Object();
        $labelsData->setCreatedAt($this->__('Created At'))->setTtn($this->__('TTN'))
            ->setIncoming($this->__('Incoming'))->setBalance($this->__('Balance'))
            ->setPurposePayment($this->__('Purpose Payment'));

        return $labelsData;
    }

    /**
     * @return Varien_Object
     */
    public function getNameRawTable()
    {
        $labelsData = new Varien_Object();
        $labelsData->setCreatedAt()->setTtn()->setIncoming()->setBalance()->setPurposePayment();
        return $labelsData;
    }

    /**
     * @param Varien_Object $value
     * @return mixed
     */
    public function getValidateData(Varien_Object $value) 
    {
        return $value->setIncoming($this->getBalance($value->getIncoming()))
            ->setPurposePayment($this->getBalance($value->getPurposePayment()))
            ->setBalance($this->getBalance($value->getBalance()));
    }
}