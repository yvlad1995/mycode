<?php

class Brander_PriceRules_Model_Resource_Message extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('brander_pricerules/pricerule_message', 'entity_id');
    }

    public function toOptionArray($isMultiselect=false)
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('brander_pricerules/priceType_collection')->loadData()->toOptionArray(false);
        }

        $options = $this->_options;
        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
        }

        return $options;
    }
}