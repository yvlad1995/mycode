<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/** @var $installer Mage_Paypal_Model_Resource_Setup */
$installer = $this;

/**
 * Prepare database for install
 */
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_pricerules/rule'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Entity Id')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        ), 'Label')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 64, array(
        ), 'Code')
    ->addIndex(
        $installer->getIdxName('brander_pricerules/rule',
            array('code'),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        'code',
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE)
    );
$installer->getConnection()->createTable($table);

/**
 * Create table 'paypal/settlement_report_row'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('brander_pricerules/pricerule_message'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Entity Id')
    ->addColumn('pricerule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Price Rule Id')
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        ), 'Message')
    ->addColumn('store_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 19, array(
    ), 'Store Code')
    ->addForeignKey($installer->getFkName('brander_pricerules/pricerule_message', 'pricerule_id', 'brander_pricerules/rule', 'entity_id'),
        'pricerule_id', $installer->getTable('brander_pricerules/rule'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('brander_pricerules/pricerule_message', 'store_code', 'core/store', 'code'),
        'store_code', $installer->getTable('core/store'), 'code',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->getConnection()->createTable($table);


$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$priceLarge = array(
    'type'                       => 'decimal',
    'label'                      => 'Price for large purchases',
    'input'                      => 'price',
    'backend'                    => 'catalog/product_attribute_backend_price',
    'sort_order'                 => 2,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'                 => true,
    'filterable'                 => true,
    'visible_in_advanced_search' => true,
    'used_in_product_listing'    => true,
    'used_for_sort_by'           => true,
    'apply_to'                   => 'simple,configurable,virtual',
    'group'                      => 'Prices',
);
$setup->addAttribute('catalog_product','price_large', $priceLarge);

$installer->endSetup();

