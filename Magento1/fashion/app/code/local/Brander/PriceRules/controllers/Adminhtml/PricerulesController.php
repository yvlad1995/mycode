<?php

/**
 * Class Brander_PriceRules_Adminhtml_PricerulesController
 */
class Brander_PriceRules_Adminhtml_PricerulesController extends Mage_Adminhtml_Controller_Action
{
    public function rulesGridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editRulesAction()
    {
        $this->_initRule();
        $this->_initMessage();
        if (!$this->getRequest()->getParam('store')) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('brander_pricerules')->__('Please select store!')
            );
        }
        $this->loadLayout()->_setActiveMenu('brander_base');
        $this->renderLayout();
    }

    protected function _initRule()
    {
        $ruleId = $this->getRequest()->getParam('id');
        $rule = Mage::getModel('brander_pricerules/rule')->load($ruleId);
        if (!empty($rule->getData())) {
            Mage::register('current_rule', $rule);
        }

        return $rule;
    }

    protected function _initMessage()
    {
        $storeCode = $this->getRequest()->getParam('store');
        $ruleId = $this->getRequest()->getParam('id');
        $message = Mage::getResourceModel('brander_pricerules/message_collection')
            ->addFieldToFilter('store_code', $storeCode)->addFieldToFilter('pricerule_id', $ruleId)->getLastItem();
        if ($message->getId()) {
            Mage::register('current_message', $message);
        }

        return $message;
    }

    public function saveRuleAction()
    {
        $data = $this->getRequest()->getParams();
        unset($data['code']);
        try {
            if(empty($data['label']) || empty($data['store'])) {
                throw new Exception(Mage::helper('brander_pricerules')->__('Please fill all fields and select store!'));
            }

            $rule = $this->_initRule();
            $message = $this->_initMessage();
            $rule->addData($this->_getRuleData($data))->save();
            $message->addData($this->_getMessageData($data))->save();
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect('*/*/editRules');
        }
        $this->_getSession()->addSuccess(Mage::helper('brander_pricerules')->__('Data Save'));
        return $this->_redirect('*/*/editRules');
    }

    protected function _redirect($path, $arguments = array())
    {
        $data = array(
            'id' => $this->getRequest()->getParam('id'),
            'store' => $this->getRequest()->getParam('store')
        );

        $arguments = array_merge($data, $arguments);
        return parent::_redirect($path, $arguments);
    }

    protected function _getMessageData($data)
    {
        $dataObject = new Varien_Object();
        $dataObject->setMessage($data['message'])
            ->setStoreCode($data['store'])
            ->setPriceruleId($data['id']);
        Mage::dispatchEvent('brander_pricerules_message_data_before_save', array('messageData' => $dataObject));
        return $dataObject->getData();
    }

    protected function _getRuleData($data)
    {
        $dataObject = new Varien_Object();
        $dataObject->setLabel($data['label']);
        Mage::dispatchEvent('brander_pricerules_rule_data_before_save', array('ruleData' => $dataObject));
        return $dataObject->getData();
    }
}