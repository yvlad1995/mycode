<?php

class Brander_PriceRules_Block_Adminhtml_Rules_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'brander_pricerules';
        $this->_controller = 'adminhtml_rules';
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/rulesGrid');
    }

    public function getHeaderText()
    {
        if (Mage::registry('current_rule') && Mage::registry('current_rule')->getId()) {
            return Mage::helper('brander_pricerules')->__(
                "Edit Rule"
            );
        } else {
            return Mage::helper('brander_pricerules')->__('New Rule');
        }
    }
}