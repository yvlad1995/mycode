<?php

class Brander_PriceRules_Block_Adminhtml_Config_Form_Field_Prices extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $attributes = Mage::getResourceModel('catalog/product_attribute_collection');
        $attributes->setCodeFilter(['price', 'special_price', 'price_large'])
            ->addVisibleFilter()->load();
        $this->addOption('', $this->__('-- Please Select --'));
        foreach ($attributes as $attribute) {
            $this->addOption($attribute->getAttributeCode(), $attribute->getFrontendLabel());
        }

        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}