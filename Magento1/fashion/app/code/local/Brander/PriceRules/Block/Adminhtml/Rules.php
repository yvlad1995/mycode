<?php


class Brander_PriceRules_Block_Adminhtml_Rules extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'brander_pricerules';
        $this->_controller = 'adminhtml_rules';
        $this->_headerText = Mage::helper('brander_pricerules')->__('Manage Rules');
        parent::__construct();
        $this->_removeButton('add');
    }

}