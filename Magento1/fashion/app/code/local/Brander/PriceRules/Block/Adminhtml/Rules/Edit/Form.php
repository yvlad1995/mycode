<?php

class Brander_PriceRules_Block_Adminhtml_Rules_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _getStoreCode()
    {
        return $this->getRequest()->getParam('store');
    }

    protected function _getEntityId()
    {
        return $this->getRequest()->getParam('id');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'         => 'edit_form',
                'action'     => $this->getUrl(
                    '*/*/saveRule',
                    array(
                        'id'       => $this->getRequest()->getParam('id'),
                        'store' => $this->_getStoreCode()
                    )
                ),
                'method'     => 'post',
                'enctype'    => 'multipart/form-data'
            )
        );

        $fieldset = $form->addFieldset('rule',
            array('legend' => Mage::helper('brander_pricerules')->__('Price Rule'))
        );


        $fieldset->addField('label', 'text', array(
            'label'=> Mage::helper('brander_pricerules')->__('Label'),
            'title'=> Mage::helper('brander_pricerules')->__('Label'),
            'name' => 'label',
            'required' => true
        ));

        $fieldset->addField('code', 'text', array(
            'label'=> Mage::helper('brander_pricerules')->__('Edentity Code'),
            'title'=> Mage::helper('brander_pricerules')->__('Edentity Code'),
            'name' => 'code',
            'disabled' => true
        ));

        $fieldset->addField('message', 'textarea', array(
            'label'=> Mage::helper('brander_pricerules')->__('Message'),
            'title'=> Mage::helper('brander_pricerules')->__('Message'),
            'name' => 'message',
        ));

        Mage::dispatchEvent('brader_pricerules_form_field_add', array('form' => $form));

        if ($this->getRequest()->getParam('store')) {
            $form->setValues($this->_getRule()->getData());
            $form->addValues($this->_getMessage()->getData());
            $form->setUseContainer(true);
            $this->setForm($form);
        }

    }

    protected function _getRule()
    {
        if(!empty(Mage::registry('current_rule'))) {
            return Mage::registry('current_rule');
        }

        return Mage::getModel('brander_pricerules/rule')
            ->load($this->_getEntityId());
    }

    protected function _getMessage()
    {
        if(!empty(Mage::registry('current_message'))) {
            return Mage::registry('current_message');
        }

        return Mage::getModel('brander_pricerules/message')
            ->setStoreCode($this->_getStoreCode())
            ->load($this->_getEntityId(), 'pricerule_id');
    }
}