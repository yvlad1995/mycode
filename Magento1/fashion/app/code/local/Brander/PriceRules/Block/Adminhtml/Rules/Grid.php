<?php

class Brander_PriceRules_Block_Adminhtml_Rules_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('brander_pricerules/rule_collection');

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn('label',
            array(
                'header'=> Mage::helper('brander_pricerules')->__('Label Rule'),
                'index' => 'label',
            )
        );

        $this->addColumn('code',
            array(
                'header'=> Mage::helper('brander_pricerules')->__('Code'),
                'index' => 'code',
            ));

        Mage::dispatchEvent('brander_rules_grid', array('grid' => $this));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editRules', array(
                'store'=>$this->getRequest()->getParam('store'),
                'id'=>$row->getId())
        );
    }
}