function DataBalance(data) {
    var self = this;
    this.created_at  = ko.observable(data.created_at);
    this.ttn        = ko.observable(data.ttn);
    this.incoming   = ko.observable(data.incoming);
    this.balance    = ko.observable(data.balance);
    this.purpose_payment = ko.observable(data.purpose_payment);
};

function CustomerBalance(labels, values, size_data) {
    this.dataLabelArray = ko.observableArray([new DataBalance(labels)]);
    this.dataArray = ko.observableArray(values);
    this.display = ko.computed(function() {
        if(size_data > 0) {
            return true;
        }
        return false;
    }, this);
};