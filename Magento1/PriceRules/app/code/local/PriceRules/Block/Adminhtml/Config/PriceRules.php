<?php

class Brander_PriceRules_Block_Adminhtml_Config_PriceRules extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $_rules;

    protected $_prices;

    public function _prepareToRender()
    {
        $this->addColumn('from_qty', array(
            'label' => Mage::helper('brander_pricerules')->__('From Quantity'),
            'style' => 'width:50px',
        ));
        $this->addColumn('to_qty', array(
            'label' => Mage::helper('brander_pricerules')->__('To Quantity'),
            'style' => 'width:50px',
        ));

        $this->addColumn('rules', array(
            'label' => Mage::helper('brander_pricerules')->__('Rule'),
            'style' => 'width:50px',
            'renderer' => $this->_getRendererRules()
        ));

        $this->addColumn('prices', array(
            'label' => Mage::helper('brander_pricerules')->__('Price'),
            'style' => 'width:50px',
            'renderer' => $this->_getRendererPrices()
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('brander_pricerules')->__('Add');
    }

    protected function  _getRendererRules()
    {
        if(!$this->_rules) {
            $this->_rules = $this->getLayout()->createBlock(
                'brander_pricerules/adminhtml_config_form_field_rules', '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->_rules;
    }

    protected function  _getRendererPrices()
    {
        if (!$this->_prices) {
            $this->_prices = $this->getLayout()->createBlock(
                'brander_pricerules/adminhtml_config_form_field_prices', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_prices;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
//        $row->setData(
//            'option_extra_attr_' . $this->_getRenderer()
//                ->calcOptionHash($row->getData('country_id')),
//            'selected="selected"'
//        );
    }
}