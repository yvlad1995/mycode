<?php

class Brander_PriceRules_Block_Adminhtml_Config_Form_Field_Rules extends Mage_Core_Block_Html_Select
{
    public function _toHtml()
    {
        $options = Mage::getSingleton('brander_pricerules/rule')
            ->toOptionArray('code', 'label');
        foreach ($options as $option) {
            $this->addOption($option['value'], $option['label']);
        }

        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}