<?php

/**
 * Class Brander_PriceRules_Model_Resource_Price_Collection
 */
class Brander_PriceRules_Model_Resource_PriceType_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('brander_pricerules/priceType');
    }

    public function toOptionArray($valueField='id', $labelField='name')
    {
        return $this->_toOptionArray($valueField, $labelField);
    }
}