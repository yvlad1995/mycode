<?php

class Brander_PriceRules_Model_PriceType extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('brander_pricerules/priceType');
    }

    public function toOptionArray($valueField='id', $labelField='name', $isMultiselect = false)
    {
        if (!$this->_options) {
            $this->_options = Mage::getResourceModel('brander_pricerules/priceType_collection')->loadData()->toOptionArray($valueField, $labelField);
        }

        $options = $this->_options;
        if(!$isMultiselect){
            array_unshift($options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
        }

        return $options;
    }
}