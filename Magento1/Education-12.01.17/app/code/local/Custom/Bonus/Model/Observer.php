<?php

/**
 * Class Custom_Bonus_Model_Observer
 */
class Custom_Bonus_Model_Observer
{

    /**
     * @param $observer
     */
   public function editCreditmemo($observer)
   {

       $orderId = $observer->getData('request')->getParams();
       $orderObject = $this->getOrder($orderId['order_id']);

       $paymentMethod = $orderObject->getPayment()->getMethodInstance()->getCode();

       if($paymentMethod !== 'custombonus'){
           return false;
       }

       $invoiceIds = $orderObject->getInvoiceCollection()->getItems();

       foreach($invoiceIds as $item) {
           $invoice = $item;
       }

       $creditmemo = $observer->getCreditmemo();
       $creditmemo->setInvoice($invoice);

       return $creditmemo;
   }

    /**
     * @param $orderId
     * @return Mage_Core_Model_Abstract
     */
    protected function getOrder($orderId)
    {
        return Mage::getModel('sales/order')->load($orderId);
    }
}


        

