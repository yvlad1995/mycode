<?php

class Custom_Bonus_Model_Bonus
{
    protected $customer_id;

    protected function saveBonus($bonus_invoice = false)
    {
        if($bonus_invoice === false)
        {
            return false;
        }
        $bonus = (float) $this->getBonus($this->customer_id, $bonus_invoice);

        Mage::getSingleton('customer/customer')->load($this->customer_id)->setData('bonus_invoice', $bonus)->save();

        return true;
    }

    public function editBonus($customer_id, $amount, $refund = false)
    {
        $this->customer_id = $customer_id;

        $bonus = $this->getBonus($this->customer_id);
        $bonus_invoice = false;
        if($bonus >= $amount && $refund == false){
            $bonus_invoice =  $bonus - $amount;
        }elseif($refund == true){
            $bonus_invoice = $bonus + $amount;
        }

        return $this->saveBonus($bonus_invoice);
    }

    public function getBonus($customer_id, $bonus = false)
    {
        $this->customer_id = $customer_id;
        return $this->inspectBonus($bonus);
    }

    protected function inspectBonus($bonus)
    {
        $exchange = (float) Mage::getStoreConfig('custombonus/custombonus_group/custombonus_text');

        if($bonus !== false){
            return $bonus / $exchange;
        }

        $invoice = (float) Mage::getModel('customer/customer')->load($this->customer_id)->getData('bonus_invoice');

        if(empty($exchange))
        {
            return (float) $invoice;
        }
        return $invoice * $exchange;
    }
}


        

