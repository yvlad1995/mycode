<?php

/**
 * Class Web_Qorder_Model_Shipping_Quickshipping
 */
class Web_Qorder_Model_Shipping_Quickshipping extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * @var string
     */
    protected $_code = 'quickshipping';

    /**
     *
     */
    public function getAllowedMethods()
    {
    }

    /**
     *
     */
    public function isTrackingAvailable()
    {
    }

    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return false|Mage_Core_Model_Abstract
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $result = Mage::getModel('shipping/rate_result');

        $this->_updateFreeMethodQuote($request);

        $method = Mage::getModel('shipping/rate_result_method');

        $method->setCarrier('quickshipping');
        $method->setCarrierTitle('Quick Order Shipping');

        $method->setMethod('quickshipping');
        $method->setMethodTitle('Quick Order Shipping');

        $method->setPrice('0.00');
        $method->setCost('0.00');

        $result->append($method);

        return $result;
    }
    
}