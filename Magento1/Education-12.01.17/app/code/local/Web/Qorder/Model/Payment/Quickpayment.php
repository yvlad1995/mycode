<?php

/**
 * Class Web_Qorder_Model_Payment_Quickpayment
 */
class Web_Qorder_Model_Payment_Quickpayment extends Mage_Payment_Model_Method_Abstract
{
    /**
     * @var string
     */
    protected $_code = 'quickpayment';
}