<?php

/**
 * Class Web_Qorder_Model_Customer_Address
 */
class Web_Qorder_Model_Customer_Address
{

    /**
     * @param Varien_Event_Observer $observer
     */
    public function validate(Varien_Event_Observer $observer)
    {
        $address = $observer->getEvent()->getAddress();

        if(empty($address->getQuickOrder()))
        {
            return;
        }

        $address->getQuote()->getBillingAddress()->setShouldIgnoreValidation(true);
        $address->getQuote()->getShippingAddress()->setShouldIgnoreValidation(true);

        return $address;
    }
}
