<?php

/**
 * Class Web_Qorder_Model_System_Amount
 */
class Web_Qorder_Model_System_Amount// extends Mage_Core_Model_Abstract
{
    /**
     * @return int
     */
    public function getMinimumAmount()
    {
        return $this->getAmount();
    }

    /**
     * @return int
     */
    protected function getAmount()
    {
        $minAmount = Mage::getStoreConfig('customqorder/customqorder_group/customqorder_amount');
        return (int)$minAmount;
    }
}