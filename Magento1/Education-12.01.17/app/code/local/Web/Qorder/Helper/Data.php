<?php

class Web_Qorder_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param $order
     * @return array
     */
    public function getShippingData($order)
    {
        return array(
            'firstname'  => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getFirstname() : $order->getFirstname(),
            'lastname'   => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getLastname() : $order->getLastname(),
            'telephone'  => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getTelephone() : $order->getTelephone(),
            'street'     => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getStreet() : false,
            'city'       => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getCity() : false,
            'country_id' => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getCountryId(): true,
            'postcode'   => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getPostcode() : false,
            'region'     => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getRegion() : false,
            'region_id'  => !empty($order->getPrimaryShippingAddress()) ? $order->getPrimaryShippingAddress()->getRegionId() : false,
            'quick_order'=> true
        );
    }

    /**
     * @param $order
     * @return array
     */
    public function getBillingData($order)
    {
        return array(
            'firstname'  => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getFirstname() : $order->getFirstname(),
            'lastname'   => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getLastname() : $order->getLastname(),
            'telephone'  => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getTelephone() : $order->getTelephone(),
            'street'     => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getStreet() : false,
            'city'       => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getCity() : false,
            'country_id' => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getCountryId() : true,
            'postcode'   => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getPostcode() : false,
            'region'     => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getRegion() : false,
            'region_id'  => !empty($order->getPrimaryBillingAddress()) ? $order->getPrimaryBillingAddress()->getRegionId() : false,
            'quick_order'=> true
        );
    }
}