<?php

/**
 * Class Web_Qorder_Block_Checkout_Shipping
 */
class Web_Qorder_Block_Checkout_Shipping extends Mage_Checkout_Block_Cart_Shipping
{
    /**
     * @return array
     */
    public function getEstimateRates()
    {
        $this->_rates = parent::getEstimateRates();
        if (array_key_exists('quickshipping', $this->_rates)) {
            unset($this->_rates['quickshipping']);
        }
        return $this->_rates;
    }
}