<?php

/**
 * Class Web_Qorder_Block_Quickorder
 */
class Web_Qorder_Block_Quickorder extends Mage_Checkout_Block_Onepage_Abstract
{
    /**
     * @return string
     */
    public function getFormAction()
    {
        return Mage::getUrl('quickorder/cart/post', array('_secure' => $this->getRequest()->isSecure()));
    }

    /**
     * @return mixed
     */
    public function getMinAmount()
    {
        return Mage::getModel('webqorder/system_amount')->getMinimumAmount();
    }
    

}