<?php

class Web_Promotions_Model_Promotions_Category extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        $this->_init('webpromotions/promotions_category');
    }

    public function savePromotionRelation($promotion)
    {
        $data = $promotion->getCategoriesData();
        if (!is_null($data)) {
            $this->_getResource()->savePromotionRelation($promotion, $data);
        }
        return $this;
    }

    public function getCategoryCollection($promotion)
    {
        $collection = Mage::getResourceModel('webpromotions/promotions_category_collection')
            ->addPromotionFilter($promotion);
            return $collection;
    }
}