<?php

class Web_Promotions_Model_Resource_Promotions_Product
    extends Mage_Core_Model_Resource_Db_Abstract {
    
    protected function  _construct(){
        $this->_init('webpromotions/promotions_product', 'rel_id');
    }
    
    public function savePromotionsRelation($promotions, $data){
        if (!is_array($data)) {
            $data = array();
        }
        $deleteCondition = $this->_getWriteAdapter()->quoteInto('promotion_id=?', $promotions->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $productId => $info) {
            $this->_getWriteAdapter()->insert($this->getMainTable(), array(
                'promotion_id'      => $promotions->getId(),
                'product_id'     => $productId,
                'position'      => @$info['position']
            ));
        }
        return $this;
    }
}