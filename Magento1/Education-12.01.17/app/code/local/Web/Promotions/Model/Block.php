<?php

class Web_Promotions_Model_Block
{
    protected $_options = null;
    protected $_type;

    protected function getOptions()
    {
        if ($this->_options === null) {
            $this->_options = array();
            
            $this->_options = $this->getData($this->getDataArray());
            
        }
        return $this->_options;
    }

    public function toOptionArray()
    {
        $options = array();

        foreach ($this->getOptions() as $value => $label) {
            $options[] = array(
                'label'  => $label,
                'value'  => (int) $value,
            );
        }

        return $options;
    }

    public function getDataArray()
    {
        $blocks = Mage::getSingleton('cms/block')->getCollection()->getData();

        $dataArray[] = array(
            'name' => 'None',
            'id'   => '0'
        );

        foreach($blocks as $item){
            if($item['is_active'] == '1'){
                $dataArray[] = array(
                    'name' => $item['title'],
                    'id'   => $item['block_id']
                );
            }
        }

        return $dataArray;
    }

    protected function getData($dataArray)
    {
        foreach ($dataArray as $layout) {
            $this->_options[$layout['id']] = $layout['name'];
        }

        return $this->_options;
    }

}