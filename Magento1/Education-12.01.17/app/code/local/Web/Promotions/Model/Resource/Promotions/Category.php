<?php

class Web_Promotions_Model_Resource_Promotions_Category
    extends Mage_Core_Model_Resource_Db_Abstract {

    protected function  _construct()
    {
        $this->_init('webpromotions/promotions_category', 'rel_id');
    }

    public function savePromotionRelation($promotion, $data){
        if (!is_array($data)) {
            $data = array();
        }

        $deleteCondition = $this->_getWriteAdapter()->quoteInto('promotion_id=?', $promotion->getId());
        $this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

        foreach ($data as $categoryId) {
            if (!empty($categoryId)){
                $this->_getWriteAdapter()->insert($this->getMainTable(), array(
                    'promotion_id'  => $promotion->getId(),
                    'category_id'   => $categoryId,
                    'position'      => 1
                ));
            }
        }
        return $this;
    }
}