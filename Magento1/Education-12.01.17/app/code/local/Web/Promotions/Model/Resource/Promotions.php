<?php

class Web_Promotions_Model_Resource_Promotions extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('webpromotions/promotions', 'id');
    }

    public function getProductsPosition($promotion)
    {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->getTable('webpromotions/promotions_product'), array('product_id', 'position'))
            ->where('promotion_id = :promotion_id');
        $bind = array('promotion_id' => (int)$promotion->getId());

        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }

    public function getCategoryIds($promotion)
    {
        $adapter = $this->_getReadAdapter();

        $select = $adapter->select()
            ->from($this->getTable('webpromotions/promotions_category'), 'category_id')
            ->where('promotion_id = ?', (int)$promotion->getId());

        return $adapter->fetchCol($select);
    }
}