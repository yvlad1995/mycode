<?php

class Web_Promotions_Model_Resource_Promotions_Product_Collection
    extends Mage_Catalog_Model_Resource_Product_Collection {

    protected $_joinedFields = false;

    protected $_productLimitationFilters;

    public function joinFields(){
        if (!$this->_joinedFields){
            $this->getSelect()->join(
                array('related' => $this->getTable('webpromotions/promotions_product')),
                'related.product_id = e.entity_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }
    public function addPromotionsFilter($promotion){
        if ($promotion instanceof Web_Promotions_Model_Promotions){
            $promotion = $promotion->getId();
        }
        if (!$this->_joinedFields){
            $this->joinFields();
        }
        $this->getSelect()->where('promotion_id = ?', $promotion);
        return $this;
    }

}