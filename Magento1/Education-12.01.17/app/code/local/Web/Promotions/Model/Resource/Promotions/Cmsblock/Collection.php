<?php

class Web_Promotions_Model_Resource_Promotions_Cmsblock_Collection
    extends Mage_Core_Model_Resource_Db_Collection_Abstract{


    protected $_joinedFields = false;

    public function joinFields(){
        if (!$this->_joinedFields){
            $this->getSelect()->join(
                array('related' => $this->getTable('webpromotions/promotions_cmsblock')),
                'related.block_id = e.block_id',
                array('position')
            );
            $this->_joinedFields = true;
        }
        return $this;
    }

    public function addPromotionFilter($promotion){
        if ($promotion instanceof Web_Promotions_Model_Promotions){
            $promotion = $promotion->getId();
        }
        if (!$this->_joinedFields){
            $this->joinFields();
        }
        $this->getSelect()->where('related.promotion_id = ?', $promotion);
        return $this;
    }
}