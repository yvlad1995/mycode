<?php

class Web_Promotions_Block_Carousel_Front extends Mage_Core_Block_Template
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getPromotionsCollection()
    {
        $id = $this->getRequest()->getParam('id');

        return Mage::helper('webpromotions')->getCmsBlocks($id);
    }

    public function getDataPromotions()
    {
        $data = $this->getPromotionsCollection();
        $blocks = array();
        $i = 0;
        foreach($data['block_id'] as $id){
            if($id != '0' && $i < 4){
                $blocks_id[] = $id;
            }
            $i++;
        }

        return array(
            'blocks'          => $blocks_id,
            'promotions_name' => $data['promotions']->getPromotionsName(),
            'description'     => $data['promotions']->getDescription()
        );
    }

    protected function getCategory($category_id)
    {
        foreach ($category_id as $id) {
            $category[] = Mage::getSingleton('catalog/category')->load($id)->getData();
        }

        return $category;
    }

    /**
     * @return mixed
     */
    public function getPromotions()
    {
            return  Mage::helper('webpromotions')->getPromotionsData($this->getCategoryId());
    }

    /**
     * @return string
     */
    public function getFront()
    {
        return Mage::getUrl() . "webpromotions/carousel/front/id/";
    }

    /**
     * @return null
     */
    protected function getCategoryId()
    {
        return Mage::registry('current_category') ? Mage::registry('current_category')->getId() : null;
    }

    /**
     * @return mixed
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();    
    }

    /**
     * @return bool
     */
    public function count()
    {
        $i = 0;
        foreach($this->getLoadedProductCollection() as $item)
        {
            if($item->getVisibility() != 1 && $item->getVisibility() != 3)
            $i++;
        }

        if($i == 0)
        {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    protected function _getProductCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
                                                ->addAttributeToSelect('image')
                                                ->addAttributeToSelect('visibility');

        $collection->joinTable(
            'webpromotions/promotions_product',
            'product_id=entity_id',
            array('product_id'=>'product_id'),
            'promotion_id=' . $this->getRequest()->getParam('id'),
            'right');

        return $collection;
    }
}
