<?php

class Web_Promotions_Block_Adminhtml_Promotions_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function _construct()
    {
        $helper = $this->helper('webpromotions');

        parent::_construct();
        $this->setId('webpromotions_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($helper->__('Promotion Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('products', array(
            'label'     => $this->__('Products'),
            'title'     => $this->__('Products'),
            'url'       => $this->getUrl('*/*/products', array('_current' => true)),
            'class'     => 'ajax'
        ));

        $this->addTab('categories', array(
            'label' => Mage::helper('webpromotions')->__('Associated categories'),
            'url'   => $this->getUrl('*/*/categories', array('_current' => true)),
            'class'    => 'ajax'
        ));
        return parent::_beforeToHtml();
    }

    protected function _prepareLayout()
    {
        $helper = $this->helper('webpromotions');

        $this->addTab('general_section', array(
            'label'    => $helper->__('General Information'),
            'content' => $this->getLayout()
                ->createBlock('webpromotions/adminhtml_promotions_edit_tab_form')
                ->toHtml(),
        ));



//        $this->addTab('products', array(
//            'label'   => $helper->__('Products Management'),
//            'content' => $this->getLayout()
//                ->createBlock('webpromotions/adminhtml_cmslinks_grid')
//                ->toHtml(),
//        ));

//        $this->addTab('products', array(
//            'label' => $helper->__('Associated products'),
//            'url'   => $this->getUrl('*/*/customersgrid', array('_current' => true)),
//            'class'    => 'ajax'
//        ));
    }

    public function helper($helper)
    {
        return Mage::helper($helper);
    }
}