<?php

$installer = $this;
$installer->getConnection()->dropTable($this->getTable('webpromotions/promotions_cmsblock'));
$table = $this->getConnection()
    ->newTable($this->getTable('webpromotions/promotions_cmsblock'))
    ->addColumn('rel_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Relation ID')
    ->addColumn('promotion_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Promotion ID')
    ->addColumn('block_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'CMS BLOCK ID')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
    ), 'Position')
    ->addIndex($this->getIdxName('webpromotions/promotions_cmsblock', array('block_id')), array('block_id'))
    ->addForeignKey($this->getFkName('webpromotions/promotions_cmsblock', 'promotion_id', 'webpromotions/promotions', 'id'),
        'promotion_id', $this->getTable('webpromotions/promotions'), 'id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($this->getFkName('webpromotions/promotions_cmsblock', 'block_id', 'cms/block', 'block_id'),
        'block_id', $this->getTable('cms/block'), 'block_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Promotion to Block Linkage Table');

$this->getConnection()->createTable($table);

$installer->endSetup();