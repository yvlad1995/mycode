<?php

class Web_Promotions_Helper_Image
{
    protected $_id;

    protected function getHelper()
    {
       return Mage::helper('webpromotions');
    }

    protected function getModel()
    {
        return Mage::getSingleton('webpromotions/promotions');
    }
    
    public function setId($id)
    {
        $this->_id = $id;
    }

    public function imageBaseSave()
    {
        try{
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg', 'jpeg'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);

            if(file_exists($this->getHelper()->getImagePath($this->_id))){
                unlink($this->getHelper()->getImagePath($this->_id));
            }

            $uploader->save($this->getHelper()->getImagePath(), $this->_id . '.jpg'); // Upload the image
            $this->imageResize();
        }catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

    }

    protected function imageResize()
    {
        $model = $this->getModel();

        $basePath = $this->getHelper()->getImagePath($this->_id);
        $newPath = $this->getHelper()->getImagePath($this->_id);

        $imageObj = new Varien_Image($basePath);
        $imageObj->constrainOnly(TRUE);
        $imageObj->keepAspectRatio(FALSE);
        $imageObj->keepFrame(FALSE);
        $imageObj->resize('760', '420');

        $imageObj->save($newPath);
    }

    public function getImagePromotions($first,$last)
    {
        $collection = Mage::getSingleton('webpromotions/promotions')->getCollection();
        $length = $last - $first + 1;

        foreach ($collection as $item)
        {
            if(!empty($item->getImageUrl()))
            {
                $images[] = '<li><a href=' . $this->getFront($item->getId()) . '><img src=' . $item->getImageUrl() .  ' alt=' . $item->getPromotionsName() . ' /></a></li>';
            }else{
                $images[] = '<li><a href=' . $this->getFront($item->getId()) . '><img src=' . Mage::helper('webpromotions')->getImageUrl() . '0.jpg' .  ' alt=' . $item->getPromotionsName() . ' /></a></li>';
            }
        }

        $selected = array_slice($images, $first, $length);
        $total    = count($images);

        return array(
            'n'      => $total,
            'result' => $selected
        );
    }

    protected function getFront($id)
    {
        return Mage::getUrl() . "webpromotions/carousel/front/id/" . $id;
    }
}
