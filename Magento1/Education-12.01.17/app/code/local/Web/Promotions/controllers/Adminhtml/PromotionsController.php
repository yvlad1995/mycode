<?php

class Web_Promotions_Adminhtml_PromotionsController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('webpromotions');
        $this->_addContent($this->getLayout()->createBlock('webpromotions/adminhtml_promotions'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function productsAction(){
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('promotion.edit.tab.product');
        $this->renderLayout();
    }

    public function productsGridAction(){
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('promotion.edit.tab.product')
                ->setPromotionProducts($this->getRequest()->getPost('promotion_products', null));;
        $this->renderLayout();
    }


    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        $model = Mage::getModel('webpromotions/promotions');
        $data = Mage::getSingleton('adminhtml/session')->getFormData();
        $cmsblockId = Mage::helper('webpromotions')->getCmsblockId($id);
        if($data){
            $model->setData($data)->setId($id);
        }else{
            $model->load($id);
            $model->setStaticBlocks($cmsblockId);
        }

        Mage::register('current_promotions', $model);

        $this->loadLayout()->_setActiveMenu('webpromotions');
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addLeft($this->getLayout()->createBlock('webpromotions/adminhtml_promotions_edit_tabs'));
        $this->_addContent($this->getLayout()->createBlock('webpromotions/adminhtml_promotions_edit'));

        $this->renderLayout();
    }

    public function saveAction()
    {
        if($data = $this->getRequest()->getPost())
        {
            try{
                $helper = Mage::helper('webpromotions');
                $model = Mage::getModel('webpromotions/promotions');
                $dataPost = $this->getDataPost($data);

                $model->setData($dataPost)->setId($this->getRequest()->getParam('id'));

                $cmsblock = $this->getRequest()->getPost('static_blocks', -1);
                if ($cmsblock != -1) {
                    $cmsblock = array_diff($cmsblock, array(''));
                    $cmsblock = array_unique($cmsblock);
                    $model->setCmsblockData($cmsblock);
                }

                $products = $this->getRequest()->getPost('products', -1);
                if ($products != -1) {
                    $model->setProductsData(Mage::helper('webpromotions/serializedecode')->decodeGridSerializedInput($products));
                }

                $categories = $this->getRequest()->getPost('category_ids', -1);
                if ($categories != -1) {
                    $categories = explode(',', $categories);
                    $categories = array_diff($categories, array(''));
                    $categories = array_unique($categories);
                    $model->setCategoriesData($categories);
                }

                $model->save();
                $id = $model->getId();
                
                if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '' ) {
                    $image = Mage::helper('webpromotions/image');
                    $image->setId($id);
                    $image->imageBaseSave();
                    $model->setImage($helper->getImageUrl($id));
                    $model->save()->setId('id');
                }

                if (isset($data['image']['delete']) && $data['image']['delete'] == 1 && file_exists($helper->getImagePath($id))) {
                    @unlink($helper->getImagePath($id));
                    $model->setImage('');
                    $model->save()->setId($id);
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Promotion \'%s\' was succesfuly saved', $model->getPromotionsName()));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                $this->_redirect('*/*/');

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);

                $this->_redirect('*/*/edit' , array(
                    'id'    => $this->getRequest()->getParam('id'),
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if($id = $this->getRequest()->getParam('id')){
            try{
                $helper = Mage::helper('webpromotions');

                Mage::getModel('webpromotions/promotions')->setId($id)->delete();
                @unlink($helper->getImagePath($id));
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Promotion was deleted successfully'));
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $promotions = $this->getRequest()->getParam('promotions', null);

        if(is_array($promotions) && sizeof($promotions) > 0){
            try {
                $helper = Mage::helper('webpromotions');
                foreach ($promotions as $id){
                    Mage::getModel('webpromotions/promotions')->setId($id)->delete();
                    @unlink($helper->getImagePath($id));
                }
                $this->_getSession()->addSuccess($this->__('Total of %d promotions have been deleted', sizeof($promotions)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select promotions'));
        }
        $this->_redirect('*/*');
    }

    public function categoriesAction(){
        $this->_initProduct();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function categoriesJsonAction(){
        $this->_initProduct();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('webpromotions/adminhtml_promotions_edit_tab_categories')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    protected function _initProduct()
    {

        $promotionId  = (int) $this->getRequest()->getParam('id');
        $promotion    = Mage::getModel('webpromotions/promotions')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        $promotion->setData('_edit_mode', true);
        if ($promotionId) {
            try {
                $promotion->load($promotionId);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        Mage::register('promotion', $promotion);
        Mage::register('current_promotion', $promotion);
        Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $promotion;
    }

    protected function getDataPost($data)
    {
       return array(
            'form_key'            => $data['form_key'],
            'promotions_name'     => $data['promotions_name'],
            'description'         => $data['description'],
            'is_enabled'          => $data['is_enabled'],
            'static_blocks'       => serialize($data['static_blocks']),
        );
    }

//    protected function _isAllowed()
//    {
//        switch ($this->getRequest()->getActionName()) {
//            case 'new':
//            case 'save':
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page/save');
//                break;
//            case 'delete':
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page/delete');
//                break;
//            default:
//                return Mage::getSingleton('admin/session')->isAllowed('cms/page');
//                break;
//        }
//    }



    protected function _initPromotionSave()
    {
        $promotion     = $this->_initPromotion();
        $promotionData = $this->getRequest()->getPost('promotion');
        if ($promotionData) {
            $this->_filterStockData($promotionData['stock_data']);
        }

        if (!isset($promotionData['website_ids'])) {
            $promotionData['website_ids'] = array();
        }

        $promotion->addData($promotionData);

        if (Mage::app()->isSingleStoreMode()) {
            $promotion->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }

        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $promotion->setData($attributeCode, false);
            }
        }

        $categoryIds = $this->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $promotion->setCategoryIds($categoryIds);
        }


        return $promotion;
    }
    
}