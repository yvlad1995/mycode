<?php

class Web4pro_HoneypotContactMe_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('name_hidden') != '')
        {
            $now = Mage::getModel('core/date')->timestamp(time());
            $date = date('m/d/y h:i:s', $now);
            $message = $this->getRequest()->getPost('name_hidden');

            Mage::log($this->__('Access error. Spam message: ') . $message . ' | time: ' . $date, null, 'honeypotcontactform.txt');

            Mage::getSingleton('core/session')->addError($this->__('Access error'));
            $this->_redirectReferer();
        }else{

            if ($this->getRequest()->isPost() && $this->getRequest()->getPost('phone')) {
                $session            = Mage::getSingleton('core/session');
                $contact            = $this->getRequest()->getPost('phone');
                $message            = $this->getRequest()->getPost('message');

                try {
                    $name           = Mage::getStoreConfig('trans_email/ident_general/name');
                    $email          = Mage::getStoreConfig('trans_email/ident_general/email');
                    $vars           = array('message' => $message, 'contact' => $contact, 'product_url' => $this->_getRefererUrl());
                    $emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Contact Me Request');

                    $emailTemplate
                        ->setSenderName($name)
                        ->setSenderEmail($email)
                        ->send($email, $name, $vars);

                    $session->addSuccess($this->__('Thank you. We will contact you as soon as possible.'));
                }
                catch (Mage_Core_Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the contact request: %s', $e->getMessage()));
                }
                catch (Exception $e) {
                    $session->addException($e, $this->__('There was a problem with the contact request.'));
                }
            }
            $this->_redirectReferer();
        }
    }

}

