jQuery(document).on("sharingSocialIcons", function(elem, param){
    if(url == null) return;
    jQuery.ajax({
        url: url,
        dataType: "json",
        data: {
            product_id: param.productId
        },
        success: function (json) {
            console.log(json);
            jQuery('body').prepend(json);
        }
    })
});

jQuery('body').on('click', function () {
    jQuery(document).mouseup(function (e){
        var div = jQuery('#sharing-social-popup');
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.remove();
        }
    });
});