var productLikes = Class.create({

    initialize: function(url, data){
        this.urlAddLike = url + data[0];
        this.urlActive = url + data[1];
        this.urlLikeQty = url + data[2];
    },

    setActiveBlock: function () {
        jQuery.ajax({
            url: this.urlActive,
            dataType: "json",
            data: {
                is_load: true
            },
            success: function (ids) {
                if (ids != null && ids.length > 0) {
                    for (var i = 0; i < ids.length; i++) {
                        jQuery('#product-like-' + ids[i] + ' span').addClass('active-likes');
                        jQuery(document).trigger("setActiveBlock", {product_id: ids[i]});
                    }
                }
            }
        })
    },

    setLikeQty: function (id) {
        jQuery.ajax({
            url: this.urlLikeQty,
            dataType: "json",
            data: {
                product_id: id,
                is_ajax: true
            },
            success: function (qty) {
                if (qty != null) {
                    jQuery('#product-like-' + id + ' span').text(qty);
                }
            }
        })
    },

    addProductLike: function(productId){
        jQuery(document).trigger("sharingSocialIcons", {productId: productId});
        jQuery.ajax({
            url: this.urlAddLike,
            dataType: "json",
            data: {
                product_id: productId
            },
            success: function(success) {
                if(success)
                {
                    jQuery(document).trigger("addProductLike", {addProductLike: true});
                }
            }
        });
    }
});