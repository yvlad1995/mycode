<?php

/**
 * Class Brander_LikesProduct_SocialController
 */
class Brander_LikesProduct_SocialController extends Brander_LikesProduct_Controller_AbstractController
{
    public function _construct()
    {
        $this->setHelper();
    }

    /**
     * @return bool
     */
    public function getSocialAction()
    {
        if ($this->_isAjax()) {
            $productId = $this->getRequest()->getParam('product_id');
            $product = $this->_getHelper()->getProduct($productId);
            echo $this->_getHelper()->getHtmlSocialLogin($product);
            return true;
        }
        return false;
    }

    public function setHelper()
    {
        $this->_helper = Mage::helper('branderlikes/social');
    }
}