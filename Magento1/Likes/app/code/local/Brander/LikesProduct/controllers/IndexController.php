<?php

/**
 * Class Brander_LikesProduct_IndexController
 */
class Brander_LikesProduct_IndexController extends Brander_LikesProduct_Controller_AbstractController
{
    public function _construct()
    {
        $this->setHelper();
    }

    public function addLikeAction()
    {
        $this->_validate();
        $productId = $this->getRequest()->getParam('product_id');
        $sessionProductLikesIds = $this->_getHelper()->getProductLikesIds();

        if (in_array($productId, $sessionProductLikesIds)) {
            echo false;
            return false;
        }

        $this->_getHelper()->setProductLikesSession($productId);

        $product = $this->_getHelper()->getProduct($productId);
        try {
            $qty = $product->getProductLikes();
            $product->setProductLikes(++$qty);
            $product->getResource()->saveAttribute($product, 'product_likes');

        } catch (Exception $e) {
            Mage::log('Save Likes exception:', $e, null, 'product_likes.log');
            echo false;
            return false;
        }

        echo true;
        return true;
    }


    public function getProductLikesIdsAction()
    {
        $this->_validate();
        echo $this->_getHelper()->jsonEncode($this->_getHelper()->getProductLikesIds());
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function setHelper()
    {
        $this->_helper = Mage::helper('branderlikes/likes');
    }

    public function getLikeQtyAction()
    {
        $this->_validate();
        $productId = $this->getRequest()->getParam('product_id');
        $likeQty = $this->_getHelper()->getQtyForProductLikes($productId);
        echo $this->_getHelper()->jsonEncode($likeQty);
    }

    /**
     * @throws Exception
     */
    protected function _validate()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) throw new Exception('not Ajax Request');
    }

}