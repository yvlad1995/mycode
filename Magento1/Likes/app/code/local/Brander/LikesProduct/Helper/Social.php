<?php

class Brander_LikesProduct_Helper_Social extends Brander_LikesProduct_Helper_Data
{
    /**
     * @param $product
     * @return mixed
     */
    public function getHtmlSocialLogin($product)
    {
        $html = '';
        if($this->isSharingEnabled()) {
            $socialSettings = $this->getCfg('social_accounts/social_settings');
            $item = $this->getShareData($product);
            $socialNetworks = array(
                '1' => 'facebook',
                '2' => 'google',
            );

            $html .= "<div id='sharing-social-popup'>";
            $html .= "<div class='social-links ib-wrapper--" . $socialSettings->getIconsType() . "'>";
            $html .= "<ul class='social-list'>";
            foreach ($socialNetworks as $socialNetwork) {
                $html .= "<li>";
                $html .= '<a onclick="Share.' . $socialNetwork . '(\'' . $item['url'] . '\', \'' . $item['title'] .
                    '\', \'' . $item['image'] . '\', \'' . $item['description'] . '\')">';
                $html .= "<span class='ib ib-hover" . $socialNetwork . "  ic-lg ic-" . $socialNetwork . "'></span>";
                $html .= "</a>";
                $html .= "</li>";

            }
            $html .= "</ul></div></div>";
        }
        return $this->jsonEncode($html);
    }

    /**
     * @param $product
     * @return array|bool
     */
    public function getShareData($product)
    {
        if ($product instanceof Mage_Catalog_Model_Product) {
            return array(
                'url' => $product->getProductUrl(),
                'title' => htmlspecialchars(str_replace("'", "\'", $product->getName())),
                'image' => Mage::helper('catalog/image')->init($product, 'image')->resize('600', '600'),
                'description' => htmlspecialchars(str_replace(array("'", "\r", "\n"), array("\'", " ", " "),
                    mb_substr($product->getData('short_content'), 0)))
            );
        } elseif ($product instanceof CommerceLab_News_Model_News) {
            return array(
                'url' => $product->getUrl(),
                'title' => htmlspecialchars(str_replace("'", "\'", $product->getTitle())),
                'image' => Mage::helper('et_sociallogin')->resizeImage(str_replace('clnews/', '',
                    $product->getImageFullContent()), '930', null, 'clnews'),
                'description' => htmlspecialchars(str_replace(array("'", "\r", "\n"), array("\'", " ", " "),
                    mb_substr($product->getData('short_content'), 0)))
            );
        }
        return false;
    }

    public function isSharingEnabled($storeId = null)
    {
        if (!$this->isSocialEnabled($storeId)) {
            return false;
        }

        return Mage::getStoreConfig('social_login/sharing/sharing');
    }

    public function isSocialEnabled($storeId = null)
    {
        if ($storeId == null) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return (int)Mage::getStoreConfig('social_login/general/active', $storeId);
    }
}