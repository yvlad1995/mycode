<?php

/**
 * Class Brander_LikesProduct_Helper_Data
 */
class Brander_LikesProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LIKES_ENABLE = "branderlikes/config/enable";

    /**
     * @param $data
     * @return mixed
     */
    public function jsonEncode($data)
    {
        return Mage::helper('core')->jsonEncode($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function jsonDecode($data)
    {
        return Mage::helper('core')->jsonDecode($data);
    }

    /**
     * @param $paramName
     * @param null $storeId
     * @return mixed|Varien_Object
     */
    public function getCfg($paramName, $storeId = null)
    {
        $configOption = Mage::getStoreConfig($paramName, $storeId);
        if (is_array($configOption)) {
            $config = new Varien_Object($configOption);
            return $config;
        }
        return $configOption;
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getSession()
    {
        return Mage::getSingleton('core/session');
    }

    /**
     * @param $productId
     * @return Mage_Core_Model_Abstract
     */
    public function getProduct($productId)
    {
        return Mage::getModel('catalog/product')->load($productId);
    }

    /**
     * @param $path
     * @return string
     */
    public function getUrl($path)
    {
        return Mage::getUrl($path);
    }

    /**
     * @return Mage_Core_Model_Layout
     */
    public function getLayout()
    {
        return Mage::app()->getLayout();
    }

    public static function isEnableLike()
    {
        return Mage::getStoreConfig(self::LIKES_ENABLE, Mage::app()->getStore()->getId());
    }
}