<?php

/**
 * Class Brander_LikesProduct_Helper_Template
 * Class rewrite Infortis_Ultimo_Helper_Template
 */
class Brander_LikesProduct_Helper_Template extends Infortis_Ultimo_Helper_Template
{
	public function getCategoryAddtoLinksIcons($_product, $_compareUrl, $wrapperClasses = '')
	{
	    if(!Brander_LikesProduct_Helper_Data::isEnableLike())
        {
            return parent::getCategoryAddtoLinksIcons($_product, $_compareUrl, $wrapperClasses);
        }

        $html = '
			<li><a class="link-like" id="product-like-' . $_product->getId() . '"
			    onclick="productLikes.addProductLike(' . $_product->getId() . ', ' . $_product->getProductLikes() . ')"
				title="' . $this->__('Like') . '">
					<span class="2 icon ib ic ic-heart">' . $_product->getProductLikes() . '</span>
			</a></li>';

        if (Mage::helper('wishlist')->isAllow())
        {
            $html .= '
			<li><a class="link-wishlist" 
				href="' . Mage::helper('wishlist')->getAddUrl($_product) . '" 
				title="' . $this->__('Add to Wishlist') . '">
					<span class="2 icon ib ic ic-heart"></span>
			</a></li>';
        }

        if ($_compareUrl)
        {
            $html .= '
			<li><a class="link-compare"
				href="' . $_compareUrl . '" 
				title="' . $this->__('Add to Compare') . '">
					<span class="2 icon ib ic ic-compare"></span>
			</a></li>';
        }

        if (!empty($html))
        {
            return '<ul class="add-to-links clearer ' . $wrapperClasses .'">' . $html . '</ul>';
        }
        return $html;
	}

}
