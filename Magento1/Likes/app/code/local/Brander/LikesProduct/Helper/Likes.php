<?php

/**
 * Class Brander_LikesProduct_Helper_Likes
 */
class Brander_LikesProduct_Helper_Likes extends Brander_LikesProduct_Helper_Data
{
    /**
     * @return array|mixed
     */
    public function getProductLikesIds()
    {
        $productIds = $this->jsonDecode($this->getSession()->getProductLikesIds());
        if(!$productIds)
        {
            return [];
        }

        return $productIds;
    }

    /**
     * @param $productId
     */
    public function setProductLikesSession($productId)
    {
        $sessionProductLikesIds = $this->getProductLikesIds();
        $sessionProductLikesIds[] = $productId;
        $sessionProductLikesIds = array_unique($sessionProductLikesIds);
        Mage::getSingleton('core/session')
            ->setProductLikesIds($this->jsonEncode($sessionProductLikesIds));
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getQtyForProductLikes($productId)
    {
        return $this->getProduct($productId)->getProductLikes();
    }
}