<?php

abstract class Brander_LikesProduct_Controller_AbstractController extends Mage_Core_Controller_Front_Action
{
    protected $_helper;
    /**
     * @return Mage_Core_Helper_Abstract
     */
    abstract protected function setHelper();

    protected function _getHelper()
    {
        return $this->_helper;
    }

    /**
     * @return bool
     */
    protected function _isAjax()
    {
        return $this->getRequest()->isXmlHttpRequest();
    }
}