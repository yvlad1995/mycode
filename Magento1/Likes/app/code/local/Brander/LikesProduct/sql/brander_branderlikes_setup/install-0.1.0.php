<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'product_likes', array(
    'type'                      => 'int',
    'backend'                   => '',
    'frontend'                  => '',
    'label'                     => 'Product Likes',
    'input'                     => 'text',
    'class'                     => '',
    'group'                     => 'General',
    'source'                    => '',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => true,
    'required'                  => false,
    'default'                   => 0,
    'filterable'                => false,
    'visible_on_front'          => true,
    'unique'                    => false,
    'is_configurable'           => true,
    'used_in_product_listing'   => 1
));

$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'product_likes',
    '40'
);

$installer->endSetup();

?>