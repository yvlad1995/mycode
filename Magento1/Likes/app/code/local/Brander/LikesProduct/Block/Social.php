<?php

/**
 * Class Brander_LikesProduct_Block_Social
 */
class Brander_LikesProduct_Block_Social extends Mage_Core_Block_Template
{
    const CATEGORY_LIST = 'catalog_category_layered';

    const CMS_PAGE      = 'cmsadvanced_page_view';

    /**
     * Brander_LikesProduct_Block_Social constructor.
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        $this->getBlockForHandle();
        parent::__construct($args);
    }

    public function getBlockForHandle()
    {
        $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
        if(in_array(self::CATEGORY_LIST,$handles))
        {
            $child = $this->getHelper()->getLayout()->createBlock('core/template')
                ->setTemplate('brander/productLikes/socialPopupProducts.phtml');
            $this->setChild('social.script', $child);
        }elseif (in_array(self::CMS_PAGE,$handles)){
            $child = $this->getHelper()->getLayout()->createBlock('core/template')
                ->setTemplate('brander/productLikes/socialPopupProducts.phtml');
            $this->setChild('social.script', $child);
        }

    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function getHelper()
    {
        return Mage::helper('branderlikes/social');
    }
}